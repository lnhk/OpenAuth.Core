﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace OpenAuth.Repository.Enum.SumpleEnum
{
    //是否
    public enum YesNo
    {
        [Description("是")]
        是 = 1,
        [Description("否")]
        否 = 0
    }
    //报告语言
    public enum PortLg
    {
        [Description("中文")]
        中文 = 0,
        [Description("英文")]
        英文 = 1,
        [Description("中英对照")]
        中英对照 = 2
    }
    //报告语言
    public enum PortType
    {
        [Description("电子版")]
        电子版 = 0,
        [Description("纸质版")]
        纸质版 = 1,
        [Description("电子版和纸质版")]
        电子版和纸质版 = 2
    }
    //报告语言
    public enum PortNum
    {
        [Description("一份")]
        一份 = 1,
        [Description("两份")]
        两份 = 2,
        [Description("三份")]
        三份 = 3,
        [Description("四份")]
        四份 = 4,
        [Description("五份")]
        五份 = 5,
        [Description("6份")]
        六份 = 6,
        [Description("七份")]
        七份 = 7
    }
    //样品状态
    public enum SampleState
    {
        [Description("草稿")]
        草稿 = 0,
        [Description("审核中")]
        审核中 = 1,
        [Description("待接收")]
        待接收 = 2,
        [Description("待流转")]
        待流转 = 3,
        [Description("已流转")]
        已流转 = 4,
        [Description("制样中")]
        制样中 = 5,
        [Description("制样完成")]
        制样完成 = 6,
        [Description("检测中")]
        检测中 = 7,
        [Description("报告编制")]
        报告编制 = 8,
        [Description("报告审核")]
        报告审核 = 9,
        [Description("报告批准")]
        报告批准 = 10,
        [Description("报告完成")]
        报告完成 = 11,
        [Description("变更中")]
        变更中 = 9,
        [Description("已作废")]
        已作废 = 999,
    }
    //样品状态
    public enum BuildSampleState
    {

        [Description("检测中")]
        检测中 = 7,
        [Description("报告编制")]
        报告编制 = 8,
        [Description("报告完成")]
        报告完成 = 11

    }
    //分包实验室类型
    public enum FenbaoLibTyppe
    {

        [Description("理化")]
        理化 = 0,
        [Description("微生物")]
        微生物 = 1
    }
    //样品状态
    public enum ReportState
    {
        [Description("报告编制")]
        报告编制 = 8,
        [Description("报告审核")]
        报告审核 = 9,
        [Description("报告批准")]
        报告批准 = 10,
        [Description("报告完成")]
        报告完成 = 11
    }
    public enum PrintType
    {
        [Description("不按科室")]
        不按科室 = 0,
        [Description("单科室")]
        单科室 = 1,
        [Description("全部科室")]
        全部科室 = 2,
        [Description("全部科室(按样品数量)")]
        全部科室按样品数量 = 3
    }
    //样品状态
    public enum AudiType
    {
        [Description("监督抽检项目")]
        监督抽检项目 = 0,
        [Description("风险检测项目")]
        风险检测项目 = 1
    }
    public enum WrapTypeEnum
    {
        [Description("单包装")]
        单包装 = 0,
        [Description("多包装")]
        多包装 = 1
    }
    public enum ReportWay
    {
        [Description("国抽")]
        国抽 = 0,
        [Description("LIMS")]
        LIMS = 1
    }
    public enum FeedBackEnum
    {
        [Description("未反馈")]
        未反馈 = 0,
        [Description("部分反馈")]
        部分反馈 = 1,
        [Description("已反馈")]
        已反馈 = 2
    }

    /// <summary>
    /// 是否为重新流转
    /// </summary>
    public enum IsFlowAgin
    {
        [Description("否")]
        否 = 0,
        [Description("是")]
        是 = 1
    }

    /// <summary>
    /// 是否需要重新流转（合同变更时,可勾选样品是否需要重新流转）
    /// </summary>
    public enum IsNeedFlowAgin
    {
        [Description("不需要")]
        不需要 = 0,
        [Description("需要")]
        需要 = 1,
        [Description("已重新流转过")]
        已重新流转过 = 2,
    }
    //基础数据状态 
    public enum BaseDataState
    {
        [Description("扩项中")]
        扩项中 = 0,
        [Description("可用数据")]
        可用数据 = 1
    }
    //物体表面采样方法 
    public enum XcOrginfoState
    {
        [Description("保存")]
        保存 = 0,
        [Description("待审核")]
        待审核 = 1,
        [Description("已完成")]
        已完成 = 2
    }
}
