﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace OpenAuth.Repository.Enum.ContractEnum
{
    public static class EventType
    {
        public const string ContractCreate = "创建合同";
        public const string ContractEdit = "修改合同";
        public const string ContractCommit = "提交合同";
        public const string ContractAuditPas = "评审通过";
        public const string ContractAuditBort = "评审驳回";
        public const string ContractChange = "合同变更";
        public const string ContractChangeBort = "变更驳回";
        public const string ContractChangePas = "变更通过";
        public const string ContractDelete = "删除合同";
    }
    //协议领域
    public enum CtTypeEnum
    {
        [Description("食品及相关产品")]
        SP = 0,
        [Description("消毒产品或消毒场所")]
        WS = 1,
        [Description("环境")]
        HJ = 2,
        //[Description("血液透析水/透析液")]
        //血液透析水 = 3, 
        [Description("生活饮用水")]
        SS = 4,
        [Description("饲料")]
        SL = 5,
        [Description("农业相关产品")]
        NY = 6,
        [Description("涉水材料等")]
        None = 7,
        [Description("公共场所")]
        GG = 8,
        ////[Description("肥料")]
        ////FL = 9
        //[Description("质控样品")]
        //质控样品 = 9,
        //[Description("扩项样品")]
        //扩项样品 = 10

    }
    public enum CtStateEnum
    {
        [Description("草稿")]
        草稿 = 0,
        [Description("驳回")]
        驳回 = 6,
        [Description("合同评审")]
        合同评审 = 1,
        [Description("主管审核")]
        主管审核 = 2,
        [Description("工作中")]
        工作中 = 3,
        [Description("变更中")]
        变更中 = 4,
        [Description("报告编制")]
        报告编制 = 5,
        [Description("报告审核")]
        报告审核 = 7,
        [Description("报告批准")]
        报告批准 = 8,
        [Description("已完成")]
        已完成 = 9,
        [Description("实验室驳回")]
        实验室驳回 = 10,
        [Description("已作废")]
        已作废 = 999
    }
    public enum InvoiceStateEnum
    {
        [Description("未申请")]
        未申请 = 0,
        [Description("申请开票")]
        申请开票 = 1,
        [Description("已开票")]
        已开票 = 2,
        [Description("已邮寄")]
        已邮寄 = 3
    }
    //是否
    public enum YesNo
    {
        [Description("是")]
        是 = 1,
        [Description("否")]
        否 = 0
    }
    //是否同意方法偏离
    public enum YesNoMethod
    {
        [Description("同意")]
        是 = 1,
        [Description("不同意")]
        否 = 0
    }
    //是否同意分包
    public enum YesNoSub
    {
        [Description("同意外部实验室和本实验室共同完成")]
        同意外部实验室和本实验室共同完成 = 0,
        [Description("不同意外部实验室和本实验室共同完成")]
        不同意外部实验室和本实验室共同完成 = 1,
        [Description("无分包")]
        无分包 = 2
    }
    //报告领取方式
    public enum ReportWay
    {
        [Description("邮寄(邮费自理)")]
        邮寄 = 0,
        [Description("自取")]
        自取 = 1
    }
    //报告服务类型
    public enum ReportService
    {
        [Description("标准服务")]
        标准服务 = 0,
        [Description("加急服务")]
        加急服务 = 1
    }
    public enum OperateType
    {
        [Description("提交")]
        提交 = 0,
        [Description("通过")]
        通过 = 1,
        [Description("驳回")]
        驳回 = 2,
        [Description("变更申请")]
        变更申请 = 3,
        [Description("变更驳回")]
        变更驳回 = 4
    }
    public enum PaymentAudit
    {
        [Description("审核中")]
        提交 = 0,
        [Description("通过")]
        通过 = 1,
        [Description("驳回")]
        驳回 = 2
    }
    public enum ObjectCategoryEnum   //项目类别
    {
        [Description("环评检测")]
        环评检测 = 0,
        [Description("验收检测")]
        验收检测 = 1,
        [Description("现状检测")]
        现状检测 = 2,
        [Description("例行检测")]
        例行检测 = 3,
    }

    public enum TestCategoryEnum   //检测类别
    {
        [Description("水质")]
        水质 = 0,
        [Description("环境空气")]
        环境空气 = 1,
        [Description("废气")]
        废气 = 2,
        [Description("其他")]
        其他 = 3,
    }

    public enum SampleSourceEnum   //样品来源
    {
        [Description("采样")]
        采样 = 0,
        [Description("送样")]
        送样 = 1,
        [Description("现场检测")]
        现场检测 = 2
    }
    public enum PaymentMethodEnum   //样品来源
    {
        [Description("报告发送前")]
        报告发送前 = 0,
        [Description("报告发送后")]
        报告发送后 = 1
    }
    public enum RetentionSamplesEnum   ///样品留样 
    {
        [Description("留样")]
        采样 = 0,
        [Description("不留样")]
        送样 = 1
    }
    public enum CtLicensTypeEnum   ///样品留样 
    {
        [Description("企业委托")]
        企业委托 = 0,
        [Description("政府委托")]
        政府委托 = 1,
        [Description("政府年协议")]
        政府年协议 = 2,
        [Description("企业年协议")]
        企业年协议 = 3
    }


    public enum ReportNumEnum  //报告份数
    {
        [Description("一份")]
        一份 = 1,
        [Description("两份")]
        两份 = 2,
        [Description("三份")]
        三份 = 3,
        [Description("四份")]
        四份 = 4,
        [Description("五份")]
        五份 = 5,
        [Description("六份")]
        六份 = 6,
        [Description("七份")]
        七份 = 7
    }

    public enum InvoiceTypeEnum  //报告份数
    {
        [Description("增值税普通发票")]
        增值税普通发票 = 0,
        [Description("增值税专用发票")]
        增值税专用发票 = 1,
        [Description("不开发票")]
        不开发票 = 2,
    }
    public enum YearEnum  //年度枚举
    {
        [Description("2019年")]
        _2019年 = 2019,
        [Description("2020年")]
        _2020年 = 2020,
        [Description("2021年")]
        _2021年 = 2021,
        [Description("2022年")]
        _2022年 = 2022,
    }

    ///// <summary>
    ///// 任务单项目状态
    ///// </summary>
    //public enum TaskListProjectStatus
    //{
    //    /// <summary>
    //    /// 待接收
    //    /// </summary>
    //    [Description("待接收")]
    //    待接收 = 0,
    //    /// <summary>
    //    /// 已接收
    //    /// </summary>
    //    [Description("已接收")]
    //    已接收 = 1,
    //    /// <summary>
    //    /// 通过
    //    /// </summary>
    //    [Description("已通过")]
    //    已通过 = 2,
    //    /// <summary>
    //    /// 驳回
    //    /// </summary>
    //    [Description("已驳回")]
    //    已驳回 = 3
    //}

    /// <summary>
    /// 样品任务单状态
    /// </summary>
    public enum SampleTaskListStatus
    {
        /// <summary>
        /// 待处理
        /// </summary>
        [Description("待处理")]
        待处理 = 0,
        /// <summary>
        /// 处理中
        /// </summary>
        [Description("处理中")]
        处理中 = 1,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        已驳回 = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        [Description("已完成")]
        已完成 = 3,
        /// <summary>
        /// 已作废
        /// </summary>
        [Description("已作废")]
        已作废 = 999
    }

    /// <summary>
    /// 原始记录审核状态
    /// </summary>
    public enum OrgRecordStatus
    {
        /// <summary>
        /// 待处理
        /// </summary>
        [Description("待处理")]
        待处理 = -2,
        /// <summary>
        /// 处理中
        /// </summary>
        [Description("处理中")]
        处理中 = -1,
        /// <summary>
        /// 待处理
        /// </summary>
        [Description("待审核")]
        待审核 = 0,
        /// <summary>
        /// 已通过
        /// </summary>
        [Description("已通过")]
        已通过 = 1,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        已驳回 = 2,
        /// <summary>
        /// 已退回
        /// </summary>
        [Description("已退回")]
        已退回 = 3
    }


    /// <summary>
    /// 样品任务单项目状态
    /// </summary>
    public enum SampleTasklistProjectStatus
    {
        /// <summary>
        /// 待处理
        /// </summary>
        [Description("待处理")]
        待处理 = 0,
        /// <summary>
        /// 处理中
        /// </summary>
        [Description("处理中")]
        处理中 = 1,
        /// <summary>
        /// 审核中
        /// </summary>
        [Description("审核中")]
        审核中 = 2,
        /// <summary>
        /// 已通过
        /// </summary>
        [Description("已通过")]
        已通过 = 3,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        已驳回 = 4,
        /// <summary>
        /// 已退回
        /// </summary>
        [Description("已退回")]
        已退回 = 5,
        /// <summary>
        /// 已作废
        /// </summary>
        [Description("已作废")]
        已作废 = 999
    }

    /// <summary>
    /// 任务单检测类型
    /// </summary>
    public enum TasklistCheckType
    {
        /// <summary>
        /// 企业
        /// </summary>
        [Description("委托检测")]
        委托检测 = 0,

        /// <summary>
        /// 政府
        /// </summary>
        [Description("抽检")]
        抽检 = 1
    }

    /// <summary>
    /// 样品作废申请类型
    /// </summary>
    public enum SampleObsoleteApplyType
    {
        /// <summary>
        /// 作废申请
        /// </summary>
        [Description("作废申请")]
        作废申请 = 0,

        /// <summary>
        /// 恢复申请
        /// </summary>
        [Description("恢复申请")]
        恢复申请 = 1
    }

    /// <summary>
    /// 样品作废状态
    /// </summary>
    public enum SampleObsoleteStatus
    {
        /// <summary>
        /// 审核中
        /// </summary>
        [Description("审核中")]
        审核中 = 0,
        /// <summary>
        /// 已通过
        /// </summary>
        [Description("已通过")]
        已通过 = 1,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        已驳回 = 2
    }


    /// <summary>
    /// 原始记录退回来源
    /// </summary>
    public enum OrgRecordRetrunSource
    {
        实验室 = 1,
        报告编制 = 2,
        报告审核 = 3,
        报告批准 = 4
    }

    /// <summary>
    /// 原始记录复检申请书状态
    /// </summary>
    public enum RecheckApplyStatus
    {
        /// <summary>
        /// 处理中
        /// </summary>
        [Description("处理中")]
        处理中 = 0,
        /// <summary>
        /// 审核中
        /// </summary>
        [Description("审核中")]
        审核中 = 1,
        /// <summary>
        /// 已通过
        /// </summary>
        [Description("已通过")]
        已通过 = 2,
        /// <summary>
        /// 已驳回
        /// </summary>
        [Description("已驳回")]
        已驳回 = 3
    }

    /// <summary>
    /// 合同作废
    /// </summary>
    public enum ContractObsoleteEnum
    {
        /// <summary>
        /// 否
        /// </summary>
        [Description("否")]
        否 = 0,
        /// <summary>
        /// 是
        /// </summary>
        [Description("是")]
        是 = 1
    }
    /// <summary>
    /// 年协议完成方式
    /// </summary>
    public enum DoneTylpeEnum
    {
        /// <summary>
        /// 时间点
        /// </summary>
        [Description("时间点")]
        时间点 = 0,
        /// <summary>
        /// 时间段
        /// </summary>
        [Description("时间段")]
        时间段 = 1
    }

    /// <summary>
    /// 企标类型
    /// </summary>
    public enum QBTypeEnum
    {
        [Description("现行有效")]
        现行有效 = 1,
        [Description("修订")]
        修订 = 2,
        [Description("废止")]
        废止 = 3
    }
}