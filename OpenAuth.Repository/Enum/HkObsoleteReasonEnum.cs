﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace OpenAuth.Repository.Enum
{
    public enum HkObsoleteReasonEnum
    {
        [Description("审核中")]
        审核中 = 0,
        [Description("申请通过")]
        申请通过 = 1,
        [Description("驳回")]
        驳回 = 2
    }
}
