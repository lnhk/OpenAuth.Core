﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace OpenAuth.Repository.Enum
{
    public enum InvoiceEnum
    {
        [Description("增值税普通发票")]
        增值税普通发票 = 0,
        [Description("增值税专用发票")]
        增值税专用发票 = 1,
        [Description("收据")]
        收据 = 2,
        [Description("电子发票")]
        电子发票 = 3,
        [Description("不开发票")]
        不开发票 = 4
    }
}
