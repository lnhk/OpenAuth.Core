﻿//------------------------------------------------------------------------------
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
//     Author:Yubao Li
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using OpenAuth.Repository.Core;

namespace OpenAuth.Repository.Domain
{
    /// <summary>
    ///
    /// </summary>
    [Table("hk_base_qualition")]
    public class BaseQualition : IntAutoGenEntity
    {
        public BaseQualition()
        {
            this.ntype = 0;
            this.unitname = "";
            this.update_by = "";
            this.remarks = "";
            this.create_by = "";
            this.sampletype = "";
            this.unit = 0;
            this.update_date = new DateTime?();
            this.create_date = DateTime.Now;
            this.qualition = "";
            this.factor_standa_id = "";
            this.xh = 0;
            this.factor_standazf_id = "";

        }
        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public int? ntype { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string unitname { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string update_by { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string remarks { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string create_by { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string sampletype { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public int? unit { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public DateTime? update_date { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public DateTime? create_date { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string qualition { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string factor_standa_id { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public int? xh { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string factor_standazf_id { get; set; }


    }
}