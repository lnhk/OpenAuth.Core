﻿//------------------------------------------------------------------------------
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
//     Author:Yubao Li
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using OpenAuth.Repository.Core;

namespace OpenAuth.Repository.Domain
{
    /// <summary>
    ///
    /// </summary>
    [Table("Library")]
    public class Library : StringEntity
    {
        public Library()
        {
            this.pid = "";
            this.departname = "";

        }
        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string pid { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string departname { get; set; }


    }
}