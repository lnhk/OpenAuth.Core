﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using OpenAuth.Repository.Core;

namespace OpenAuth.Repository.Domain
{
    /// <summary>
    /// 单位转换
    /// </summary>
    [Table("hk_unit_convert")]
    public class UnitConvert : IntAutoGenEntity
    {
        public UnitConvert()
        {
            this.suid = 0;
            this.tuid = 0;
            this.convert = 0;
            this.create_by = "";
            this.create_date = DateTime.Now;
            this.update_by = "";
            this.update_date = new DateTime?();
        }
        /// <summary>
        /// 原始单位id
        /// </summary>
        [Description("")]
        public int? suid { get; set; }

        /// <summary>
        /// 目标单位id
        /// </summary>
        [Description("")]
        public int? tuid { get; set; }

        /// <summary>
        /// 换算值
        /// </summary>
        [Description("")]
        public int? convert { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string create_by { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public DateTime? create_date { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public string update_by { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Description("")]
        public DateTime? update_date { get; set; }
    }
}
