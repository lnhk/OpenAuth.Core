using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 任务单样品数据接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "合同_任务单样品数据接口_SampleTaskListDatas")]
    public class SampleTaskListDatasController : ControllerBase
    {
        private readonly SampleTaskListDataApp _app;
        
        //获取详情
        [HttpGet]
        public Response<SampleTaskListData> Get(decimal id)
        {
            var result = new Response<SampleTaskListData>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add(AddSampleTaskListDataReq req)
        {
            var result = new Response();
            try
            {
                SampleTaskListData sampleTaskListData = req.MapTo<SampleTaskListData>();
                _app.Add(sampleTaskListData);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update(AddSampleTaskListDataReq req)
        {
            var result = new Response();
            try
            {
                SampleTaskListData sampleTaskListData = req.MapTo<SampleTaskListData>();
                _app.Update(sampleTaskListData);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QuerySampleTaskListDataListReq request)
        {
            return await _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody]decimal[] ids)
        {
            var result = new Response();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public SampleTaskListDatasController(SampleTaskListDataApp app) 
        {
            _app = app;
        }
    }
}
