using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 作废原因接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "合同_作废原因接口_ObsoleteReasons")]
    public class ObsoleteReasonsController : ControllerBase
    {
        private readonly ObsoleteReasonApp _app;
        
        //获取详情
        [HttpGet]
        public Response<ObsoleteReason> Get(decimal id)
        {
            var result = new Response<ObsoleteReason>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add(AddObsoleteReasonReq req)
        {
            var result = new Response();
            try
            {
                ObsoleteReason obsoleteReason = req.MapTo<ObsoleteReason>();
                _app.Add(obsoleteReason);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update(UpdateObsoleteReasonReq req)
        {
            var result = new Response();
            try
            {
                ObsoleteReason obsoleteReason = req.MapTo<ObsoleteReason>();
                _app.Update(obsoleteReason);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QueryObsoleteReasonListReq request)
        {
            return await _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _app.Delete(ids);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public ObsoleteReasonsController(ObsoleteReasonApp app) 
        {
            _app = app;
        }
    }
}
