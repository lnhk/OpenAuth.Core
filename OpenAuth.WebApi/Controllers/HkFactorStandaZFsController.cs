using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 政府项目方法关联接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "政府项目方法关联接口_HkFactorStandaZFs")]
    public class HkFactorStandaZFsController : ControllerBase
    {
        private readonly FactorStandaZFApp _app;
        
        //获取详情
        [HttpGet]
        public Response<FactorStandaZF> Get(string id)
        {
            var result = new Response<FactorStandaZF>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add(AddFactorStandaZFReq req)
        {
            var result = new Response();
            try
            {
                FactorStandaZF factorStandaZF = req.MapTo<FactorStandaZF>();
                _app.Add(factorStandaZF);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update(UpdateFactorStandaZFReq req)
        {
            var result = new Response();
            try
            {
                FactorStandaZF factorStandaZF = req.MapTo<FactorStandaZF>();
                _app.Update(factorStandaZF);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QueryFactorStandaZFListReq request)
        {
            return await _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody]string[] ids)
        {
            var result = new Response();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public HkFactorStandaZFsController(FactorStandaZFApp app) 
        {
            _app = app;
        }
    }
}
