using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 客户接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "客户_客户接口_Customers")]
    public class CustomersController : ControllerBase
    {
        private readonly CustomerApp _cstmrApp;
        private readonly CustomerUserApp _cstmrUserApp;
        private readonly CustomerContractApp _cstmrContractApp;
        private readonly InvoiceApp _invoiceApp;

        //获取详情
        [HttpGet]
        public Response<Customer> Get(decimal id)
        {
            var result = new Response<Customer>();
            try
            {
                result.Result = _cstmrApp.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 添加客户
        /// </summary>
        /// <param name="addCstmrReq"></param>
        /// <returns></returns>
        [HttpPost]
        public Response Add(AddCustomerReq addCstmrReq)
        {
            var result = new Response();
            try
            {
                Customer cstmr = addCstmrReq.MapTo<Customer>();
                _cstmrApp.Add(cstmr);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 添加客户以及关联归属领域，联系人，发票
        /// </summary>
        /// <param name="addCstmrReq"></param>
        /// <returns></returns>
        [HttpPost]
        public Response AddWithRelated(AddCustomerWithRelatedReq addCstmrReq)
        {
            var result = new Response();
            try
            {
                //映射客户
                Customer cstmr = addCstmrReq.Customer.MapTo<Customer>();

                //映射归属领域
                List<CustomerUser> cstmrUsers = new List<CustomerUser>();
                string[] domainNameArray = addCstmrReq.CustomerDomainNameArray.Split(',');
                string[] domainValueArray = addCstmrReq.CustomerDomainValueArray.Split(',');
                for (int i = 0; i < domainNameArray.Length; i++)
                {
                    CustomerUser cstmrUser = new CustomerUser();
                    cstmrUser.Domain = domainValueArray[i];
                    cstmrUser.DomainName = domainNameArray[i];
                    cstmrUser.UserId = addCstmrReq.CustomerUserId;
                    cstmrUser.Username = addCstmrReq.CustomerUsername;
                    cstmrUser.UserType = "0";
                    cstmrUser.UserTypeName = "开发";
                    cstmrUsers.Add(cstmrUser);
                }

                //映射联系人
                List<CustomerContract> cstmrContracts = new List<CustomerContract>();
                if (addCstmrReq.CustomerContractReqs != null && addCstmrReq.CustomerContractReqs.Any())
                {
                    foreach (var contractReq in addCstmrReq.CustomerContractReqs)
                    {
                        CustomerContract cstmrContract = contractReq.MapTo<CustomerContract>();
                        cstmrContracts.Add(cstmrContract);
                    }
                }

                //映射发票
                List<Invoice> invoices = new List<Invoice>();
                if (addCstmrReq.InvoiceReqs != null && addCstmrReq.InvoiceReqs.Any())
                {
                    foreach (var invoiceReq in addCstmrReq.InvoiceReqs)
                    {
                        Invoice invoice = invoiceReq.MapTo<Invoice>();
                        invoices.Add(invoice);
                    }
                }

                _cstmrApp.AddWithRelated(cstmr, cstmrUsers, cstmrContracts, invoices);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        //修改
        [HttpPost]
        public Response Update(UpdateCustomerReq obj)
        {
            var result = new Response();
            try
            {
                _cstmrApp.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery] QueryCustomerListReq request)
        {
            return await _cstmrApp.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _cstmrApp.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 获取客户详细信息,无客户返回空结果
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response<QueryCustomerInfo> GetCustomerInfo([FromBody] QueryCustomerInfoReq req)
        {
            var result = new Response<QueryCustomerInfo>();
            try
            {
                bool isExist = false;
                Customer customer = new Customer();
                if (req.CompanyId != null)
                {
                    isExist = _cstmrApp.Check(req.CompanyId);
                    if (isExist)
                    {
                        customer = _cstmrApp.Get(req.CompanyId);
                    }
                }
                else if (req.CompanyName != null)
                {
                    isExist = _cstmrApp.Check(req.CompanyName);
                    if (isExist)
                    {
                        customer = _cstmrApp.Get(req.CompanyName);
                    }
                }

                if (isExist)
                {
                    result.Result = customer.MapTo<QueryCustomerInfo>();
                    result.Result.Invoices = _invoiceApp.GetByComapnyId(customer.Id);
                    result.Result.CustomerUsers = _cstmrUserApp.GetByComapnyId(customer.Id);
                    result.Result.CustomerContracts = _cstmrContractApp.GetByComapnyId(customer.Id);
                }
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 获取当前用户所归属的所有客户的详细信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Response<QueryAllCstmrByNowUser> GetAllCstmrByNowUser()
        {
            var resp = new Response<QueryAllCstmrByNowUser>();
            try
            {
                resp.Result = new QueryAllCstmrByNowUser();
                List<Customer> cstmrs = _cstmrApp.GetAllCstmrByNowUser();
                resp.Result.CustomerInfos = new List<QueryCustomerInfo>();
                foreach (Customer cstmr in cstmrs)
                {
                    QueryCustomerInfo cstmrInfo = GetCustomerInfo(new QueryCustomerInfoReq() { CompanyId = cstmr.Id }).Result;
                    resp.Result.CustomerInfos.Add(cstmrInfo);
                }
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        /// <summary>
        /// 获取指定客户的指定领域的开发归属的数量
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response<int> GetDevUserNum(GetDevUserNumReq req)
        {
            Response<int> resp = new Response<int>();
            try
            {
                resp.Result = _cstmrApp.GetDevUserNum(req.Domain, req.CompanyId);
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="domainName"></param>
        ///// <param name="companyName"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public Response<bool> CheckDevCstmrUser(string domainName,string companyName)
        //{
        //    var resp = new Response<bool>();
        //    try
        //    {
        //        resp.Result = _app.CheckDevCstmrUser(domainName, companyName);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = 500;
        //        resp.Message = ex.InnerException?.Message ?? ex.Message;
        //    }
        //    return resp;
        //}

        public CustomersController(CustomerApp app, CustomerUserApp cstmrUserApp,
            CustomerContractApp cstmrContractApp, InvoiceApp invoiceApp)
        {
            _cstmrApp = app;
            _cstmrUserApp = cstmrUserApp;
            _cstmrContractApp = cstmrContractApp;
            _invoiceApp = invoiceApp;
        }
    }
}
