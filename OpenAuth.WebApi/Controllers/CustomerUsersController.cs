using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 客户归属接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "客户_客户归属接口_CustomerUsers")]
    public class CustomerUsersController : ControllerBase
    {
        private readonly CustomerUserApp _cstmrUserApp;

        //获取详情
        [HttpGet]
        public Response<CustomerUser> Get(decimal id)
        {
            var result = new Response<CustomerUser>();
            try
            {
                result.Result = _cstmrUserApp.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 添加客户归属以及关联联系人
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response AddWithRelated(AddCustomerUserWithRelatedReq req)
        {
            var result = new Response();
            try
            {
                CustomerUser cstmrUser = req.MapTo<CustomerUser>();
                List<CustomerContract> cstmrContracts = new List<CustomerContract>();
                foreach (BaseCustomerContractReq addCstmrContractReq in req.CustomerContracts)
                {
                    CustomerContract cstmrContract = req.MapTo<CustomerContract>();
                    cstmrContracts.Add(cstmrContract);
                }
                _cstmrUserApp.AddWithRelated(cstmrUser, cstmrContracts);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 添加客户归属
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response Add(AddCustomerUserReq req)
        {
            var result = new Response();
            try
            {
                CustomerUser cstmrUser = req.MapTo<CustomerUser>();
                _cstmrUserApp.Add(cstmrUser);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        //修改
        [HttpPost]
        public Response Update(UpdateCustomerUserReq updateCstmrUserReq)
        {
            var result = new Response();
            try
            {
                _cstmrUserApp.Update(updateCstmrUserReq);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery] QueryCustomerUserListReq request)
        {
            return await _cstmrUserApp.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _cstmrUserApp.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public CustomerUsersController(CustomerUserApp cstmrUserApp)
        {
            _cstmrUserApp = cstmrUserApp;
        }
    }
}
