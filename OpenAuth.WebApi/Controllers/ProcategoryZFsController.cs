using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 政府产品项目标准接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "政府产品项目标准接口_ProcategoryZFs")]
    public class ProcategoryZFsController : ControllerBase
    {
        private readonly ProcategoryZFApp _app;
        
        //获取详情
        [HttpGet]
        public Response<ProcategoryZF> Get(decimal id)
        {
            var result = new Response<ProcategoryZF>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add([FromBody] AddProcategoryZFReq req)
        {
            var result = new Response();
            try
            {
                ProcategoryZF procategoryZF = req.MapTo<ProcategoryZF>();
                _app.Add(procategoryZF);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update([FromBody] UpdateProcategoryZFReq req)
        {
            var result = new Response();
            try
            {
                ProcategoryZF procategoryZF = req.MapTo<ProcategoryZF>();
                _app.Update(procategoryZF);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QueryProcategoryZFListReq request)
        {
            return await _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody]decimal[] ids)
        {
            var result = new Response();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public ProcategoryZFsController(ProcategoryZFApp app) 
        {
            _app = app;
        }
    }
}
