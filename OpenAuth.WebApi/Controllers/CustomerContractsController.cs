using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 客户联系人接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "客户_客户联系人接口_CustomerContracts")]
    public class CustomerContractsController : ControllerBase
    {
        private readonly CustomerContractApp _cstmrContractApp;

        //获取详情
        [HttpGet]
        public Response<CustomerContract> Get(decimal id)
        {
            var result = new Response<CustomerContract>();
            try
            {
                result.Result = _cstmrContractApp.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add(AddCustomerContractReq req)
        {
            var result = new Response();
            try
            {
                CustomerContract cstmrContract =req.MapTo<CustomerContract>();
                _cstmrContractApp.Add(cstmrContract);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update(UpdateCustomerContractReq obj)
        {
            var result = new Response();
            try
            {
                _cstmrContractApp.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QueryCustomerContractListReq request)
        {
            return await _cstmrContractApp.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _cstmrContractApp.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public CustomerContractsController(CustomerContractApp app) 
        {
            _cstmrContractApp = app;
        }
    }
}
