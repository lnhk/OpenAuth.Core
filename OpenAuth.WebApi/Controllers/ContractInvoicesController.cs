using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.QueryObj;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 合同与发票接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "合同与发票接口_ContractInvoices")]
    public class ContractInvoicesController : ControllerBase
    {
        private readonly ContractInvoiceApp _app;

        /// <summary>
        /// 获取某销售开票金额及历史金额
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Response getXsFpJe()
        {
            var result = new Response<object>();
            result.Result = _app.getXsFpJe();
            return result;
        }

        /// <summary>
        /// 获取登录账号开票记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<TableResp<ContractInvoice>> getXsOpenInvoiceHisRecords([FromQuery] QueryOpenInvoiceHisRecordReq request)
        {
            return _app.getXsOpenInvoiceHisRecords(request);
        }

        /// <summary>
        /// 获取某销售未开的发票列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public Task<TableResp<Contract>> getXsToBeInvoiceDRecords([FromQuery] QueryToBeInvoiceDRecordsReq request)
        {
            return _app.getXsToBeInvoiceDRecords(request);
        }

        /// <summary>
        /// 申请开票
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response applyInvoice([FromBody] ApplyInvoiceReq req) 
        {
            var result = new Response();
            try
            {
                _app.applyInvoice(req);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
        
        /// <summary>
        /// 作废发票
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response voidInvoice(decimal invoiceId) 
        {
            var result = new Response();
            try
            {
                _app.voidInvoice(invoiceId);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 申请发票冲红
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        [HttpPost]
        public Response redInkInvoice(decimal invoiceId)
        {
            var result = new Response();
            try
            {
                _app.redInkInvoice(invoiceId);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 财务获取待开/已开的发票列表
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public Task<TableData> getFinancialInvoiceList([FromQuery] QueryContractInvoiceListReq request)
        {
            return _app.getFinancialInvoiceList(request);
        }

        /// <summary>
        /// 财务审核发票
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response financialVerifyInvoice([FromBody] VerifyInvoiceReq req) 
        {
            var result = new Response();
            try
            {
                _app.financialVerifyInvoice(req);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public ContractInvoicesController(ContractInvoiceApp app) 
        {
            _app = app;
        }
    }
}
