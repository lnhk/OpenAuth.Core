using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aspose.Words.Fonts;
using Aspose.Words.Loading;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Caching.Memory;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.XWPF.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OpenAuth.App;
using OpenAuth.App.Common;
using OpenAuth.App.Common.Entity;
using OpenAuth.App.Common.Request;
using OpenAuth.App.Entity;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Enum;
using OpenAuth.Repository.Enum.ContractEnum;
using OpenAuth.Repository.Enum.SumpleEnum;
using Spire.Pdf;
using Spire.Pdf.General.Find;
using Spire.Pdf.Graphics;
using ReportWay = OpenAuth.Repository.Enum.ContractEnum.ReportWay;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 合同管理接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "合同_合同管理接口")]
    public class ContractsController : ControllerBase
    {
        private readonly ContractApp _contractApp;
        private CustomerApp _cstmrApp;
        private CustomerUserApp _cstmrUserApp;
        private UserManagerApp _userApp;
        private SampleApp _spApp;
        private SampleDataApp _spDataApp;
        private CategoryApp _categoryApp;
        private FactorApp _factorApp;
        private InvoiceApp _invoiceApp;
        private ObsoleteReasonApp _obsoleteReasonApp;
        private SampleTaskListDataApp _spTaskListDataApp;
        private ContractUpDataApp _contractUpDataApp;
        private CacheContext _cache;
        private SysLogApp _sysLogApp;
        private BaseDataHelper _baseDataHelper;
        private WeChatHelper _wcHelper;
        private ExtensionHelper _exHelper;
        private IAuth _auth;

        public ContractsController(ContractApp contractApp, CustomerApp cstmrApp, CustomerUserApp cstmrUserApp,
            UserManagerApp userApp, SampleApp spApp, SampleDataApp spDataApp, CategoryApp categoryApp,
            FactorApp factorApp, ObsoleteReasonApp obsoleteReasonApp, SampleTaskListDataApp spTaskListDataApp, 
            InvoiceApp invoiceApp, SysLogApp sysLogApp, ContractUpDataApp contractUpDataApp,
            BaseDataHelper baseDataHelper, WeChatHelper wcHelper, ExtensionHelper exHelper, IAuth auth)
        {
            _contractApp = contractApp;
            _cstmrApp = cstmrApp;
            _cstmrUserApp = cstmrUserApp;
            _userApp = userApp;
            _spApp = spApp;
            _spDataApp = spDataApp;
            _categoryApp = categoryApp;
            _factorApp = factorApp;
            _obsoleteReasonApp = obsoleteReasonApp;
            _spTaskListDataApp = spTaskListDataApp;
            _invoiceApp = invoiceApp;
            _contractUpDataApp = contractUpDataApp;
            _sysLogApp = sysLogApp;
            _baseDataHelper = baseDataHelper;
            _wcHelper = wcHelper;
            _exHelper = exHelper;
            _auth = auth;
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions()));
        }

        /// <summary>
        /// 添加合同,并同时修改关联的样品,最后返回合同id
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response<decimal> Add([FromBody] AddContractReq req)
        {
            var result = new Response<decimal>();
            try
            {
                Contract contract = req.MapTo<Contract>();
                _contractApp.GeneratorBum(contract);
                _contractApp.AddWithRelatedNoSave(contract);
                _exHelper.Save();
                result.Result = contract.Id;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
                result.Result = 0;
            }
            return result;
        }

        /// <summary>
        /// 修改合同
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response Update([FromBody] UpdateContractReq req)
        {
            var result = new Response();
            try
            {
                Contract contract = req.MapTo<Contract>();
                _contractApp.Update(contract);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载合同列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery] QueryContractListReq request)
        {
            return await _contractApp.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids">合同id数组</param>
        /// <returns></returns>
        [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _contractApp.Delete(ids);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 撤销所有某合同内的所有修改
        /// </summary>
        [HttpPost]
        public Response RevokeSampleAllUp([FromBody] decimal id)
        {
            var result = new Response();
            try
            {
                _contractApp.RevokeSampleAllUp(id);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 获取合同修改内容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public Response<Contract> GetUpContract([FromQuery] decimal contractId)
        {
            var result = new Response<Contract>();
            try
            {
                result.Result = _contractUpDataApp.getUpContract(contractId);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 合同变更申请
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public Response ChangeContractApply([FromBody] UpdateContractReq req)
        {
            var result = new Response();
            try
            {
                _contractApp.ChangeContractApply(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 确认合同变更
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public Response AckSubmitContract(decimal ContractId)
        {
            var result = new Response();
            try
            {
                _contractApp.AckSubmitContract(ContractId);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 导出合同
        /// </summary>
        /// <param name="req">导出合同请求</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExportContract([FromBody] ExportContractReq req)
        {
            PhysicalFileResult fresult;
            PdfDocument doc = new PdfDocument();
            try
            {
                Contract contract = _contractApp.Get(req.ContractId);
                List<Sample> samples = _spApp.GetByContractId(req.ContractId);

                bool isSame = req.IsSame != null ? Convert.ToBoolean(req.IsSame) : false;
                bool isStampQFZ = req.IsSame != null ? Convert.ToBoolean(req.isStampQFZ) : false;

                //CtTypeEnum 环境
                if (contract.ct_type == Convert.ToInt32(CtTypeEnum.HJ).ToString())
                {
                    ExptEnvirmt(contract, isSame, req.SignDate);
                }
                else
                {
                    ExptFood(contract, req.SignDate, isStampQFZ: isStampQFZ);
                }

                if (req.zeal != null || req.person != null)
                {

                    doc.LoadFromFile($"D:\\hklims\\tempcontract\\{contract.ct_number}.pdf");
                    var page = doc.Pages.Add();
                    doc.Pages.Remove(page);
                    Image sealmg = Image.FromFile(@"D:\hklims\image\contract.png");
                    float sealwidth = 130;
                    float sealheight = 130;
                    bool isNullZeal = req.zeal == null;
                    bool isNullPerson = req.person == null;
                    foreach (PdfPageBase item in doc.Pages)
                    {
                        PdfTextFind[] finds;
                        if (req.zeal != null)
                        {
                            finds = item.FindText("检验检测报告凭证").Finds;
                            if (finds.Length > 0)
                            {
                                PdfTextFind find = finds[0];
                                float y = find.Position.Y - 50;
                                if (find.Position.Y - 50 < 0)
                                {
                                    y = find.Position.Y;
                                }
                                if (y + sealheight > item.Canvas.Size.Height)
                                {
                                    y = item.Canvas.Size.Height - sealheight - 50;
                                }
                                item.Canvas.DrawImage(PdfImage.FromImage(sealmg), find.Position.X, y, sealwidth, sealheight);
                                isNullZeal = true;
                            }
                        }

                        if (req.person != null)
                        {
                            if (System.IO.File.Exists($"D:\\hklims\\idiograph\\{contract.beforeusername}.png"))
                            {
                                Image sealmgp = Image.FromFile($"D:\\hklims\\idiograph\\{contract.beforeusername}.png");
                                finds = item.FindText("承检方/人签字").Finds;
                                if (finds.Length > 0)
                                {
                                    PdfTextFind find = finds[0];
                                    float y = find.Position.Y - 10;
                                    float bas = 35.0F / (sealmgp.Height * 1.0F);
                                    sealheight = sealmgp.Height * bas;
                                    sealwidth = sealmgp.Width * bas;
                                    if (sealwidth > 68.5)
                                    {
                                        float wbas = 68.5F / sealwidth;
                                        sealwidth = wbas * sealwidth;
                                        sealheight = wbas * sealheight;
                                    }
                                    item.Canvas.DrawImage(PdfImage.FromImage(sealmgp), find.Position.X + 70, y, sealwidth, sealheight);
                                    sealmgp.Dispose();
                                    isNullPerson = true;
                                }
                            }
                            else
                            {
                                isNullZeal = true;
                            }
                        }

                        if (isNullZeal && isNullPerson)
                        {
                            break;
                        }
                    }
                    sealmg.Dispose();
                    doc.SaveToFile($"D:\\hklims\\tempcontract\\{contract.ct_number}s.pdf");
                    doc.Close();
                }
                string filename;
                if (req.zeal != null || req.person != null)
                {
                    filename = $"{contract.ct_number}s.pdf";
                }
                else
                {
                    filename = $"{contract.ct_number}.pdf";
                }
                var provider = new FileExtensionContentTypeProvider();
                var mime = provider.Mappings[".pdf"];
                fresult = new PhysicalFileResult($"D:\\hklims\\tempcontract\\{filename}", mime);
                fresult.FileDownloadName = $"{contract.ct_number}.pdf";
                return fresult;
            }
            catch (Exception ex)
            {
                doc.Close();
                return Content(ex.Message);
            }
        }

        /// <summary>
        /// 导出报价单
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public FileContentResult ExportPriceExcel([FromBody] PriceExcelReq req)
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            Contract contract = _contractApp.Get(req.ContractId);
            User user = _userApp.Get(contract.beforeuser);
            List<Sample> samples = _spApp.GetByContractId(contract.Id);
            var package = new ExcelPackage();
            var workbook = package.Workbook;
            int index = 1;
            foreach (Sample sample in samples)
            {
                var worksheet = package.Workbook.Worksheets.Add(index + sample.sp_s_7);
                //worksheet.Cells.Style.ShrinkToFit = true;//单元格自动适应大小
                worksheet.Cells.Style.WrapText = true;//自动换行
                worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;//水平居中
                worksheet.Cells[1, 1].Style.Font.Bold = true;//字体为粗体
                worksheet.Cells[10, 1, 10, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;//水平居中
                worksheet.Cells[10, 1, 10, 8].Style.Font.Bold = true;//字体为粗体
                worksheet.Column(2).Width = 20;//设置项目列宽
                worksheet.Column(3).Width = 60;//设置方法列宽
                worksheet.Column(4).Width = 20;//设置资质列宽
                worksheet.Cells[1, 1, 1, 8].Merge = true;
                worksheet.Cells[1, 1].Value = "辽宁惠康检测评价技术有限公司报价单";
                worksheet.Cells[2, 1, 2, 4].Merge = true;
                worksheet.Cells[2, 1].Value = "测试所需样品量:";
                worksheet.Cells[2, 5, 2, 8].Merge = true;
                worksheet.Cells[2, 5].Value = "测试样品邮寄要求:";
                worksheet.Cells[3, 1, 3, 8].Merge = true;
                worksheet.Cells[3, 1].Value = string.Format("测试样品寄送地址：沈阳市浑南新区金仓路10-1号  辽宁惠康检测评价技术有限公司  {0}（收）", user.DisplayName);
                worksheet.Cells[2, 1, 2, 4].Merge = true;
                worksheet.Cells[2, 1].Value = "测试所需样品量:";
                worksheet.Cells[2, 5, 2, 8].Merge = true;
                worksheet.Cells[2, 5].Value = "测试样品邮寄要求:";

                worksheet.Cells[4, 1, 4, 4].Merge = true;
                worksheet.Cells[4, 1].Value = "联系人: " + contract.ct_entrust_name;
                worksheet.Cells[4, 5, 4, 8].Merge = true;
                worksheet.Cells[4, 5].Value = "客户名称:" + contract.ct_entrust_company;

                worksheet.Cells[5, 1, 5, 4].Merge = true;
                worksheet.Cells[5, 1].Value = "传真: ";
                worksheet.Cells[5, 5, 5, 8].Merge = true;
                worksheet.Cells[5, 5].Value = "地址:" + contract.ct_entrust_company_address;


                worksheet.Cells[6, 1, 6, 4].Merge = true;
                worksheet.Cells[6, 1].Value = "电话:";
                worksheet.Cells[6, 5, 6, 8].Merge = true;
                worksheet.Cells[6, 5].Value = "联系人:";

                worksheet.Cells[7, 1, 7, 4].Merge = true;
                worksheet.Cells[7, 1].Value = "邮箱:";
                worksheet.Cells[7, 5, 7, 8].Merge = true;
                worksheet.Cells[7, 5].Value = "传真:";

                worksheet.Cells[8, 1, 8, 4].Merge = true;
                worksheet.Cells[8, 1].Value = "报价日期:" + DateTime.Now.ToString("yyyy-MM-dd");
                worksheet.Cells[8, 5, 8, 8].Merge = true;
                worksheet.Cells[8, 5].Value = "电话/移动电话:" + user.Phone;

                worksheet.Cells[9, 1, 9, 4].Merge = true;
                worksheet.Cells[9, 1].Value = "报价有效期: 30天";
                worksheet.Cells[9, 5, 9, 8].Merge = true;
                worksheet.Cells[9, 5].Value = "邮箱:" + user.Email;

                worksheet.Cells[10, 1].Value = "序号";
                worksheet.Cells[10, 2].Value = "测试项目";
                worksheet.Cells[10, 3].Value = "测试方法";
                worksheet.Cells[10, 4].Value = "资质";
                worksheet.Cells[10, 5].Value = "标准报价";
                worksheet.Cells[10, 6].Value = "数量";
                //worksheet.Cells[10, 7].Value = "系数";
                worksheet.Cells[10, 7].Value = "测试时间(工作日)";
                worksheet.Cells[10, 8].Value = "金额";
                var rownumber = 11;

                List<SampleData> sampleDatas = _spDataApp.GetBySampleId(sample.Id);
                foreach (SampleData sampledata in sampleDatas)
                {
                    worksheet.Cells[rownumber, 1].Value = rownumber - 10;
                    worksheet.Cells[rownumber, 2].Value = sampledata.spdata_1;
                    worksheet.Cells[rownumber, 3].Value = sampledata.spdata_4;
                    worksheet.Cells[rownumber, 4].Value = GetZz(sampledata);
                    float f = 0;
                    if (float.TryParse(sampledata.cose, out f))
                    {
                        worksheet.Cells[rownumber, 5].Style.Numberformat.Format = "0";
                        worksheet.Cells[rownumber, 5].Value = f;
                    }
                    else
                    {
                        worksheet.Cells[rownumber, 5].Value = sampledata.cose;
                    }

                    //worksheet.Cells[rownumber, 6].Value = sampledata.CoseDiscount;
                    //worksheet.Cells[rownumber, 7].Value = sampledata.CoseDiscount;
                    worksheet.Cells[rownumber, 7].Value = sampledata.cycle + "";
                    if (float.TryParse(sampledata.price, out f))
                    {
                        worksheet.Cells[rownumber, 8].Style.Numberformat.Format = "0";
                        worksheet.Cells[rownumber, 8].Value = f;
                    }
                    else
                    {
                        worksheet.Cells[rownumber, 8].Value = sampledata.price;
                    }
                    rownumber++;
                }
                worksheet.Cells[rownumber, 1].Value = "总价";
                worksheet.Cells[rownumber, 5].Value = sample.totalprice;
                rownumber++;
                worksheet.Cells[rownumber, 1].Value = "备注：分包不打折";
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "1. 加急服务需要提前与实验室确认，并收取相应加急费用。";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "2. 请于测试开始前安排付款并将付款凭证传真或发邮件至我公司。";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "3. 名头:辽宁惠康检测评价技术有限公司";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "4. 银行账户:286969158141";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "5. 开户银行:中国银行沈阳八经街支行";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "如果您同意本司以上报价，请签名盖章后将此件与测试委托合同一起传回本司 。";
                rownumber += 1;
                worksheet.Cells[rownumber, 1, rownumber, 8].Merge = true;
                worksheet.Cells[rownumber, 1].Value = "公司名称（公章）：____________________ 签名:  ____________________";
                index += 1;
            }
            return File(package.GetAsByteArray(), "application/octet-stream", $"辽宁惠康检测评价技术有限公司-【{contract.ct_entrust_company + (samples.Count == 1 ? "-" + samples[0].sp_s_7 : "")}】报价单{DateTime.Now.ToString("yyyyMMdd")}.xlsx");
        }

        /// <summary>
        /// 复制历史合同/引用合同
        /// </summary>
        /// <param name="req">复制合同请求</param>
        /// <returns></returns>
        [HttpPost]
        public Response CopyContract([FromBody] CopyContractReq req)
        {
            Response resp = new Response();
            try
            {
                //赋值contractsample  时间戳。保存样品时添加
                DateTime startTime = TimeZoneInfo.ConvertTimeFromUtc(new DateTime(1970, 1, 1), TimeZoneInfo.Local); // 当地时区
                long contractsample = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数

                List<Contract> contracts = _contractApp.GetByCtNum(req.CtNumber);
                Contract contract = contracts[0].MapTo<Contract>();
                if (contracts.Count == 0)
                {
                    resp.Code = 500;
                    resp.Message = "找不到此编号的合同信息！";
                    return resp;
                }
                int xh = 1;
                try
                {
                    BaseData basedata = _baseDataHelper.GetBaseData();

                    List<Sample> samples = _spApp.GetByContractId(contracts[0].Id);
                    foreach (Sample sample in samples)
                    {
                        List<SampleData> tempSampleDatas = new List<SampleData>();
                        sample.copySample = sample.sp_s_code;
                        List<SampleData> sampleDatas = _spDataApp.GetBySampleId(sample.Id);
                        for (int i = 0; i < sampleDatas.Count; i++)
                        {
                            SampleData sampleData = sampleDatas[i];
                        }
                        double fenbao_cose = 0;
                        foreach (SampleData sampleData in sampleDatas)
                        {
                            //去除该去除的
                            sampleData.Id = 0;
                            sampleData.sp_id = null;
                            sampleData.spdata_15 = null;
                            sampleData.spdata_15_2 = null;
                            sampleData.spdata_15_3 = null;
                            sampleData.spdata_15_4 = null;
                            //sampleData.Id = null;
                            sampleData.testing_personnel = null;
                            sampleData.project_status = null;
                            sampleData.spdata_time = null;
                            sampleData.spdata_2 = null;
                            sampleData.prewarning_value = null;
                            sampleData.feedback = null;
                            //item2.Quantitation = null;
                            //item2.Spdata10 = null;
                            // item2.Spdata11 = null;
                            sampleData.result = null;
                            sampleData.OrgRecordId = null;
                            //判断检测项目和检测方法、资质是否一致

                            string sql = string.Empty;
                            Standard hkStandard = basedata.Standards.Where(o => o.Id.Equals(sampleData.spdata_5_id)).FirstOrDefault();
                            Factor hkFactor = basedata.Factors.Where(o => o.Id.Equals(sampleData.spdata_1_id)).FirstOrDefault();

                            FactorStandard factorStandard = basedata.FactorStandards.Where(o => { return o.factorid.Equals(sampleData.spdata_1_id) && o.standardid.Equals(sampleData.spdata_5_id); }).FirstOrDefault();
                            List<BaseQualition> baseQualitions = _factorApp.GetBaseQualitionForSampleData(sampleData.ct_id);

                            if (baseQualitions.Count == 1)
                            {
                                sampleData.quantitation = baseQualitions[0].qualition;  //定量限
                            }

                            if (factorStandard == null)
                            {
                                resp.Message += sample.sp_s_code + " :\n 序号：" + sampleData.px + "\t项目:" + sampleData.spdata_1 + "\n方法:" + sampleData.spdata_5 + " 关联数据失效；\n";
                                sampleData.spdata_label = 2;
                            }
                            else if (hkFactor == null || hkStandard == null)
                            {
                                resp.Message += sample.sp_s_code + " :\n 序号：" + sampleData.px + "\t项目:" + sampleData.spdata_1 + "\n方法:" + sampleData.spdata_5 + " 方法或项目失效；\n";
                                sampleData.spdata_label = 2;
                            }
                            else if (!hkFactor.factorname.Equals(sampleData.spdata_1) || !hkStandard.testfunc.Equals(sampleData.spdata_4))
                            {
                                resp.Message += sample.sp_s_code + " :\n 序号：" + sampleData.px + "\t项目:" + sampleData.spdata_1 + "\n方法:" + sampleData.spdata_5 + " 方法或项目名称不能匹配；\n";
                                sampleData.spdata_label = 2;
                            }
                            else if (factorStandard.IsStop == true)
                            {
                                resp.Message += sample.sp_s_code + " :\n 序号：" + sampleData.px + "\t项目:" + sampleData.spdata_1 + "\n方法:" + sampleData.spdata_5 + " 已停用；\n";
                                sampleData.spdata_label = 2;
                            }
                            else if (factorStandard.StampCma != sampleData.stampCMA || factorStandard.stampCNAS != sampleData.stampCNAS || factorStandard.stampCATL != sampleData.stampCATL)
                            {
                                resp.Message += sample.sp_s_code + " :\n 序号：" + sampleData.px + "\t项目:" + sampleData.spdata_1 + "\n方法:" + sampleData.spdata_5 + " 资质已更新；\n";
                                sampleData.stampCMA = factorStandard.StampCma;
                                sampleData.stampCNAS = factorStandard.stampCNAS;
                                sampleData.stampCATL = factorStandard.stampCATL;
                                // item2.SpdataLabel = 2;
                            }
                            else
                            {
                                sampleData.cycle = hkStandard.cycle;
                                sampleData.spdata_13_id = hkStandard.department;
                                sampleData.spdata_13 = hkStandard.departmentName;
                                sampleData.sample_count = hkStandard.sample_count;
                                sampleData.range_of_application = hkStandard.range_of_application;  //适用范围 
                            }

                            if (sampleData.spdata_13 == "fenbao")
                            {
                                fenbao_cose += Convert.ToDouble(sampleData.cose);
                            }
                            tempSampleDatas.Add(sampleData);
                        }
                        sample.Id = 0;
                        sample.all_commit_time = null;
                        sample.sp_state = Convert.ToString(SampleState.草稿);
                        sample.sp_s_code = null;
                        sample.xh = xh;
                        sample.lastprice = (decimal)fenbao_cose;
                        //item.SpCsW1 = "同委托单位";
                        //item.SpCsW2 = "同委托单位地址";
                        //item.SpCs4 = "同委托单位地址";
                        //item.SpCs5 = "同委托单位";
                        sample.SubpackageReport = null;
                        sample.sp_s_26 = null;
                        sample.sp_s_36 = DateTime.Now.ToString("yyyy-MM-dd");
                        sample.sp_s_qr = null;
                        sample.sp_s_pic = null;
                        sample.sp_s_r = null;
                        sample.contract_sample = (decimal)contractsample;
                        sample.release_time_hksample = null;
                        sample.is_feedback = null;
                        sample.sp_s_41 = null;
                        sample.OrgFile = null;
                        sample.OrgFileDate = null;
                        sample.BatchNumber = null;
                        xh += 1;
                        _spApp.AddWithRelatedNoSave(sample, sampleDatas);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

                //保存合同
                //contracts[0].Id = null;
                contracts[0].CommitTime = null;

                Customer cstmr = _cstmrApp.Get(contracts[0].hkcompany_id);

                if (cstmr != null)
                {
                    contracts[0].ct_entrust_company = cstmr.CompanyName;
                    contracts[0].ct_entrust_company_address = cstmr.CompanyAddress;
                    //contracts[0].beforeuser = cstmr.Person;
                    //contracts[0].ct_inspection_name = cstmr.ct_inspection_name ?? "/";
                }
                else
                {
                    contracts[0].ct_entrust_company = null;
                    contracts[0].ct_entrust_company_address = null;
                    contracts[0].beforeuser = null;
                    contracts[0].ct_inspection_name = null;
                }

                List<CustomerUser> cstmrUsers = _cstmrUserApp.GetByComapnyId(contracts[0].hkcompany_id);

                if (cstmrUsers.Count > 0)
                {
                    contracts[0].ct_entrust_name = cstmrUsers[0].Username;
                    contracts[0].ct_entrust_phone = cstmrUsers[0].Phone;
                    contracts[0].ct_entrust_email = String.IsNullOrEmpty(cstmrUsers[0].Email) ? "/" : cstmrUsers[0].Email;

                }
                else
                {
                    contracts[0].ct_entrust_name = null;
                    contracts[0].ct_entrust_phone = null;
                    contracts[0].ct_entrust_email = "/";
                }
                contracts[0].Id = 0;//清空id值
                contracts[0].commituser = null;
                contracts[0].endcose_user = null;
                contracts[0].sign_time = null;
                contracts[0].contract_sample = contractsample;
                contracts[0].ct_state = CtStateEnum.草稿.ToString();
                contracts[0].copyCtCode = contract.ct_number;
                contracts[0].ct_number = null;
                contracts[0].ct_serial_number = null;
                contracts[0].ct_sqr = null;
                contracts[0].approval = null;
                contracts[0].money_back = false;
                //contracts[0].ApplyInvoice = 1;
                contracts[0].InvoiceState = InvoiceStateEnum.申请开票.ToString();
                contracts[0].ct_inspection_email = false;
                contracts[0].ct_business_license = false;
                contracts[0].invoice_id = null;
                contracts[0].ct_inspection_company_address = null;
                contracts[0].ct_inspection_name = null;
                contracts[0].ct_annual_sales = null;
                contracts[0].update_date = null;
                contracts[0].sub_package_pay = null;
                contracts[0].sub_report_time = null;
                contracts[0].backcose = 0;
                contracts[0].opencose = 0;
                contracts[0].no_opencose = contracts[0].ct_pay_cose;
                contracts[0].no_backcose = contracts[0].ct_pay_cose;
                contracts[0].openbackcose = 0;
                contracts[0].openobackcose = 0;
                contracts[0].endcose = false;
                contracts[0].Invoice = false;
                contracts[0].ct_file = null;
                contracts[0].sys_totalprice = contracts[0].sys_totalprice;
                //如果是客服权限引用合同，不更改所属人。
                if (_auth.GetCurrentUser().Resources.Find((r) => r.Id == Define.Permission.FunctionMenu.全部客户) != null)
                {
                    contracts[0].beforeuser = _auth.GetCurrentUser().User.Id;
                }
                contracts[0].applyInvoiceuser = null;

                if (req.CopyContractEnum == CopyContractEnum.复制合同)
                {
                    //复制合同生成合同编号 
                    _contractApp.GetNextContractNumber(contracts[0], (Convert.ToInt32(contracts[0].ct_type)).ToString(), 5);
                    _contractApp.AddNoSave(contracts[0]);

                    resp.Code = 200;
                    if (!string.IsNullOrEmpty(resp.Message))
                    {
                        resp.Message = "以下项目未能复制\n" + resp.Message.Replace(contract.ct_number, contracts[0].ct_number);
                    }
                    resp.Message += "引用添加成功！新合同编号为:" + contracts[0].ct_number;
                }
                else
                {
                    //引用合同生成合同编号 
                    for (int j = 0; j < 100; j++)
                    {
                        contractsample = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数 
                        _contractApp.GetNextContractNumber(contract, contracts[0].ct_type, 5);
                        contracts[0].contract_sample = contractsample;
                        _contractApp.AddNoSave(contracts[0]);
                    }
                    resp.Code = 200;
                    if (!string.IsNullOrEmpty(resp.Message))
                    {
                        resp.Message = "以下项目未能复制\n" + resp.Message.Replace(contract.ct_number, contracts[0].ct_number);
                    }
                    resp.Message += "引用添加成功！新合同编号为:" + contracts[0].ct_number;
                }
                _exHelper.Save();
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return resp;
        }

        /// <summary>
        /// 作废合同
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response AbandonContract([FromBody] AbandonContractReq req)
        {
            Response resp = new Response();
            try
            {
                Contract contract = _contractApp.Get(req.ContractId);
                List<Sample> samples = _spApp.GetByContractId(contract.Id);

                var hasFlowSample = samples.Where(a => Convert.ToInt32(a.sp_state) >= Convert.ToInt32(SampleState.已流转)).ToList();
                bool isRecordObsolete = false; //是否记录作废合同信息
                var labs = new HashSet<string>(); //记录作废实验室
                var sampleCodes = new HashSet<string>(); //记录已流转的样品编号

                if (Convert.ToInt32(contract.ct_state) >= Convert.ToInt32(CtStateEnum.工作中) && hasFlowSample.Any())
                {
                    isRecordObsolete = true;
                }
                List<string> list = new List<string>();

                if (isRecordObsolete)
                {
                    foreach (var flowSample in hasFlowSample)
                    {
                        ObsoleteReason obsoleteReason = new ObsoleteReason();
                        obsoleteReason.code = contract.ct_number;
                        obsoleteReason.obsolete_type = Convert.ToInt32(ObsoleteEnum.合同作废).ToString();
                        obsoleteReason.obsolete_reason = req.Messge;
                        obsoleteReason.contract_id = contract.Id.ToString();
                        obsoleteReason.state = Convert.ToInt32(HkObsoleteReasonEnum.审核中).ToString();
                        obsoleteReason.contract_state = contract.ct_state;
                        obsoleteReason.sample_ids = flowSample.Id.ToString();
                        obsoleteReason.sample_code = flowSample.sp_s_code;
                        obsoleteReason.sample_name = flowSample.sp_s_7;
                        obsoleteReason.sample_state_code = flowSample.sp_state;
                        _obsoleteReasonApp.Add(obsoleteReason);
                        foreach (SampleData sampledata in _spDataApp.GetBySampleId(flowSample.Id))
                        {
                            labs.Add(sampledata.spdata_13_id);
                        }
                        sampleCodes.Add(flowSample.sp_s_code);
                        _spTaskListDataApp.UpdateForAbandonNoSave(flowSample.Id);
                    }

                    contract.ct_state = Convert.ToInt32(CtStateEnum.已作废).ToString();
                    _contractApp.UpdateNoSave(contract);

                    foreach (var sample in samples)
                    {
                        sample.sp_state = Convert.ToInt32(SampleState.已作废).ToString();
                        _spApp.UpdateNoSave(sample);
                    }
                }
                if (isRecordObsolete)
                {
                    _wcHelper.SendLIMSMessage($"协议[{contract.ct_number}]及样品和检测项目已作废", null, "ShanMu");
                    StringBuilder builder = new StringBuilder();
                    foreach (var item in labs)
                    {
                        if (WxDepartment.LibraryDic.ContainsKey(item))
                        {
                            builder.Append(WxDepartment.LibraryDic[item] + "|");
                        }
                    }
                    builder.Append("7");
                    _wcHelper.SendLIMSMessage($"样品:{string.Join(",", sampleCodes)}已作废", builder.ToString(), null);
                }
                _exHelper.Save();
                return resp;
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
                return resp;
            }
        }

        /// <summary>
        /// 获取合同信息
        /// </summary>
        /// <param name="contractId">合同id</param>
        /// <returns></returns>
        [HttpGet]
        public Response<Contract> GetContract([FromQuery] decimal contractId)
        {
            Response<Contract> resp = new Response<Contract>();
            try
            {
                resp.Result = _contractApp.Get(contractId);
                return resp;
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
                return resp;
            }
        }

        /// <summary>
        /// 获取合同关联的发票信息
        /// </summary>
        /// <param name="contractId">合同id</param>
        /// <returns></returns>
        //[HttpGet]
        //public Response<Invoice> GetRelatedInvoice([FromQuery] decimal contractId)
        //{
        //    Response<Invoice> resp = new Response<Invoice>();
        //    try
        //    {
        //        Contract contract = _contractApp.Get(contractId);
        //        resp.Result = _invoiceApp.GetByContractId(contractId);
        //        return resp;
        //    }
        //    catch (Exception e)
        //    {
        //        resp.Code = 500;
        //        resp.Message = e.Message;
        //        return resp;
        //    }
        //}

        /// <summary>
        /// 获取合同关联的样品信息和检测项目信息
        /// </summary>
        /// <param name="contractId">合同id</param>
        /// <returns></returns>
        [HttpGet]
        public Response<SampleContractRelated> GetRelatedSampleAndSampleData([FromQuery] decimal contractId)
        {
            Response<SampleContractRelated> resp = new Response<SampleContractRelated>();
            try
            {
                SampleContractRelated sampleContractRelated = new SampleContractRelated();
                List<Sample> samples = _spApp.GetByContractId(contractId);
                foreach (Sample sample in samples)
                {
                    SampleRelated sampleRelated = new SampleRelated();
                    sampleRelated.Sample = sample;
                    sampleRelated.SampleDatas=_spApp.GetRelatedSpData(sample.Id);
                    sampleContractRelated.SampleRelateds = new List<SampleRelated>();
                    sampleContractRelated.SampleRelateds.Add(sampleRelated);
                }
                if (samples.Count != 0)
                {
                    Contract contract = _contractApp.GetBySampleId(samples[0].Id);
                    sampleContractRelated.ct_pay_cose = contract.ct_pay_cose;
                    sampleContractRelated.ct_report_time = contract.ct_report_time;
                    sampleContractRelated.emergenct_fee = contract.emergenct_fee;
                    sampleContractRelated.sub_package_pay = contract.sub_package_pay;
                }
                resp.Result = sampleContractRelated;
                return resp;
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
                return resp;
            }
        }

        /// <summary>
        ///  获取合同关联的操作记录
        /// </summary>
        /// <param name="contractId">合同id</param>
        /// <returns></returns>
        [HttpGet]
        public Response<List<SysLog>> GetRelatedSysLogs([FromQuery] decimal contractId)
        {
            Response<List<SysLog>> resp = new Response<List<SysLog>>();
            try
            {
                resp.Result = _sysLogApp.GetByRecordId(contractId);
                return resp;
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
                return resp;
            }
        }

        private string GetZz(SampleData data)
        {
            string zz = "";
            if (data.stampCMA != null && (bool)data.stampCMA)
            {
                zz += "[CMA]";
            }
            if (data.stampCNAS != null && (bool)data.stampCNAS)
            {
                zz += "[CNAS]";
            }
            if (data.stampCATL != null && (bool)data.stampCATL)
            {
                zz += "[CATL]";
            }
            return zz;
        }

        private void ExptEnvirmt(Contract contract, bool isSame, string signDate)
        {
            List<Sample> samples = _spApp.GetByContractId(contract.Id);
            string tempFilePath = $"D:\\hklims\\template\\temp.docx";
            string outPath = $"D:\\hklims\\tempcontract\\{contract.ct_number}.docx";
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("CtNumber", contract.ct_number);

            using (FileStream stream = System.IO.File.OpenRead(tempFilePath))
            {
                XWPFDocument doc = new XWPFDocument(stream);
                //遍历段落
                try
                {
                    foreach (var para in doc.Paragraphs)
                    {
                        ReplaceKey(para, data);
                    }
                    //遍历表格
                    foreach (var table in doc.Tables)
                    {
                        foreach (var row in table.Rows)
                        {
                            foreach (var cell in row.GetTableCells())
                            {
                                foreach (var para in cell.Paragraphs)
                                {
                                    ReplaceKey(para, data);
                                }
                            }
                        }
                    }
                    XWPFTable firsttable = doc.Tables[0];
                    firsttable.GetRow(2).GetCell(1).SetText(contract.ct_entrust_company_address + "");
                    firsttable.GetRow(2).GetCell(3).SetText(contract.ct_entrust_company_address + "");

                    firsttable.GetRow(3).GetCell(1).SetText(string.IsNullOrEmpty(contract.ct_entrust_company_comm) ? "/" : contract.ct_entrust_company_comm);//共同委托单位
                    firsttable.GetRow(3).GetCell(3).SetText(string.IsNullOrEmpty(contract.ct_entrust_company_address_comm) ? "/" : contract.ct_entrust_company_address_comm);//共同委托单位地址

                    firsttable.GetRow(4).GetCell(1).SetText(samples[0].sp_cs_w_1 + "");
                    firsttable.GetRow(4).GetCell(3).SetText(samples[0].sp_cs_w_2 + "");

                    firsttable.GetRow(5).GetCell(1).SetText(string.IsNullOrEmpty(contract.ct_entrust_name_comm) ? "/" : contract.ct_entrust_name_comm);//受检单位指定负责人
                    firsttable.GetRow(5).GetCell(3).SetText(string.IsNullOrEmpty(contract.ct_entrust_phone_comm) ? "/" : contract.ct_entrust_phone_comm);//电话

                    firsttable.GetRow(6).GetCell(1).SetText(contract.PayCompany + "");//付款单位
                    firsttable.GetRow(6).GetCell(3).SetText(contract.ObjectSamplingAddress + "");//项目采样地址

                    firsttable.GetRow(7).GetCell(1).SetText(contract.ct_entrust_name + "");//联系人
                    firsttable.GetRow(7).GetCell(3).SetText(contract.ct_entrust_phone + "");//联系方式 

                    XWPFParagraph intypepara = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var run = intypepara.CreateRun();
                    run.FontFamily = "Wingdings 2";
                    run.SetText(GetWing(contract.InvoiceType == Convert.ToInt32(InvoiceTypeEnum.不开发票).ToString()));
                    intypepara.CreateRun().SetText("增值税普通发票  ");
                    var run11 = intypepara.CreateRun();
                    run11.FontFamily = "Wingdings 2";
                    run11.SetText(GetWing(contract.InvoiceType == Convert.ToInt32(InvoiceTypeEnum.不开发票).ToString()));
                    intypepara.CreateRun().SetText("增值税专用发票  ");
                    var run12 = intypepara.CreateRun();
                    run12.FontFamily = "Wingdings 2";
                    run12.SetText(GetWing(contract.InvoiceType == Convert.ToInt32(InvoiceTypeEnum.不开发票).ToString()));
                    intypepara.CreateRun().SetText("不开发票");
                    firsttable.GetRow(8).GetCell(1).SetParagraph(intypepara);//发票类型   

                    firsttable.GetRow(9).GetCell(1).SetText(contract.ct_invoice_title + "");//单位名称
                    firsttable.GetRow(9).GetCell(3).SetText(contract.identification_number + "");//识别号
                    firsttable.GetRow(10).GetCell(1).SetText(contract.invoice_address + "、" + contract.invoice_telephone);//单位地址、电话
                    firsttable.GetRow(10).GetCell(3).SetText(contract.opening_bank + "、" + contract.opening_bank_number);//开户行号

                    XWPFParagraph sampletypepara = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetStrByStrArray(contract.TestCategory, sampletypepara, new string[] { "水质", "环境空气", "废气", "土壤" }, false);

                    firsttable.GetRow(11).GetCell(1).SetParagraph(sampletypepara);

                    //样品来源
                    XWPFParagraph sampleorgpara = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var sorgrun = sampleorgpara.CreateRun();
                    sorgrun.FontFamily = "Wingdings 2";
                    sorgrun.SetText(GetWing(contract.SampleSource.Contains("采样")));
                    sampleorgpara.CreateRun().SetText("采样    ");
                    var sorgrun1 = sampleorgpara.CreateRun();
                    sorgrun1.FontFamily = "Wingdings 2";
                    sorgrun1.SetText(GetWing(contract.SampleSource.Contains("送样")));
                    sampleorgpara.CreateRun().SetText("送样    ");
                    var sorgrun2 = sampleorgpara.CreateRun();
                    sorgrun2.FontFamily = "Wingdings 2";
                    sorgrun2.SetText(GetWing(contract.SampleSource.Contains("现场检测")));
                    sampleorgpara.CreateRun().SetText("现场检测");
                    firsttable.GetRow(12).GetCell(1).SetParagraph(sampleorgpara);

                    //样品留样
                    XWPFParagraph samplegpara2 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun2 = samplegpara2.CreateRun();
                    srun2.FontFamily = "Wingdings 2";
                    srun2.SetText(GetWing(contract.RetentionSamples == true));
                    samplegpara2.CreateRun().SetText("留样     ");
                    var srun21 = samplegpara2.CreateRun();
                    srun21.FontFamily = "Wingdings 2";
                    srun21.SetText(GetWing(contract.RetentionSamples == false));
                    samplegpara2.CreateRun().SetText("不留样");
                    firsttable.GetRow(13).GetCell(1).SetParagraph(samplegpara2);

                    //样品储存
                    XWPFParagraph samplegpara3 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetStrByStrArray(contract.SampleStored, samplegpara3, new string[] { "密封", "加固定剂" }, false);
                    firsttable.GetRow(14).GetCell(1).SetParagraph(samplegpara3);

                    //样品状态
                    XWPFParagraph samplegpara4 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetStrByStrArray(contract.SampleState, samplegpara4, new string[] { "液态", "气态", "固态" }, false);
                    firsttable.GetRow(15).GetCell(1).SetParagraph(samplegpara4);

                    //是否需要判定
                    XWPFParagraph samplegpara5 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetYesNoFormat(samplegpara5, contract.IsDecide);
                    //srun5.SetText(sval5);//样品类别
                    firsttable.GetRow(17).GetCell(1).SetParagraph(samplegpara5);

                    //判定依据
                    XWPFParagraph samplegpara6 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun6 = samplegpara6.CreateRun();
                    srun6.FontFamily = "Wingdings 2";
                    srun6.SetText(GetWing(true));
                    samplegpara6.CreateRun().SetText("标准规范");
                    var srun61 = samplegpara6.CreateRun();
                    srun61.SetUnderline(UnderlinePatterns.Single);
                    srun61.SetText(contract.StandardSpecification + "");
                    var srun62 = samplegpara6.CreateRun();
                    srun62.FontFamily = "Wingdings 2";
                    srun62.SetText(GetWing(true));
                    samplegpara6.CreateRun().SetText("委托方指定标准");
                    var srun63 = samplegpara6.CreateRun();
                    srun63.SetUnderline(UnderlinePatterns.Single);
                    srun63.SetText(contract.CompanyStandardSpecification + "");
                    firsttable.GetRow(18).GetCell(1).SetParagraph(samplegpara6);

                    //是否同意分包
                    XWPFParagraph samplegpara7 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetYesNoFormat(samplegpara7, contract.IsSubpackage);
                    firsttable.GetRow(19).GetCell(1).SetParagraph(samplegpara7);

                    //是否同意方法偏离
                    XWPFParagraph samplegpara8 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetYesNoFormat(samplegpara8, contract.methodDeviate);
                    firsttable.GetRow(19).GetCell(3).SetParagraph(samplegpara8);

                    firsttable.GetRow(20).GetCell(1).SetText(contract.SubpackageObject + "");
                    //分包项目是否纳入本单位检测报告
                    XWPFParagraph samplegpara9 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetYesNoFormat(samplegpara9, contract.IsExaminingReport);
                    firsttable.GetRow(20).GetCell(3).SetParagraph(samplegpara9);
                    //CMA
                    XWPFParagraph samplegpara10 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun10 = samplegpara10.CreateRun();
                    srun10.SetText("CMA  ");
                    GetYesNoFormat(samplegpara10, contract.stampCMA);
                    firsttable.GetRow(21).GetCell(1).SetParagraph(samplegpara10);
                    //CNAS
                    XWPFParagraph samplegpara11 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun111 = samplegpara11.CreateRun();
                    srun111.SetText("CNAS  ");
                    GetYesNoFormat(samplegpara11, contract.stampCNAS);
                    firsttable.GetRow(21).GetCell(2).SetParagraph(samplegpara11);
                    //CATL
                    XWPFParagraph samplegpara12 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun121 = samplegpara12.CreateRun();
                    srun121.SetText("CATL  ");
                    GetYesNoFormat(samplegpara12, contract.stampCATL);
                    firsttable.GetRow(21).GetCell(3).SetParagraph(samplegpara12);
                    //报告时间要求
                    XWPFParagraph samplegpara13 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun13 = samplegpara13.CreateRun();
                    srun13.FontFamily = "Wingdings 2";
                    srun13.SetText(GetWing(contract.presentation_nature == Convert.ToInt32(ReportService.标准服务).ToString()));
                    samplegpara13.CreateRun().SetText("标准服务   ");
                    var srun132 = samplegpara13.CreateRun();
                    srun132.FontFamily = "Wingdings 2";
                    srun132.SetText(GetWing(contract.presentation_nature == Convert.ToInt32(ReportService.加急服务).ToString()));
                    samplegpara13.CreateRun().SetText("加急服务");
                    firsttable.GetRow(22).GetCell(1).SetParagraph(samplegpara13);
                    XWPFParagraph reporttime = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var reportrun = reporttime.CreateRun();
                    reportrun.IsBold = true;
                    reportrun.SetText(((DateTime)contract.ct_report_time).ToString("yyyy-MM-dd"));
                    firsttable.GetRow(22).GetCell(3).SetParagraph(reporttime);//报告日期
                    firsttable.GetRow(22).GetCell(5).SetText(contract.sp_cc_18 + "");//报告份数

                    //报告领取
                    XWPFParagraph samplegpara14 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var srun14 = samplegpara14.CreateRun();
                    srun14.FontFamily = "Wingdings 2";
                    srun14.SetText(GetWing(contract.ct_report_receive == Convert.ToInt32(ReportWay.自取).ToString()));
                    samplegpara14.CreateRun().SetText("自取   ");
                    var srun142 = samplegpara14.CreateRun();
                    srun142.FontFamily = "Wingdings 2";
                    srun142.SetText(GetWing(contract.ct_report_receive == Convert.ToInt32(ReportWay.邮寄).ToString()));
                    samplegpara14.CreateRun().SetText("邮寄");
                    firsttable.GetRow(23).GetCell(1).SetParagraph(samplegpara14);
                    firsttable.GetRow(23).GetCell(3).SetText(contract.ct_inspection_name + "");//报告邮寄地址
                    firsttable.GetRow(24).GetCell(1).SetText(contract.ct_pay_cose + "");//费用
                    firsttable.GetRow(25).GetCell(0).SetText(contract.ct_remarl + "");//其它
                    if (string.IsNullOrEmpty(signDate))
                    {
                        firsttable.GetRow(26).GetCell(2).SetText("年    月    日");//其他
                    }
                    else
                    {
                        firsttable.GetRow(26).GetCell(2).SetText(Convert.ToDateTime(signDate).ToString("yyyy年MM月dd日"));//其他
                    }

                    XWPFTable tableContent = doc.Tables[1];
                    int rowindex = 1;
                    tableContent.CreateRow();
                    if (isSame)
                    {
                        int? minXh = samples.Min(o => o.xh);
                        int? maxXh = samples.Max(o => o.xh);
                        string spscode = samples[0].sp_s_code.Substring(0, 11);
                        string sampleName = samples[0].sp_s_7;
                        CT_Tc cttc = tableContent.GetRow(rowindex).GetCell(0).GetCTTc();
                        CT_Tc cttc2 = tableContent.GetRow(rowindex).GetCell(1).GetCTTc();
                        if (cttc.tcPr == null)
                        {
                            cttc.AddNewTcPr();
                        }
                        if (cttc2.tcPr == null)
                        {
                            cttc2.AddNewTcPr();
                        }

                        cttc.tcPr.AddNewVMerge().val = ST_Merge.restart;
                        cttc2.tcPr.AddNewVMerge().val = ST_Merge.restart;
                        List<SampleData> sampleDatas = _spDataApp.GetBySampleId(samples[0].Id);
                        foreach (var sampleData in sampleDatas)
                        {
                            tableContent.GetRow(rowindex).GetCell(2).SetText(sampleData.px + "");
                            tableContent.GetRow(rowindex).GetCell(3).SetText(sampleData.spdata_1 + "");
                            tableContent.GetRow(rowindex).GetCell(4).SetText(sampleData.spdata_5 + "");
                            tableContent.CreateRow();
                            rowindex += 1;
                            cttc = tableContent.GetRow(rowindex).GetCell(0).GetCTTc();
                            cttc2 = tableContent.GetRow(rowindex).GetCell(1).GetCTTc();
                            if (cttc.tcPr == null)
                            {
                                cttc.AddNewTcPr();
                            }
                            if (cttc2.tcPr == null)
                            {
                                cttc2.AddNewTcPr();
                            }
                            cttc.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                            cttc2.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                        }
                        tableContent.GetRow(1).GetCell(0).SetText(spscode + "-" + minXh + "~" + maxXh);
                        tableContent.GetRow(1).GetCell(1).SetText(sampleName);
                    }
                    else
                    {
                        foreach (var sample in samples)
                        {
                            CT_Tc cttc = tableContent.GetRow(rowindex).GetCell(0).GetCTTc();
                            CT_Tc cttc2 = tableContent.GetRow(rowindex).GetCell(1).GetCTTc();
                            if (cttc.tcPr == null)
                            {
                                cttc.AddNewTcPr();
                            }
                            if (cttc2.tcPr == null)
                            {
                                cttc2.AddNewTcPr();
                            }
                            tableContent.GetRow(rowindex).GetCell(0).SetText(sample.xh + "");
                            tableContent.GetRow(rowindex).GetCell(1).SetText(sample.sp_s_7 + "");
                            cttc.tcPr.AddNewVMerge().val = ST_Merge.restart;
                            cttc2.tcPr.AddNewVMerge().val = ST_Merge.restart;

                            List<SampleData> sampleDatas = _spDataApp.GetBySampleId(sample.Id);
                            foreach (var sd in sampleDatas)
                            {

                                tableContent.GetRow(rowindex).GetCell(2).SetText(sd.px + "");
                                tableContent.GetRow(rowindex).GetCell(3).SetText(sd.spdata_1 + "");
                                tableContent.GetRow(rowindex).GetCell(4).SetText(sd.spdata_5 + "");
                                tableContent.CreateRow();
                                rowindex += 1;
                                cttc = tableContent.GetRow(rowindex).GetCell(0).GetCTTc();
                                cttc2 = tableContent.GetRow(rowindex).GetCell(1).GetCTTc();
                                if (cttc.tcPr == null)
                                {
                                    cttc.AddNewTcPr();
                                }
                                if (cttc2.tcPr == null)
                                {
                                    cttc2.AddNewTcPr();
                                }
                                cttc.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                                cttc2.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                            }
                        }
                    }

                    tableContent.RemoveRow(rowindex);
                    //写文件
                    FileStream outFile = new FileStream(outPath, FileMode.Create);
                    try
                    {
                        doc.Write(outFile);
                        outFile.Close();
                    }
                    catch (Exception e)
                    {
                        outFile.Dispose();
                        throw new Exception(e.Message);
                    }
                    var preword = new Aspose.Words.Document(outPath);
                    preword.Save(outPath.Replace("docx", "pdf"), Aspose.Words.SaveFormat.Pdf);
                    QFZeal(outPath.Replace("docx", "pdf"));
                }
                catch (Exception e)
                {


                    throw new Exception(e.Message);
                }

            }
        }

        private void ExptFood(Contract contract, string SignDate, bool isStampQFZ = true)
        {
            List<Sample> samples = _spApp.GetByContractId(contract.Id);
            
            string tempFilePath = "";
            string outPath = @"D:\hklims\tempcontract\" + contract.ct_number + ".docx";
            if (samples.Count > 1)
            {
                tempFilePath = @"D:\hklims\template\tempsp2.docx";
            }
            else
            {
                tempFilePath = @"D:\hklims\template\tempsp2s.docx";
            }
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("CtNumber", contract.ct_number);

            using (FileStream stream = System.IO.File.OpenRead(tempFilePath))
            {
                stream.Position = 0;
                XWPFDocument doc = new XWPFDocument(stream);
                //遍历段落
                foreach (var para in doc.Paragraphs)
                {
                    ReplaceKey(para, data);
                }
                //遍历表格
                foreach (var table in doc.Tables)
                {
                    foreach (var row in table.Rows)
                    {
                        foreach (var cell in row.GetTableCells())
                        {
                            foreach (var para in cell.Paragraphs)
                            {
                                ReplaceKey(para, data);
                            }
                        }
                    }
                }
                XWPFTable firsttable = doc.Tables[0];
                firsttable.GetRow(2).GetCell(1).SetText(contract.ct_entrust_company + "");
                firsttable.GetRow(2).GetCell(3).SetText(contract.ct_entrust_company_address + "");

                firsttable.GetRow(3).GetCell(1).SetText(string.IsNullOrEmpty(contract.ct_entrust_company_comm) ? "/" : contract.ct_entrust_company_comm);//共同委托单位
                firsttable.GetRow(3).GetCell(3).SetText(string.IsNullOrEmpty(contract.ct_entrust_company_address_comm) ? "/" : contract.ct_entrust_company_address_comm);//共同委托单位地址

                firsttable.GetRow(5).GetCell(1).SetText(string.IsNullOrEmpty(contract.ct_entrust_name_comm) ? "/" : contract.ct_entrust_name_comm);//受检单位指定负责人
                firsttable.GetRow(5).GetCell(3).SetText(string.IsNullOrEmpty(contract.ct_entrust_phone_comm) ? "/" : contract.ct_entrust_phone_comm);//电话

                int addrow = 1;
                if (samples.Count > 1)
                {

                    firsttable.GetRow(4).GetCell(1).SetText("见附页");
                    firsttable.GetRow(4).GetCell(3).SetText("见附页");
                    firsttable.GetRow(6).GetCell(1).SetText("见附页");
                    firsttable.GetRow(6).GetCell(3).SetText("见附页");
                    firsttable.GetRow(8).GetCell(1).SetText("见附页");
                    firsttable.GetRow(8).GetCell(3).SetText("见附页");
                    firsttable.GetRow(8).GetCell(5).SetText("见附页");
                    firsttable.GetRow(9).GetCell(1).SetText("见附页");
                    firsttable.GetRow(9).GetCell(3).SetText("见附页");
                    firsttable.GetRow(11).GetCell(1).SetText("见附页");
                    firsttable.GetRow(12).GetCell(1).SetText("见附页");//现场采样地点
                    firsttable.GetRow(14).GetCell(0).SetText("见附页");
                    firsttable.GetRow(14).GetCell(1).SetText("见附页");
                    firsttable.GetRow(14).GetCell(2).SetText("见附页");
                    firsttable.GetRow(14).GetCell(3).SetText("见附页");
                    firsttable.GetRow(14).GetCell(4).SetText("见附页");
                    firsttable.GetRow(16 + addrow).GetCell(1).SetText("见附页");
                    firsttable.GetRow(24 + addrow).GetCell(1).SetText("见附页");
                    //firsttable.GetRow(23).GetCell(1).SetText("见附页");
                }
                else
                {
                    firsttable.GetRow(4).GetCell(1).SetText(samples[0].sp_cs_w_1 + "");
                    firsttable.GetRow(4).GetCell(3).SetText(samples[0].sp_cs_w_2 + "");
                    firsttable.GetRow(6).GetCell(1).SetText(samples[0].sp_cs_5 + "");
                    firsttable.GetRow(6).GetCell(3).SetText(samples[0].sp_cs_4 + "");
                    firsttable.GetRow(8).GetCell(1).SetText(samples[0].sp_s_7 + "");
                    firsttable.GetRow(8).GetCell(3).SetText(samples[0].sp_s_17 + "");
                    string ggxh = "";
                    if ("/".Equals(samples[0].sp_s_18) && "/".Equals(samples[0].sp_s_19))
                    {
                        ggxh = "/";
                    }
                    else if (!"/".Equals(samples[0].sp_s_18) && "/".Equals(samples[0].sp_s_19))
                    {
                        ggxh = samples[0].sp_s_18 + "";
                    }
                    else if ("/".Equals(samples[0].sp_s_18) && !"/".Equals(samples[0].sp_s_19))
                    {
                        ggxh = samples[0].sp_s_19 + "";
                    }
                    else if (!"/".Equals(samples[0].sp_s_18) && !"/".Equals(samples[0].sp_s_19))
                    {
                        ggxh = samples[0].sp_s_18 + " " + samples[0].sp_s_19;
                    }
                    firsttable.GetRow(8).GetCell(5).SetText(ggxh);//规格型号
                    firsttable.GetRow(9).GetCell(1).SetText(samples[0].sp_s_28 + "");
                    firsttable.GetRow(9).GetCell(3).SetText(samples[0].remarks + "");
                    firsttable.GetRow(12).GetCell(1).SetText(string.IsNullOrEmpty(samples[0].sp_cs_w_5) || samples[0].sp_cs_w_5 == "/" ? samples[0].sp_cs_w_7 + "" : samples[0].sp_cs_w_5 + "");//现场采样地点

                    XWPFParagraph items = new XWPFParagraph(new CT_P(), firsttable.Body);
                    var itemrun = items.CreateRun();
                    int index = 0;

                    List<SampleData> sampleDatas = _spDataApp.GetBySampleId(samples[0].Id);
                    InsertRow(firsttable, 14, 15, sampleDatas.Count - 1);
                    addrow = sampleDatas.Count + 1;
                    //foreach (var sd in entity.SampleList[0].SampleDataList)
                    //{
                    //    if (addrow!=1)
                    //    {
                    //        insertRow(firsttable,12, 11 + addrow, entity.SampleList[0].SampleDataList.Count-1);   
                    //    }
                    //    addrow += 1;
                    //}
                    var rowindex = 1;
                    CT_Tc cttc = firsttable.GetRow(13 + rowindex).GetCell(0).GetCTTc();
                    if (cttc.tcPr == null)
                    {
                        cttc.AddNewTcPr();

                    }
                    cttc.tcPr.AddNewVMerge().val = ST_Merge.restart;
                    foreach (SampleData sampleData in sampleDatas)
                    {
                        // cttc = firsttable.GetRow(11 + rowindex).GetCell(0).GetCTTc();
                        //if (rowindex == 1)
                        //{
                        firsttable.GetRow(13 + rowindex).GetCell(0).SetText(samples[0].sp_s_7);
                        //} 
                        firsttable.GetRow(13 + rowindex).GetCell(1).SetText(sampleData.px + "");
                        firsttable.GetRow(13 + rowindex).GetCell(2).SetText(sampleData.spdata_1 + "");
                        firsttable.GetRow(13 + rowindex).GetCell(3).SetText(sampleData.spdata_5 + "");
                        firsttable.GetRow(13 + rowindex).GetCell(4).SetText(sampleData.spdata_6 + "");
                        cttc = firsttable.GetRow(13 + rowindex).GetCell(0).GetCTTc();
                        cttc.tcPr.AddNewVMerge().val = ST_Merge.@continue;

                        rowindex += 1;

                    }
                    addrow = addrow - 1;
                    firsttable.GetRow(16 + addrow).GetCell(1).SetText(samples[0].sp_s_32 + "");
                    //样品储存条件
                    XWPFParagraph samplesave = new XWPFParagraph(new CT_P(), firsttable.Body);
                    GetStrByStrArray(samples[0].sp_s_31, samplesave, new string[] { "常温", "冷藏", "冷冻" }, false);
                    firsttable.GetRow(11).GetCell(1).SetParagraph(samplesave);
                    //样品状态
                    XWPFParagraph samplegpara4 = new XWPFParagraph(new CT_P(), firsttable.Body);
                    string ypzt = samples[0].sp_s_24;

                    GetStrByStrArray(ypzt, samplegpara4, new string[] { "固体", "液体", "半固体", "新鲜" }, false);

                    firsttable.GetRow(24 + addrow).GetCell(1).SetParagraph(samplegpara4);

                }
                Dictionary<string, string> bzdic = new Dictionary<string, string>();
                bool ispd = false;//是否需要判定
                foreach (Sample sample in samples)
                {
                    if (sample.sp_s_50 != null)
                    {
                        if (sample.sp_s_50 == Convert.ToBoolean(Repository.Enum.ContractEnum.YesNo.是))
                        {
                            ispd = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(sample.sp_s_25))
                    {
                        string[] sps25s = sample.sp_s_25.Split(',');
                        if (!bzdic.ContainsKey(sps25s[0]))
                        {
                            bzdic.Add(sps25s[0], "");

                        }
                    }
                }
                //报告要求□无 □CMA □CNAS □CATL □中文报告 □英文报告 报告份数： 份 
                XWPFParagraph reportyq = new XWPFParagraph(new CT_P(), firsttable.Body);
                var portrun = reportyq.CreateRun();
                portrun.FontFamily = "Wingdings 2";
                bool yq = !((bool)contract.stampCATL || (bool)contract.stampCMA || (bool)contract.stampCNAS);

                var portrun21 = reportyq.CreateRun();
                portrun21.FontFamily = "Wingdings 2";
                portrun21.SetText(GetWing(yq));
                reportyq.CreateRun().SetText("无 ");
                var portrun22 = reportyq.CreateRun();
                portrun22.FontFamily = "Wingdings 2";
                portrun22.SetText(GetWing((bool)contract.stampCMA));
                reportyq.CreateRun().SetText("CMA");
                var portrun2 = reportyq.CreateRun();
                portrun2.FontFamily = "Wingdings 2";
                portrun2.SetText(" " + GetWing((bool)contract.stampCNAS));
                reportyq.CreateRun().SetText("CNAS");
                var portrun3 = reportyq.CreateRun();
                portrun3.FontFamily = "Wingdings 2";
                portrun3.SetText(" " + GetWing((bool)contract.stampCATL));
                reportyq.CreateRun().SetText("CATL");
                var portrun4 = reportyq.CreateRun();
                portrun4.FontFamily = "Wingdings 2";
                portrun4.SetText(GetWing(contract.report_language == Convert.ToInt32(OpenAuth.Repository.Enum.SumpleEnum.PortLg.中文).ToString()));
                reportyq.CreateRun().SetText(" 中文报告 ");
                var portrun41 = reportyq.CreateRun();
                portrun41.FontFamily = "Wingdings 2";
                portrun41.SetText(GetWing(contract.report_language == Convert.ToInt32(OpenAuth.Repository.Enum.SumpleEnum.PortLg.英文).ToString()));
                reportyq.CreateRun().SetText("英文报告 报告份数：");
                var portrun5 = reportyq.CreateRun();
                portrun5.SetUnderline(UnderlinePatterns.Single);
                string fs = contract.sp_cc_18 == null ? "" : contract.sp_cc_18 + "";
                portrun5.SetText(fs + "   份");
                firsttable.GetRow(15 + addrow).GetCell(1).SetParagraph(reportyq);
                //样品包装
                XWPFParagraph samplebz = new XWPFParagraph(new CT_P(), firsttable.Body);
                string bztmp = string.Join(",", bzdic.Keys);
                GetStrByStrArray(bztmp, samplebz, new string[] { "袋装", "盒装", "瓶装", "桶装" }, false, bztmp.Contains("装") ? "包装完好" : null);

                firsttable.GetRow(25 + addrow).GetCell(1).SetParagraph(samplebz);
                //样品返还
                XWPFParagraph returnsam = new XWPFParagraph(new CT_P(), firsttable.Body);
                var resamrun = returnsam.CreateRun();
                resamrun.FontFamily = "Wingdings 2";
                resamrun.SetText(GetWing(contract.ct_return_sample == true));
                returnsam.CreateRun().SetText("返还   ");
                var resamrun2 = returnsam.CreateRun();
                resamrun2.FontFamily = "Wingdings 2";
                resamrun2.SetText(GetWing(contract.ct_return_sample == false));
                returnsam.CreateRun().SetText("不返还（默认）");
                firsttable.GetRow(10).GetCell(1).SetParagraph(returnsam);
                //是否需要判定
                XWPFParagraph samplegpara5 = new XWPFParagraph(new CT_P(), firsttable.Body);
                GetYesNoFormat(samplegpara5, ispd ? true : false);
                firsttable.GetRow(14 + addrow).GetCell(1).SetParagraph(samplegpara5);
                //是否同意分包
                XWPFParagraph samplegpara7 = new XWPFParagraph(new CT_P(), firsttable.Body);
                GetYesNoFormat(samplegpara7, contract.subpackage == Convert.ToInt32(YesNoSub.同意外部实验室和本实验室共同完成).ToString());
                firsttable.GetRow(17 + addrow).GetCell(1).SetParagraph(samplegpara7);

                //是否同意方法偏离
                XWPFParagraph samplegpara8 = new XWPFParagraph(new CT_P(), firsttable.Body);
                GetYesNoFormat(samplegpara8, contract.methodDeviate);
                firsttable.GetRow(17 + addrow).GetCell(3).SetParagraph(samplegpara8);
                //报告时间要求
                XWPFParagraph samplegpara13 = new XWPFParagraph(new CT_P(), firsttable.Body);
                var srun13 = samplegpara13.CreateRun();
                srun13.FontFamily = "Wingdings 2";
                srun13.SetText(GetWing(contract.presentation_nature == Convert.ToInt32(ReportService.标准服务).ToString()));
                samplegpara13.CreateRun().SetText("标准服务 ");
                var srun132 = samplegpara13.CreateRun();
                srun132.FontFamily = "Wingdings 2";
                srun132.SetText(GetWing(contract.presentation_nature == Convert.ToInt32(ReportService.加急服务).ToString()));
                samplegpara13.CreateRun().SetText("加急服务");
                firsttable.GetRow(18 + addrow).GetCell(1).SetParagraph(samplegpara13);
                XWPFParagraph reporttime = new XWPFParagraph(new CT_P(), firsttable.Body);
                var reportrun = reporttime.CreateRun();
                reportrun.IsBold = true;
                reportrun.SetText(((DateTime)contract.ct_report_time).ToString("yyyy-MM-dd"));
                firsttable.GetRow(18 + addrow).GetCell(3).SetParagraph(reporttime);//报告日期 
                firsttable.GetRow(18 + addrow).GetCell(5).SetText(contract.ct_pay_cose + "");//费用

                firsttable.GetRow(19 + addrow).GetCell(1).SetText(contract.ct_entrust_name + "");//联系人
                firsttable.GetRow(19 + addrow).GetCell(3).SetText(contract.ct_entrust_phone + "");//联系方式

                //报告领取
                XWPFParagraph samplegpara14 = new XWPFParagraph(new CT_P(), firsttable.Body);
                var srun14 = samplegpara14.CreateRun();
                srun14.FontFamily = "Wingdings 2";
                srun14.SetText(GetWing(contract.ct_report_receive == Convert.ToInt32(ReportWay.自取).ToString()));
                samplegpara14.CreateRun().SetText("自取   ");
                var srun142 = samplegpara14.CreateRun();
                srun142.FontFamily = "Wingdings 2";
                srun142.SetText(GetWing(contract.ct_report_receive == Convert.ToInt32(ReportWay.邮寄).ToString()));
                samplegpara14.CreateRun().SetText("邮寄");
                firsttable.GetRow(20 + addrow).GetCell(1).SetParagraph(samplegpara14);
                firsttable.GetRow(20 + addrow).GetCell(3).SetText(contract.ct_inspection_name + "");//报告邮寄地址

                XWPFParagraph intypepara = new XWPFParagraph(new CT_P(), firsttable.Body);
                intypepara.CreateRun().SetText("发票类型：（开票信息不填写，默认为不开发票） ");
                var run = intypepara.CreateRun();
                run.FontFamily = "Wingdings 2";
                run.SetText(GetWing(contract.ct_invoice_type == Convert.ToString(InvoiceEnum.增值税普通发票)));
                intypepara.CreateRun().SetText("增值税普通发票   ");
                var run2 = intypepara.CreateRun();
                run2.FontFamily = "Wingdings 2";
                run2.SetText(GetWing(contract.ct_invoice_type == Convert.ToString(InvoiceEnum.增值税专用发票)));
                intypepara.CreateRun().SetText("增值税专用发票");
                firsttable.GetRow(21 + addrow).GetCell(0).SetParagraph(intypepara);//发票类型  

                firsttable.GetRow(22 + addrow).GetCell(1).SetText(string.IsNullOrEmpty(contract.ct_invoice_title) ? "/" : contract.ct_invoice_title);//单位名称
                firsttable.GetRow(22 + addrow).GetCell(3).SetText(string.IsNullOrEmpty(contract.identification_number) ? "/" : contract.identification_number);//识别号

                string address_tel = string.IsNullOrEmpty(contract.invoice_address) ? "/" : contract.invoice_address;
                if (address_tel.Equals("/"))
                {
                    address_tel = string.IsNullOrEmpty(contract.invoice_telephone) ? "/" : contract.invoice_telephone;
                }
                else
                {
                    if (!string.IsNullOrEmpty(contract.invoice_telephone))
                    {
                        address_tel += "、" + contract.invoice_telephone;
                    }
                }
                firsttable.GetRow(23 + addrow).GetCell(1).SetText(address_tel);//单位地址、电话

                string bank_number = string.IsNullOrEmpty(contract.opening_bank) ? "/" : contract.opening_bank;
                if (bank_number.Equals("/"))
                {
                    bank_number = string.IsNullOrEmpty(contract.opening_bank_number) ? "/" : contract.opening_bank_number;
                }
                else
                {
                    if (!string.IsNullOrEmpty(contract.opening_bank_number))
                    {
                        bank_number += "、" + contract.opening_bank_number;
                    }
                }
                firsttable.GetRow(23 + addrow).GetCell(3).SetText(bank_number);

                // //开户行号
                // //是否满足检测要求
                //XWPFParagraph samplegpara3 = new XWPFParagraph(new CT_P(), firsttable.Body);
                // var srun3 = samplegpara3.CreateRun();
                // srun3.FontFamily = "Wingdings 2";
                // string sval3 = string.Format("{0}是 {1}否", GetWing(false), GetWing(false));
                // srun3.SetText(sval3);
                // var srun31 = samplegpara3.CreateRun();
                // srun31.SetUnderline(UnderlinePatterns.Single);
                // srun31.SetText("            ");
                // firsttable.GetRow(25).GetCell(1).SetParagraph(samplegpara3);
                // //是否满足检测要求
                // XWPFParagraph samplegpara3 = new XWPFParagraph(new CT_P(), firsttable.Body);
                // var srun3 = samplegpara3.CreateRun();
                // srun3.FontFamily = "Wingdings 2";
                // string sval3 = string.Format("{0}是 {1}否", GetWing(false), GetWing(false));
                // srun3.SetText(sval3);
                // var srun31 = samplegpara3.CreateRun();
                // srun31.SetUnderline(UnderlinePatterns.Single);
                // srun31.SetText("            ");
                // firsttable.GetRow(25).GetCell(1).SetParagraph(samplegpara3);
                // firsttable.GetRow(28 + addrow).GetCell(0).SetText(entity.CtRemarl + "");//其他

                if (string.IsNullOrEmpty(SignDate))
                {
                    firsttable.GetRow(29 + addrow).GetCell(2).SetText("年    月    日");//其他

                }
                else
                {
                    firsttable.GetRow(29 + addrow).GetCell(2).SetText(Convert.ToDateTime(SignDate).ToString("yyyy年MM月dd日"));//其他
                }
                firsttable.GetRow(28 + addrow).GetCell(0).SetText(contract.ct_remarl + "");//其他
                if (samples.Count > 1)
                {
                    XWPFTable tableContent = doc.Tables[1];
                    int rowindex = 1;
                    foreach (var sample in samples)
                    {
                        tableContent.CreateRow();
                        tableContent.GetRow(rowindex).GetCell(0).SetText(sample.xh + "");
                        tableContent.GetRow(rowindex).GetCell(1).SetText(sample.sp_s_7 + "");
                        tableContent.GetRow(rowindex).GetCell(2).SetText(sample.sp_s_31 + "");
                        tableContent.GetRow(rowindex).GetCell(3).SetText(sample.sp_s_17 + "");
                        tableContent.GetRow(rowindex).GetCell(4).SetText(sample.sp_s_28 + "");
                        string ypzt = sample.sp_s_24;
                        if (!string.IsNullOrEmpty(ypzt) && ypzt.Contains("包装完好"))
                        {
                            ypzt = sample.sp_s_24.Replace(",包装完好", "").Replace("包装完好", "");
                        }
                        tableContent.GetRow(rowindex).GetCell(5).SetText(ypzt + "");
                        tableContent.GetRow(rowindex).GetCell(6).SetText(sample.sp_cs_w_1 + "");
                        tableContent.GetRow(rowindex).GetCell(7).SetText(sample.sp_cs_w_2 + "");
                        tableContent.GetRow(rowindex).GetCell(8).SetText(sample.sp_cs_5 + "");
                        tableContent.GetRow(rowindex).GetCell(9).SetText(sample.sp_cs_4 + "");
                        tableContent.GetRow(rowindex).GetCell(10).SetText(string.IsNullOrEmpty(sample.sp_cs_w_5) || sample.sp_cs_w_5 == "/" ? sample.sp_cs_w_7 + "" : sample.sp_cs_w_5 + "");
                        tableContent.GetRow(rowindex).GetCell(11).SetText((string.IsNullOrEmpty(sample.sp_s_18) || sample.sp_s_18 == "/" ? "" : sample.sp_s_18 + "/") + sample.sp_s_19 + "");
                        tableContent.GetRow(rowindex).GetCell(12).SetText(sample.remarks + "");
                        rowindex += 1;
                    }

                    XWPFTable tableContent2 = doc.Tables[2];
                    rowindex = 1;
                    tableContent2.CreateRow();
                    foreach (var sample in samples)
                    {
                        CT_Tc cttc = tableContent2.GetRow(rowindex).GetCell(0).GetCTTc();
                        CT_Tc cttc2 = tableContent2.GetRow(rowindex).GetCell(1).GetCTTc();
                        if (cttc.tcPr == null)
                        {
                            cttc.AddNewTcPr();
                        }
                        if (cttc2.tcPr == null)
                        {
                            cttc2.AddNewTcPr();
                        }
                        tableContent2.GetRow(rowindex).GetCell(0).SetText(sample.xh + "");
                        tableContent2.GetRow(rowindex).GetCell(1).SetText(sample.sp_s_7);
                        cttc.tcPr.AddNewVMerge().val = ST_Merge.restart;
                        cttc2.tcPr.AddNewVMerge().val = ST_Merge.restart;
                        List<SampleData> sampleDatas = _spDataApp.GetBySampleId(sample.Id);
                        foreach (var sd in sampleDatas)
                        {

                            tableContent2.GetRow(rowindex).GetCell(2).SetText(sd.px + "");
                            tableContent2.GetRow(rowindex).GetCell(3).SetText(sd.spdata_1 + "");
                            tableContent2.GetRow(rowindex).GetCell(4).SetText(sd.spdata_5 + "");
                            tableContent2.GetRow(rowindex).GetCell(5).SetText(sd.spdata_6 + "");
                            tableContent2.CreateRow();
                            rowindex += 1;
                            cttc = tableContent2.GetRow(rowindex).GetCell(0).GetCTTc();
                            cttc2 = tableContent2.GetRow(rowindex).GetCell(1).GetCTTc();
                            if (cttc.tcPr == null)
                            {
                                cttc.AddNewTcPr();
                            }
                            if (cttc2.tcPr == null)
                            {
                                cttc2.AddNewTcPr();
                            }
                            cttc.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                            cttc2.tcPr.AddNewVMerge().val = ST_Merge.@continue;
                        }
                    }
                    tableContent2.RemoveRow(rowindex);
                }

                //写文件
                FileStream outFile = new FileStream(outPath, FileMode.Create);
                doc.Write(outFile);
                outFile.Close();
                string userfontsfoloder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Microsoft\\Windows\\Fonts\\";

                ArrayList fontSources = new ArrayList(FontSettings.DefaultInstance.GetFontsSources());
                //将用户目录字体添加到字体源中
                FolderFontSource folderFontSource = new FolderFontSource(userfontsfoloder, true);
                fontSources.Add(folderFontSource);
                FontSourceBase[] updatedFontSources = (FontSourceBase[])fontSources.ToArray(typeof(FontSourceBase));
                FontSettings.DefaultInstance.SetFontsSources(updatedFontSources);
                //outPath = @"D:\hklims\tempcontract\HKGG2103764.docx";
                var preword = new Aspose.Words.Document(outPath, new LoadOptions() { MswVersion = Aspose.Words.Settings.MsWordVersion.Word2016 });

                preword.Save(outPath.Replace("docx", "pdf"), Aspose.Words.SaveFormat.Pdf);
                if (isStampQFZ)
                {
                    QFZeal(outPath.Replace("docx", "pdf"));
                }
            }
        }

        private static string GetWing(bool bol)
        {
            if (bol)
            {
                return Convert.ToChar(0x0052).ToString();
            }
            else
            {
                return Convert.ToChar(0x00A3).ToString();
            }
        }

        private static void GetImage(int num)
        {
            //List<Bitmap> lists = new List<Bitmap>();
            Image image = Image.FromFile(@"D:\hklims\image\contract_q.png");
            int w = image.Width / num;
            Bitmap bitmap = null;
            for (int i = 0; i < num; i++)
            {
                if (i == num - 1)
                {
                    w = w + 1;
                }
                bitmap = new Bitmap(w, image.Height, PixelFormat.Format32bppArgb);
                using (Graphics g = Graphics.FromImage(bitmap))
                {

                    Rectangle rect = new Rectangle(i * w, 0, w, image.Height);
                    g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height), rect, GraphicsUnit.Pixel);
                }
                bitmap.MakeTransparent(ColorTranslator.FromHtml("#000000"));
                bitmap.Save(string.Format(@"D:\hklims\image\seals\seal{0}.png", i));
                //  lists.Add(bitmap);
            }
        }

        /// <summary>
        /// 在word表格中指定位置插入一行，并将某一行的样式复制到新增行
        /// </summary>
        /// <param name="table"></param>
        /// <param name="copyrowIndex">需要复制的行位置</param>
        /// <param name="newrowIndex">需要新增一行的位置</param>
        /// <param name="count"></param>
        private static void InsertRow(XWPFTable table, int copyrowIndex, int newrowIndex, int count)
        {
            XWPFTableRow copyRow = table.GetRow(copyrowIndex);
            CT_Tc cttc = copyRow.GetCell(0).GetCTTc();
            if (cttc.tcPr == null)
            {
                cttc.AddNewTcPr();
            }
            cttc.tcPr.AddNewVMerge().val = ST_Merge.restart;
            for (int j = 0; j < count; j++)
            {
                // 在表格中指定的位置新增一行
                XWPFTableRow targetRow = table.InsertNewTableRow(newrowIndex);

                //复制行对象
                targetRow.GetCTRow().trPr = copyRow.GetCTRow().trPr;

                //或许需要复制的行的列
                List<XWPFTableCell> copyCells = copyRow.GetTableCells();
                //复制列对象
                XWPFTableCell targetCell = null;
                for (int i = 0; i < copyCells.Count; i++)
                {

                    XWPFTableCell copyCell = copyCells[i];
                    targetCell = targetRow.AddNewTableCell();
                    targetCell.GetCTTc().tcPr = copyCell.GetCTTc().tcPr;
                    if (i == 0 && j > 0)
                    {
                        targetCell.GetCTTc().tcPr.AddNewVMerge().val = ST_Merge.@continue;
                    }
                    if (copyCell.Paragraphs != null && copyCell.Paragraphs.Count > 0)
                    {
                        targetCell.Paragraphs[0].GetCTP().pPr = copyCell.Paragraphs[0].GetCTP().pPr;
                        if (copyCell.Paragraphs[0].Runs != null
                                && copyCell.Paragraphs[0].Runs.Count > 0)
                        {
                            XWPFRun cellR = targetCell.Paragraphs[0].CreateRun();
                            cellR.IsBold = copyCell.Paragraphs[0].Runs[0].IsBold;
                        }
                    }
                }
                newrowIndex += 1;
            }
            // 获取需要复制行对象


        }

        private static void QFZeal(string path)
        {
            PdfDocument sdoc = new PdfDocument();

            sdoc.LoadFromFile(path);
            var page = sdoc.Pages.Add();
            sdoc.Pages.Remove(page);
            PdfPageBase pageBase = null;
            float x = 80;
            float y = 20;
            int offset = 25;
            //获取分割后的印章图片
            if (sdoc.Pages.Count > 1)
            {
                GetImage(sdoc.Pages.Count);
                for (int i = 0; i < sdoc.Pages.Count; i++)
                {
                    pageBase = sdoc.Pages[i];
                    Image mg = Image.FromFile(string.Format(@"D:\hklims\image\seals\seal{0}.png", i));

                    float width = mg.Width * 0.12F;
                    float height = mg.Height * 0.12F;
                    //if (i != 1)
                    //{ 
                    if (pageBase.Size.Width > 700)
                    {
                        //pageBase.Rotation = PdfPageRotateAngle.RotateAngle90;
                        float rw = pageBase.Size.Width;
                        mg.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        mg.Save(string.Format(@"D:\hklims\image\seals\seal{0}.png", i));
                        mg.Dispose();
                        mg = Image.FromFile(string.Format(@"D:\hklims\image\seals\seal{0}.png", i));
                        y = pageBase.Size.Height - width - offset;// convert.ConvertToPixels(images[(i > 1 ? i - 1 : i)].Width, PdfGraphicsUnit.Point);
                        x = pageBase.Size.Width / 2;
                        pageBase.Canvas.DrawImage(PdfImage.FromImage(mg), x, y, height, width);
                        pageBase.Rotation = PdfPageRotateAngle.RotateAngle270;
                    }
                    else
                    {
                        x = pageBase.Canvas.ClientSize.Width - width - offset;// convert.ConvertToPixels(images[(i > 1 ? i - 1 : i)].Width, PdfGraphicsUnit.Point);
                        y = pageBase.Size.Height / 2;
                        pageBase.Canvas.DrawImage(PdfImage.FromImage(mg), x, y, width, height);
                    }

                    // PdfImage bitimg=  PdfBitmap.FromImage(Image.FromFile(sealPath + "seal4.png")); 

                    mg.Dispose();
                    System.IO.File.Delete(string.Format(@"D:\hklims\image\seals\seal{0}.png", i));
                }
            }
            sdoc.SaveToFile(path);
            sdoc.Close();
        }

        private static void GetYesNoFormat(XWPFParagraph paragraph, bool? yesno)
        {
            var srun = paragraph.CreateRun();
            srun.FontFamily = "Wingdings 2";
            srun.SetText(GetWing(true));
            paragraph.CreateRun().SetText("是    ");
            var srun2 = paragraph.CreateRun();
            srun2.FontFamily = "Wingdings 2";
            srun2.SetText(GetWing(false));
            paragraph.CreateRun().SetText("否");
        }

        private static void GetStrByStrArray(string values, XWPFParagraph paragraph, string[] strs, bool equal, string other = null)
        {
            List<string> samtp2 = values.Split(",").ToList();
            foreach (var item in strs)
            {
                var run = paragraph.CreateRun();
                run.FontFamily = "Wingdings 2";
                run.SetText(equal ? GetWing(values.Equals(item)) : GetWing(values.Contains(item)));
                paragraph.CreateRun().SetText(item + " ");
                samtp2.Remove(item);
            }
            var runq = paragraph.CreateRun();
            runq.FontFamily = "Wingdings 2";
            runq.SetText(GetWing(samtp2.Count != 0));
            if (string.IsNullOrEmpty(other))
            {
                paragraph.CreateRun().SetText("其他：");
            }
            else
            {
                paragraph.CreateRun().SetText("其他：" + other);
            }

            var runend = paragraph.CreateRun();
            runend.SetUnderline(UnderlinePatterns.Single);
            runend.SetText("  " + string.Join(",", samtp2));
        }

        private static void ReplaceKey(XWPFParagraph para, Dictionary<string, string> data)
        {
            string text = para.Text;

            foreach (var key in data.Keys)
            {
                //$$模板中数据占位符为$KEY$
                if (text.Contains($"${key}$"))
                {
                    if (key == "SpCsW1")
                    {
                        foreach (var item in para.Runs)
                        {
                            item.FontFamily = "Wingdings 2";
                            para.ReplaceText($"${key}$", data[key] + Convert.ToChar(0x0052).ToString());
                        }
                    }
                    else
                    {
                        para.ReplaceText($"${key}$", data[key]);
                    }
                }
            }
        }
    }
}