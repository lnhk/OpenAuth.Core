using System;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 发票接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "客户_发票接口_Invoices")]
    public class InvoicesController : ControllerBase
    {
        private readonly InvoiceApp _invoiceApp;
        
        //获取详情
        [HttpGet]
        public Response<Invoice> Get(decimal id)
        {
            var result = new Response<Invoice>();
            try
            {
                result.Result = _invoiceApp.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public Response Add(AddInvoiceReq addInvoiceReq)
        {
            var result = new Response();
            try
            {
                Invoice invoice = addInvoiceReq.MapTo<Invoice>();
                _invoiceApp.Add(invoice);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public Response Update(UpdateInvoiceReq obj)
        {
            var result = new Response();
            try
            {
                _invoiceApp.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery]QueryInvoiceListReq request)
        {
            return await _invoiceApp.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            var result = new Response();
            try
            {
                _invoiceApp.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public InvoicesController(InvoiceApp app) 
        {
            _invoiceApp = app;
        }
    }
}
