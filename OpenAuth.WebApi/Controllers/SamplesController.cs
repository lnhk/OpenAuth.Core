using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using OpenAuth.App;
using OpenAuth.App.Common;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 样品接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "合同_样品接口_Samples")]
    public class SamplesController : ControllerBase
    {
        private readonly ContractApp _contractApp;
        private readonly SampleApp _spApp;
        private readonly ExtensionHelper _exHelper;
        private readonly SampleUpDataApp _sampleUpDataApp;

        public SamplesController(ContractApp contractApp, SampleApp spApp, ExtensionHelper exHelper, SampleUpDataApp sampleUpDataApp)
        {
            _contractApp = contractApp;
            _spApp = spApp;
            _exHelper = exHelper;
            _sampleUpDataApp = sampleUpDataApp;
        }

        //获取详情
        [HttpGet]
        public Response<Sample> Get(decimal id)
        {
            var result = new Response<Sample>();
            try
            {
                result.Result = _spApp.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 添加样品,同时更新合同与样品关联的属性
        /// </summary>
        /// <param name="req">添加样品请求</param>
        /// <returns></returns>
        [HttpPost]
        public Response Add(AddSampleReq req)
        {
            Response resp = new Response();
            try
            {
                Sample sample = req.MapTo<Sample>();
                List<SampleData> sampleDatas = new List<SampleData>();
                foreach (AddNewSampleDataReq addNewSpDataReq in req.addNewSpDataReq)
                {
                    SampleData sampleData = addNewSpDataReq.MapTo<SampleData>();
                    sampleData.sp_id = sample.Id;
                    sampleDatas.Add(sampleData);
                }
                _spApp.AddWithRelatedNoSave(sample, sampleDatas);
                _exHelper.Save();
                _contractApp.UpdateRelatedSampleState(_contractApp.GetBySampleId(sample.Id).Id);
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        /// <summary>
        /// 修改样品,同时更新合同与样品关联的属性
        /// </summary>
        /// <param name="req">更新样品请求</param>
        /// <returns></returns>
        [HttpPost]
        public Response Update(UpdateSampleReq req)
        {
            Response resp = new Response();
            try
            {
                Sample sample = req.MapTo<Sample>();
                _spApp.Update(sample);
                _contractApp.UpdateRelatedSampleState(_contractApp.GetBySampleId(sample.Id).Id);
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        /// <summary>
        /// 批量删除样品,同时更新合同与样品关联的属性
        /// </summary>
        /// <param name="ids">删除样品的id数组</param>
        /// <returns></returns>
        [HttpPost]
        public Response Delete([FromBody] decimal[] ids)
        {
            Response resp = new Response();
            try
            {
                Contract contract = _contractApp.GetBySampleId(ids[0]);
                _spApp.Delete(ids);
                _contractApp.UpdateRelatedSampleState(contract.Id);
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery] QuerySampleListReq request)
        {
            return await _spApp.Load(request);
        }

        /// <summary>
        /// 变更样品及检测项目
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response ChangeSampleAndData(ChangeSampleAndSpDataReq req)
        {
            var result = new Response();
            try
            {
                Sample sample = req.MapTo<Sample>();
                List<SampleData> sampleDatas = new List<SampleData>();
                foreach (ChangeSpDataReq addNewSpDataReq in req.addNewSpDataReq)
                {
                    SampleData sampleData = addNewSpDataReq.MapTo<SampleData>();
                    sampleData.sp_id = sample.Id;
                    sampleDatas.Add(sampleData);
                }
                _spApp.ChangeSampleAndData(sample, sampleDatas);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 获取样品关联的检测项目
        /// </summary>
        /// <param name="sampleId">样品id</param>
        /// <returns></returns>
        [HttpGet]
        public Response<List<SampleData>> GetRelatedSampleDatas([FromQuery] decimal sampleId)
        {
            var resp = new Response<List<SampleData>>();
            try
            {
                resp.Result = _spApp.GetRelatedSpData(sampleId);
            }
            catch (Exception ex)
            {
                resp.Code = 500;
                resp.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return resp;
        }

        /// <summary>
        /// 获取变更的样品信息和检测项目信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public Response<SampleUpDataRes> getUpSampleAndDataList(decimal ContractId, decimal SampleId)
        {
            var result = new Response<SampleUpDataRes>();
            try
            {
                result.Result = _sampleUpDataApp.getUpSampleAndDataList(ContractId, SampleId);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
    }
}
