using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository.Domain;

namespace OpenAuth.WebApi.Controllers
{
    /// <summary>
    /// 检测项目接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "基础数据_基础数据接口_BaseData")]
    public class FactorController : ControllerBase
    {
        private readonly FactorApp _app;
        private CacheContext _cache;
        public FactorController(FactorApp app)
        {
            _app = app;
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions()));
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public Response<FactorResp> Get(string id)
        {
            var result = new Response<FactorResp>();
            try
            {
                result.Result = _app.FindSingle(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public Response Add(AddOrUpdateFactorReq obj)
        {
            var result = new Response();
            try
            {
                _app.Add(obj);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        public Response Update(AddOrUpdateFactorReq obj)
        {
            var result = new Response();
            try
            {
                _app.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public async Task<TableData> Load([FromQuery] QueryFactorListReq request)
        {
            return await _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        [HttpPost]
        public Response Delete([FromBody] string[] ids)
        {
            var result = new Response();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 项目方法关联下拉数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<FactorStandardSelectResp>>> GetFactorStandardSelect()
        {
            var result = new Response<List<FactorStandardSelectResp>>();
            try
            {
                result.Result = await _app.GetFactorStandardSelect();

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }


        /// <summary>
        /// 项目方法关联明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<FactorStandardDetailResp>> GetFactorStandardDetail(string id)
        {
            var result = new Response<FactorStandardDetailResp>();
            try
            {
                result.Result = await _app.GetFactorStandardDetail(id);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }


        /// <summary>
        /// 产品下拉数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<ProcategorySelectResp>>> GetProcategorySelect()
        {
            var result = new Response<List<ProcategorySelectResp>>();
            try
            {
                result.Result = await _app.GetProcategorySelect();

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 产品项目分类
        /// </summary>
        /// <param name="id">产品id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<ProcategoryItemdataListResp>>> GetProcategoryItemdataList(int id)
        {
            var result = new Response<List<ProcategoryItemdataListResp>>();
            try
            {
                result.Result = await _app.GetProcategoryItemdataList(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 产品标准分类
        /// </summary>
        /// <param name="name">分类名称</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<CategorytreeListResp>>> GetCategorytreeList(string name)
        {
            var result = new Response<List<CategorytreeListResp>>();
            try
            {
                result.Result = await _app.GetCategorytreeList(name);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 产品项目标准
        /// </summary>
        /// <param name="categorytreeId">产品分类id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<ProcategoryListResp>>> GetProcategoryListByCategorytreeId(int categorytreeId)
        {
            var result = new Response<List<ProcategoryListResp>>();
            try
            {
                result.Result = await _app.GetProcategoryList(categorytreeId);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 项目方法关联列表
        /// </summary>
        /// <param name="factorName">项目名称</param>
        /// <param name="standardName">方法名称</param>
        /// <param name="dataSource">数据来源</param>
        /// <param name="dataStatus">数据状态</param>
        /// <param name="isSubpakege">是否分包</param>
        /// <param name="lab">实验室</param>
        /// <param name="factorOtherName">项目别名</param>
        /// <param name="page">页码</param>
        /// <param name="limit">每页条数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<List<FactorStandardListResp>>> GetFactorStandardList(string factorName, string standardName, int? dataSource, int? dataStatus, bool? isSubpakege, string lab, string factorOtherName, int page = 1, int limit = 20)
        {
            var result = new PageResponse<List<FactorStandardListResp>>();
            try
            {
                var data = await _app.GetFactorStandardList(factorName, standardName, dataSource, dataStatus, isSubpakege, lab, factorOtherName, page, limit);
                result.TotalCount = data.totalCount;
                result.Result = data.data;
                result.page = page;
                result.limit = limit;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 检测项目下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<FactorStandardSelectResp>>> GetFactorSelect()
        {
            var result = new Response<List<FactorStandardSelectResp>>();
            try
            {
                result.Result = await _app.GetFactorSelect();
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 检测方法下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<FactorStandardSelectResp>>> GetStandardSelect()
        {
            var result = new Response<List<FactorStandardSelectResp>>();
            try
            {
                result.Result = await _app.GetStandardSelect();
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }


        /// <summary>
        /// 添加或者修改项目方法关联
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> UpdateOrAddFactorStandard(AddOrUpdateFactorStandardReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.UpdateOrAddFactorStandard(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 分包公司下拉数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<SubpackageCompanySelectResp>>> GetSubpackageCompanySelect()
        {
            var result = new Response<List<SubpackageCompanySelectResp>>();
            try
            {
                result.Result = await _app.GetSubpackageCompanySelect();
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 删除项目方法关联
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> BatchDeleteFactorStandard(DeleteFactorStandardReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.BatchDeleteFactorStandard(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        #region 限值单位
        /// <summary>
        ///  限值单位列表
        /// </summary>
        /// <param name="name">单位名称</param>
        /// <param name="page">页码</param>
        /// <param name="limit">每页条数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<List<UnitSelectResp>>> GetUnitList(string name, int page = 1, int limit = 20)
        {
            var result = new PageResponse<List<UnitSelectResp>>();
            try
            {
                var data = await _app.GetUnitList(name, page, limit);
                result.Result = data.data;
                result.TotalCount = data.totalCount;
                result.page = page;
                result.limit = limit;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 添加或修改限值单位
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> AddOrUpdateUnit(AddOrUpdateUnit req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.AddOrUpdateUnit(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 限值单位明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<UnitSelectResp>> GetUnitDetail(int id)
        {
            var result = new Response<UnitSelectResp>();
            try
            {
                result.Result = await _app.GetUnitDetail(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 批量删除限值单位
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> BatchDeleteUnit(BatchDeleteUnitReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.BatchDeleteUnit(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        #endregion

        /// <summary>
        /// 添加或修改产品分类
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> AddOrUpdateCategoryTree(AddOrUpdateCategoryTreeReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.AddOrUpdateCategoryTree(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 删除产品分类
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> DeleteCategoryTree(DeleteCategoryTreeReq req)
            => await _app.DeleteCategoryTree(req);

        /// <summary>
        /// 删除产品分类-产品
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public Response<bool> DeleteCategoryTreeProcategory(DeleteCategoryTreeReq req)
            => _app.DeleteCategoryTreeProcategory(req);

        /// <summary>
        /// 单位下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<List<UnitSelectResp>>> GetUnitSelect()
        {
            var result = new Response<List<UnitSelectResp>>();
            try
            {
                result.Result = await _app.GetUnitSelect();
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 获取产品项目标准列表
        /// </summary>
        /// <param name="name">产品名称</param>
        /// <param name="standardCode">产品标准代号</param>
        /// <param name="dataSource">数据源</param>
        /// <param name="standardId">检测标准id</param>
        /// <param name="factorId">检测项目id</param>
        /// <param name="page">页码</param>
        /// <param name="limit">每页条数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<List<ProcategoryListResp>>> GetProcategoryList(string name, string standardCode, int? dataSource, string standardId, string factorId, int page = 1, int limit = 20)
            => await _app.GetProcategoryList(name, standardCode, dataSource, standardId, factorId, page, limit);

        /// <summary>
        /// 获取产品项目标准明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Response<ProcategoryDetailResp>> GetProcategoryDetail(int id)
            => await _app.GetProcategoryDetail(id);

        /// <summary>
        /// 添加或修改产品项目标准
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> AddOrUpdateProcategory(AddOrUpdateProcategoryReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.AddOrUpdateProcategory(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 删除产品项目标准
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<Response<bool>> BatchDeleteProcategory(BatchDeleteProcategoryReq req)
        {
            var result = new Response<bool>();
            try
            {
                result.Result = await _app.BatchDeleteProcategory(req);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        /// <summary>
        /// 更新产品分类下 产品项目标准 返回 产品列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public PageResponse<List<ProcategoryListResp>> UpdateProcategoryBySelect(UpdateProcategoryBySelectReq req)
            => _app.UpdateProcategoryBySelect(req);

        /// <summary>
        /// 获取产品列表通过产品分类id（分页）
        /// </summary>
        /// <param name="categoryTreeId"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public PageResponse<List<ProcategoryListResp>> GetProcategoryPageListByCategoryTreeId(int categoryTreeId, int page, int limit)
            => _app.GetProcategoryPageListByCategoryTreeId(categoryTreeId, page, limit);
    }
}
