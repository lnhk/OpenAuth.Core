﻿namespace Infrastructure
{
    public class BoolResponse: Response
    {
        public bool BoolResult { get; set; }

        public BoolResponse()
        {
            BoolResult= false;
        }
    }

    public class Response
    {
        /// <summary>
        /// 操作消息【当Status不为 200时，显示详细的错误信息】
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 操作状态码，200为正常
        /// </summary>
        public int Code { get; set; }

        public Response()
        {
            Code = 200;
            Message = "操作成功";
        }
    }


    /// <summary>
    /// WEBAPI通用返回泛型基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Response<T> : Response
    {
        /// <summary>
        /// 回传的结果
        /// </summary>
        public T Result { get; set; }
    }

    public class PageResponse<T> : Response
    {
        public T Result { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int page { get; set; }
        /// <summary>
        /// 每页条数
        /// </summary>
        public int limit { get; set; }
    }
}
