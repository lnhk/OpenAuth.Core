﻿namespace Infrastructure
{
    public static class Define
    {
        public static string USERROLE = "UserRole";       //用户角色关联KEY
        public const string ROLERESOURCE= "RoleResource";  //角色资源关联KEY
        public const string USERORG = "UserOrg";  //用户机构关联KEY
        public const string ROLEELEMENT = "RoleElement"; //角色菜单关联KEY
        public const string ROLEMODULE = "RoleModule";   //角色模块关联KEY
        public const string ROLEDATAPROPERTY = "RoleDataProperty";   //角色数据字段权限

        public const string DBTYPE_SQLSERVER = "SqlServer";    //sql server
        public const string DBTYPE_MYSQL = "MySql";    //mysql
        public const string DBTYPE_ORACLE = "Oracle";    //oracle


        public const int INVALID_TOKEN = 50014;     //token无效

        public const string TOKEN_NAME = "X-Token";
        public const string TENANT_ID = "tenantId";


        public const string SYSTEM_USERNAME = "System";
        public const string SYSTEM_USERPWD = "123456";

        public const string DATAPRIVILEGE_LOGINUSER = "{loginUser}";  //数据权限配置中，当前登录用户的key
        public const string DATAPRIVILEGE_LOGINROLE = "{loginRole}";  //数据权限配置中，当前登录用户角色的key
        public const string DATAPRIVILEGE_LOGINORG = "{loginOrg}";  //数据权限配置中，当前登录用户部门的key

        public const string JOBMAPKEY = "OpenJob";

        //字典id常量
        public class Dictionary
        {
            public class SampleData
            {
                public const string 样品 = "";
            }
        }

        //权限id常量
        public class Permission
        {

            //功能
            public class FunctionMenu
            {
                //查看全部合同，变更历史
                public const string 全部报告 = "Contract:FunctionMenu:AllReporter";
                //加载全部客户权限
                public const string 全部客户 = "Contract:FunctionMenu:AllCustomer";
                //实验室提交数据权限
                public const string 实验室提交权限_企业 = "Contract:FunctionMenu:CommitLibrayResult_QY";
                public const string 实验室提交权限_政府 = "Contract:FunctionMenu:CommitLibrayResult_ZF";
                //基础数据修改
                public const string 基础数据维护 = "Contract:FunctionMenu:BaseInfo";
                //国抽模块-数据编辑
                public const string 国抽模块_样品数据编辑 = "Contract:FunctionMenu:GCEdit";
                //政府样品作废和恢复按钮权限
                public const string 政府样品作废和恢复按钮权限 = "Contract:FunctionMenu:SObsoleteZf";

                //政府样品作废和恢复按钮权限
                public const string 政府样品不合格按钮权限 = "Contract:FunctionMenu:DisqualificationZf";


                public const string 生成产品及标准 = "Contract:FunctionMenu:packProcategory";
                public const string 允许财务修改开票日期 = "Contract:FunctionMenu:updateInvoice";
                public const string 样品接收允许修改 = "Contract:FunctionMenu:EditByAcc";

                public const string 政府基础数据修改权限 = "Contract:FunctionMenu:ZFDataUpdate";

                public const string 政府食品_环境协议新增修改权限 = "Contract:FunctionMenu:ZFContractUpdate";
                public const string 政府食品_环境回款新增修改权限 = "Contract:FunctionMenu:ZFBackMoneyUpdate";
                public const string 政府食品_环境发票新增修改权限 = "Contract:FunctionMenu:ZFInvoiceUpdate";
                public const string 政府食品_环境发票开票及退回 = "Contract:FunctionMenu:ZFOpencoseAndUnOpencose";

                public const string 企业年协议新增权限 = "Contract:FunctionMenu:QYContractCreate";
                public const string 政府非食品年协议新增权限 = "Contract:FunctionMenu:ZF_FSPContractCreate";
                public const string 年协议修改权限 = "Contract:FunctionMenu:YearContractUpdate";
                public const string 年协议关联和移除合同 = "Contract:FunctionMenu:ConnectContract";
                public const string 年协议删除权限 = "Contract:FunctionMenu:YearContractDelete";

                public const string 制样记录修改 = "Contract:FunctionMenu:HkBuildsampleinfo_Update";
                public const string 制样记录新增 = "Contract:FunctionMenu:HkBuildsampleinfo_Insert";

                public const string 天平使用全部记录 = "Contract:FunctionMenu:HkLibrahisinfo_All";
                public const string 天平使用修改记录 = "Contract:FunctionMenu:HkLibrahisinfo_Update";
                public const string 天平使用新增记录 = "Contract:FunctionMenu:HkLibrahisinfo_Insert";
                public const string 天平使用作废记录 = "Contract:FunctionMenu:HkLibrahisinfo_Obsolete";

                public const string 实验室领样全部记录 = "Contract:FunctionMenu:HkLibraryaccinfo_All";
                public const string 实验室领样修改记录 = "Contract:FunctionMenu:HkLibraryaccinfo_Update";
                public const string 实验室领样新增记录 = "Contract:FunctionMenu:HkLibraryaccinfo_Insert";

                public const string 查看全部报告申请权限 = "Contract:FunctionMenu:AllReportApply";

                public const string 样品接受微信通知 = "Contract:FunctionMenu:WeChatToPeople";
                public const string 批量修改样品受检单位 = "Contract:FunctionMenu:BeachUpdateSample";

                public const string 政府批量修改样品为待接受 = "Contract:FunctionMenu:UpdateAcc";
                public const string 年协议合同接收和完成 = "Contract:FunctionMenu:AccAndFinshed";
                public const string 从企业引入定量限 = "Contract:FunctionMenu:FromQiYe";

                public const string 查看全部预警样品 = "Contract:FunctionMenu:AllSampledataDisqualification";
                public const string 预警更新反馈原因 = "Contract:FunctionMenu:UpdateFeedback";

                public const string 查看全部政府预警样品 = "Contract:FunctionMenu:AllSampledataDisqualificationZf";
                public const string 政府预警更新反馈原因 = "Contract:FunctionMenu:UpdateFeedbackZf";


                public const string 运输单退回 = "Contract:FunctionMenu:transportback";
                public const string 报告查询中下载报告 = "Contract:FunctionMenu:DownloadReport";
                public const string 企业微信销售认领 = "Contract:FunctionMenu:SaleClaim";
                public const string 每日应出报告样品查看全部样品 = "Contract:FunctionMenu:EverydayAllSample";
                public const string 财务统计所有业务员 = "Contract:FunctionMenu:AllBeforer";
                public const string 资质修改 = "Contract:FunctionMenu:UpdateAptitude";
            }

            //合同中心
            public class ContractMenu
            {
                public const string 我的合同 = "Contract:ContractMenu:MyContract";
                public const string 我待办的合同 = "Contract:ContractMenu:MyWaitContract";
                public const string 我能看的合同 = "Contract:ContractMenu:MyEnableContract";
                public const string 作废的合同 = "Contract:ContractMenu:ContractObsolete";
                public const string 公共场所任务单 = "Contract:ContractMenu:GGTaskSheet";
                public const string 合同审核 = "Contract:ContractMenu:ZAudit";
                public const string 合同评审 = "Contract:ContractMenu:Audit";
                public const string 合同管理 = "Contract:ContractMenu:Manage";
                public const string 我的合同变更申请 = "Contract:ContractMenu:MyContractChange";
                public const string 合同变更记录查询 = "Contract:ContractMenu:ContractChangeRecord";
                public const string 合同变更申请 = "Contract:ContractMenu:ContractChangeApply";
                public const string 受检单位管理 = "Contract:ContractMenu:SJ_Manage";
                public const string 发票管理 = "Contract:ContractMenu:InvoiceManage";
                public const string 非环境回款方式变更 = "Contract:ContractMenu:PaymentMethod";
                public const string 环境回款方式变更 = "Contract:ContractMenu:PaymentMethodHj";
            }

            //财务中心
            public class FinanceMenu
            {
                public const string 财务_合同发票管理 = "Contract:FinanceMenu:Finance";
                public const string 客服_合同发票管理 = "Contract:FinanceMenu:Finance_KF";
                public const string 财务中心_发票管理 = "Contract:FinanceMenu:InvoiceManage";
                public const string 申请的发票 = "Contract:FinanceMenu:ApplyInvoice";
                public const string 回款记录 = "Contract:FinanceMenu:MoneyBack";
                public const string 财务统计 = "Contract:FinanceMenu:FinanceTotal";
                public const string 销售与协议 = "Contract:FinanceMenu:SaleAndContract";
                public const string 银行回款信息 = "Contract:FinanceMenu:BankInfo";
                public const string 回款对账 = "Contract:FinanceMenu:BankAndContrct";
                public const string 对账信息 = "Contract:FinanceMenu:Accountcheck";
                public const string 未认领回款 = "Contract:FinanceMenu:UnbackMoney";
                public const string 销售认领回款 = "Contract:FinanceMenu:SaleBackMoney";
            }

            //扩项申请
            public class AptitudesMenu
            {
                public const string 资质 = "Contract:Aptitudes:apply";
                public const string 资质评审 = "Contract:Aptitudes:audit";
                public const string 我的资质驳回 = "Contract:Aptitudes:MyReject";
                public const string 扩项下单 = "Contract:Aptitudes:DownOrder";
            }

            //销售统计
            public class SaleMenu
            {
                public const string 我的订单分析 = "Contract:SaleMenu:Orders";
                public const string 部门订单分析 = "Contract:SaleMenu:DepartmentOrders";
            }

            //报告管理
            public class ReportMenu
            {
                public const string 报告编制_单样品 = "Contract:ReportMenu:ReportSingle";
                public const string 报告编制_政府 = "Contract:ReportMenu:Report_ZF";
                public const string 报告编制_多样品 = "Contract:ReportMenu:ReportMulti";
                public const string 报告审核 = "Contract:ReportMenu:ReportAudit";
                public const string 报告审核_政府 = "Contract:ReportMenu:ReportAudit_ZF";
                public const string 报告批准 = "Contract:ReportMenu:ReportPiz";
                public const string 报告批准_政府 = "Contract:ReportMenu:ReportPiz_ZF";
                public const string 已编制报告 = "Contract:ReportMenu:ReportBuildDone";
                public const string 已编制报告_政府 = "Contract:ReportMenu:ReportBuildDone_ZF";
                public const string 已完成报告_公共场所_消毒 = "Contract:ReportMenu:ReportBuildeDone_GG_XD";
                public const string 报告查询 = "Contract:ReportMenu:ReportSearch";
                public const string 报告进度查看 = "Contract:ReportMenu:ReportProgress";
                public const string 报告模板管理 = "Contract:ReportMenu:ReportTemplate";
                public const string 每日应出报告 = "Contract:ReportMenu:ReportToday";
                public const string 每日应出报告样品_销售 = "Contract:ReportMenu:SampleTodayReportForSales";
                public const string 每日应出报告合同_销售 = "Contract:ReportMenu:ContractTodayReportForSales";
                public const string 每日应出报告合同_报告 = "Contract:ReportMenu:ContractTodayReportForReport";
                public const string 报告修改申请 = "Contract:ReportMenu:ReportApply";
                public const string 我的报告修改待办申请 = "Contract:ReportMenu:MyReportApply";
                public const string 报告修改申请评审 = "Contract:ReportMenu:ReportApplyAudit";
                public const string 预警样品 = "Contract:ReportMenu:WarmSample";
                public const string 政府预警样品 = "Contract:ReportMenu:WarmSampleZf";
                public const string 报告查看批准 = "Contract:ReportMenu:ViewAudit";
                public const string 报告批准查看全部 = "Contract:ReportMenu:ViewReportAll";
                public const string 报告审核_环境 = "Contract:ReportMenu:ReportAudit_HJ";
            }

            //产品数据查询
            public class ProcategorySearchMenu
            {
                public const string 产品及标准数据查询 = "Contract:ProcategorySearchMenu:ProcategoryStandard";
            }

            //抽检报价系统
            public class CJ_PriceMenu
            {
                public const string 分类项目管理 = "Contract:CJ_PriceMenu:HkTypetree";
                public const string 报价项目管理 = "Contract:CJ_PriceMenu:.Priceproj";
                public const string 检测项目管理 = "Contract:CJ_PriceMenu:Bitems";
            }

            //政府任务系统
            public class ZF_TaskMenu
            {
                public const string 分类项目管理 = "Contract:ZF_TaskMenu:HkTaskTypetree";
                public const string 检测项目管理 = "Contract:ZF_TaskMenu:TaskBitems";
            }

            //电子设备
            public class ComputerMenu
            {
                public const string 电脑 = "Contract:ComputerMenu:Computer";
                public const string 电脑维修记录 = "Contract:ComputerMenu:ComputerService";
            }

            //分包报告上传
            public class SubpackagerUploadMenu
            {
                public const string 分包报告上传 = "Contract:SubpackagerUploadMenu:SubpackagerUpload";
            }

            //环境
            public class HuanJing
            {
                public const string 环境协议 = "Contract:HuanJing:Contract";
                public const string 环境回款记录 = "Contract:HuanJing:BackMoney";
                public const string 环境发票记录 = "Contract:HuanJing:Invoice";
            }

            //系统设置
            public class SystemManagement
            {
                public const string 部门管理 = "Contract:InstrumentEquipmentMenu:DepartmentManagement";
            }

            //客户中心
            public class CostumerMenu
            {
                public const string 我的客户 = "Contract:CostumerMenu:MyCostumer";
                public const string 我能看到的客户 = "Contract:CostumerMenu:MyEnableCostumer";
            }

            //实验中心
            public class LibraryMenu
            {
                public const string 结果录入 = "Contract:LibraryMenu:Result";
                public const string 已提交记录 = "Contract:LibraryMenu:ResultHistory";
                public const string 已提交记录_抽检 = "Contract:LibraryMenu:Result_ZF";
                public const string 国抽实验数据录入 = "Contract:LibraryMenu:ResultHistory_ZF";
                public const string 天平使用记录 = "Contract:LibraryMenu:HkLibrahisinfo";
                public const string 实验室领样记录 = "Contract:LibraryMenu:HkLibraryaccinfo";
                public const string 样品节点查看 = "Contract:LibraryMenu:hkSampleFlow";
                public const string 政府实验室样品接收 = "Contract:LibraryMenu:hkSampleZfAcc";
                public const string 任务单审核 = "Contract:LibraryMenu:TasklistAudit";
                public const string 任务单 = "Contract:LibraryMenu:Tasklist";
                public const string 任务单原始记录 = "Contract:LibraryMenu:TasklistOrgRecord";
                public const string 任务单原始记录审核 = "Contract:LibraryMenu:TasklistOrgRecordAudit";
                public const string 任务单查看全部 = "Contract:LibraryMenu:TasklistAll";
                public const string 方法和记录关联 = "Contract:LibraryMenu:MethodOrgRelation";
                public const string 任务单原始记录复检申请书审核 = "Contract:LibraryMenu:TasklistOrgRecordRecheckApplyAudit";
                public const string 未完成样品统计 = "Contract:LibraryMenu:UnfinishedSampleStatistic";
                public const string 未完成样品统计全部 = "Contract:LibraryMenu:UnfinishedSampleStatisticAll";
            }

            //数据管理
            public class DataManageMenu
            {
                public const string 检测项目管理 = "Contract:DataManageMenu:Factorory";
                public const string 检测方法管理 = "Contract:DataManageMenu:Standard";
                public const string 项目方法关联 = "Contract:DataManageMenu:FactororyStandard";
                public const string 限值单位管理 = "Contract:DataManageMenu:Unit";
                public const string 产品分类管理 = "Contract:DataManageMenu:Procategory";
                public const string 产品及标准管理 = "Contract:DataManageMenu:ProcategoryAndStandard";
                public const string 单位 = "Contract:DataManageMenu:Unit";
                public const string 单位转换 = "Contract:DataManageMenu:UnitConvert";
                public const string 导出项目方法 = "Contract:DataManageMenu:ExportFactororyStandard";

                public const string 公用基础数据修改 = "Contract:DataManageMenu:CommonBaseDataUpdate";
                public const string 委托基础数据修改 = "Contract:DataManageMenu:WtBaseDataUpdate";
                public const string 抽检基础数据修改 = "Contract:DataManageMenu:CjBaseDataUpdate";
                public const string 环境基础数据修改 = "Contract:DataManageMenu:HjBaseDataUpdate";
                public const string 企标基础数据修改 = "Contract:DataManageMenu:QbBaseDataUpdate";
            }

            //仪器设备管理
            public class SampleManageMenu
            {
                public const string 我的待下单样品 = "Contract:SampleManageMenu:mywaitsample";
                public const string 待下单样品 = "Contract:SampleManageMenu:waitsample";
                public const string 我的已下单样品 = "Contract:SampleManageMenu:Mywaitsamplehis";
                public const string 样品接收 = "Contract:SampleManageMenu:sampleacc";
                public const string 样品流转 = "Contract:SampleManageMenu:sampleflow";
                public const string 已流转样品 = "Contract:SampleManageMenu:sampleflowhis";
                public const string 已作废样品 = "Contract:SampleManageMenu:ObsoleteSample";
                public const string 样品入库管理 = "Contract:SampleManageMenu:Sampleintoku";
                public const string 备样样品管理 = "Contract:SampleManageMenu:Sampleintokumanage";
                public const string 样品结果统计导出 = "Contract:SampleManageMenu:samplessexcel";
                public const string 样品统计 = "Contract:SampleManageMenu:sampleTJ";
                public const string 制样室权限 = "Contract:SampleManageMenu:BuildSample";
                public const string 制样记录 = "Contract:SampleManageMenu:HkBuildsampleinfo";
                public const string 制样室样品入库管理 = "Contract:SampleManageMenu:Z_Sampleintoku";
                public const string 制样室备样样品管理 = "Contract:SampleManageMenu:Z_Sampleintokumanage";
            }

            //仪器设备管理
            public class InstrumentEquipmentMenu
            {
                public const string 仪器设备 = "Contract:InstrumentEquipmentMenu:EquipmentName";
            }

            //政府食品
            public class ZF_SPMenu
            {
                public const string 政府食品协议 = "Contract:ZF_SPMenu:ContractZF_SP";
                public const string 政府食品_回款记录 = "Contract:ZF_SPMenu:ZF_back";
                public const string 政府食品_发票记录 = "Contract:ZF_SPMenu:ZF_Invoice";
                public const string 政府食品_查看全部 = "Contract:ZF_SPMenu:ZF_ViewAll";
            }

            //政府信息
            public class ZF_Info
            {
                public const string 政府合同信息 = "Contract:ZF_Info:ContractInfo";
                public const string 实验室数据查看 = "Contract:ZF_Info:Library";

                public const string 政府产品及标准管理 = "Contract:ZF_Info:ProcategoryAndStandard";
                public const string 政府产品分类管理 = "Contract:ZF_Info:ProcategoryType";
                public const string 政府检测项目 = "Contract:ZF_Info:Factory";
                public const string 政府检测方法 = "Contract:ZF_Info:Standard";
                public const string 政府项目关联方法 = "Contract:ZF_Info:FactoryStandard";

                public const string 政府_样品管理 = "Contract:ZF_Info:SampleManage";
                public const string 政府_未提交样品 = "Contract:ZF_Info:waitaccsample";
                public const string 政府_待接收样品 = "Contract:ZF_Info:waitsample";
                public const string 政府_待流转样品 = "Contract:ZF_Info:flowsample";
                public const string 政府_已流转样品 = "Contract:ZF_Info:flowsampleok";
                public const string 政府_已作废样品 = "Contract:ZF_Info:SampleZfObsolete";
                public const string 政府_不合格样品 = "Contract:ZF_Info:SampleZfDisqualification";

                public const string 数据导入 = "Contract:ZF_Info:ImportData";
                public const string 数据柱形图 = "Contract:ZF_Info:Histogram";
            }

            //任务平台
            public class Task_Info
            {
                public const string 任务管理权限 = "Contract:Task_Info:Manage";
                public const string 任务参与人员 = "Contract:Task_Info:Gust";
            }

            //资质查询
            public class ZZ_Search
            {
                public const string 资质查询 = "Contract:ZZ_Search:FactororyStandardSerach";
            }

            //样品处置记录
            public class DisposalRecords
            {
                public const string 全部样品处置记录 = "Contract:DisposalRecords:AllDisposalRecords";
                public const string 我的未提交样品处置记录 = "Contract:DisposalRecords:DisposalRecordsUnCommit";
                public const string 我的驳回样品处置记录 = "Contract:DisposalRecords:DisposalRecordsMyReject";
                public const string 待审核样品处置记录 = "Contract:DisposalRecords:DisposalRecordsAudit";

            }

            //单项
            public class OnlyOne
            {
                public const string 政府非食品年协议 = "Contract:OnlyOne:ZF_FSP_Year";
                public const string 企业委托年协议 = "Contract:OnlyOne:QY_Year";
            }

            //运输单
            public class SampleTransportation
            {
                public const string 全部样品运输单 = "Contract:SampleTransportation:All";
                public const string 待接收样品运输单 = "Contract:SampleTransportation:Acc";
            }

            //作废申请记录
            public class ObsoleteReason
            {
                public const string 全部申请记录 = "Contract:ObsoleteReason:All";
                public const string 待审核记录 = "Contract:ObsoleteReason:Audit";

            }

            //样品作废
            public class ObsoleteSample
            {
                public const string 样品作废申请记录 = "Contract:SampleObsolete:All";
                public const string 样品作废申请审核 = "Contract:SampleObsolete:Audit";
            }
        }
    }
}
