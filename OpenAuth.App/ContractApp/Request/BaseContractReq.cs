﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseContractReq
    {
        /// <summary>
        ///受检人电话
        /// </summary>
        public string ct_inspection_phone { get; set; }

        /// <summary>
        ///复制的合同编号
        /// </summary>
        public string copyCtCode { get; set; }

        /// <summary>
        ///报告时间
        /// </summary>
        public DateTime? ct_report_time { get; set; }

        /// <summary>
        ///报告邮寄时间
        /// </summary>
        public string ct_annual_sales { get; set; }

        /// <summary>
        ///报告服务类型txt
        /// </summary>
        public string presentation_nature_txt { get; set; }

        /// <summary>
        ///纳税人识别号
        /// </summary>
        public string identification_number { get; set; }

        /// <summary>
        ///关联
        /// </summary>
        public decimal? contract_sample { get; set; }

        /// <summary>
        ///旧创建者
        /// </summary>
        public string oldCreateBy { get; set; }

        /// <summary>
        ///检测类别
        /// </summary>
        public string TestCategory { get; set; }

        /// <summary>
        ///委托单位选择
        /// </summary>
        public decimal? hkcompany_id { get; set; }

        /// <summary>
        ///是否同意方法偏离
        /// </summary>
        public bool? methodDeviate { get; set; }

        /// <summary>
        ///分包说明txt
        /// </summary>
        public string subpackageTxt { get; set; }

        /// <summary>
        ///开票回款金额
        /// </summary>
        public decimal? openbackcose { get; set; }

        /// <summary>
        ///报告服务类型value
        /// </summary>
        public string presentation_nature { get; set; }

        /// <summary>
        ///受检单位指定负责人
        /// </summary>
        public string ct_entrust_name_comm { get; set; }

        /// <summary>
        ///加盖CMA印章
        /// </summary>
        public bool? stampCMA { get; set; }

        /// <summary>
        ///共同委托单位地址
        /// </summary>
        public string ct_entrust_company_address_comm { get; set; }

        /// <summary>
        ///协议类型value
        /// </summary>
        public string ct_license_type { get; set; }

        /// <summary>
        ///指定负责人电话
        /// </summary>
        public string ct_entrust_phone_comm { get; set; }

        /// <summary>
        ///委托人电话
        /// </summary>
        public string ct_entrust_phone { get; set; }

        /// <summary>
        ///未回款金额
        /// </summary>
        public decimal? no_backcose { get; set; }

        /// <summary>
        ///回款节点txt
        /// </summary>
        public string ct_payment_txt { get; set; }

        /// <summary>
        ///检测领域id
        /// </summary>
        public string ct_detection_id { get; set; }

        /// <summary>
        ///发票地址
        /// </summary>
        public string invoice_address { get; set; }

        /// <summary>
        ///是否同意分包
        /// </summary>
        public bool? IsSubpackage { get; set; }

        /// <summary>
        ///评审人id
        /// </summary>
        public int? applyInvoiceuser { get; set; }

        /// <summary>
        ///超出折扣
        /// </summary>
        public int? OutDiscount { get; set; }

        /// <summary>
        ///委托人邮箱
        /// </summary>
        public string ct_entrust_email { get; set; }

        /// <summary>
        ///流程实例id
        /// </summary>
        public string proc_ins_id { get; set; }

        /// <summary>
        ///费用折扣
        /// </summary>
        public string ct_cose_discount { get; set; }

        /// <summary>
        ///费用支付单位
        /// </summary>
        public string ct_cose_payment { get; set; }

        /// <summary>
        ///环境发票类型value
        /// </summary>
        public string InvoiceType { get; set; }

        /// <summary>
        ///环境发票类型txt
        /// </summary>
        public string InvoiceTypeTxt { get; set; }

        /// <summary>
        ///旧委托单位选择
        /// </summary>
        public string oldHkCompanyId { get; set; }

        /// <summary>
        ///标准规范
        /// </summary>
        public string StandardSpecification { get; set; }

        /// <summary>
        ///计划采样时间
        /// </summary>
        public DateTime? samplingdate { get; set; }

        /// <summary>
        ///回款节点申请材料
        /// </summary>
        public string ct_payment_files { get; set; }

        /// <summary>
        ///更新者
        /// </summary>
        public string update_by { get; set; }

        /// <summary>
        ///样品是否留样
        /// </summary>
        public bool? RetentionSamples { get; set; }

        /// <summary>
        ///审批通过时间
        /// </summary>
        public DateTime? approval { get; set; }

        /// <summary>
        ///收检日期
        /// </summary>
        public string sp_s_36 { get; set; }

        /// <summary>
        ///检测领域txt
        /// </summary>
        public string ct_type_txt { get; set; }

        /// <summary>
        ///是否已合同金额为准
        /// </summary>
        public bool? is_ctpaycose { get; set; }

        /// <summary>
        ///付款单位
        /// </summary>
        public string PayCompany { get; set; }

        /// <summary>
        ///委托单位
        /// </summary>
        public string ct_entrust_company { get; set; }

        /// <summary>
        ///委托联系人姓名
        /// </summary>
        public string ct_entrust_name { get; set; }

        /// <summary>
        ///CATL
        /// </summary>
        public bool? stampCATL { get; set; }

        /// <summary>
        ///发票邮寄地址
        /// </summary>
        public string ct_inspection_company_address { get; set; }

        /// <summary>
        ///发票类型txt
        /// </summary>
        public string ct_invoice_type { get; set; }

        /// <summary>
        ///快递费
        /// </summary>
        public decimal? express_cose { get; set; }

        /// <summary>
        ///样品来源
        /// </summary>
        public string SampleSource { get; set; }

        /// <summary>
        ///报告份数txt
        /// </summary>
        public string sp_cc_18_txt { get; set; }

        /// <summary>
        ///折扣
        /// </summary>
        public decimal? discount { get; set; }

        /// <summary>
        ///业务员名称
        /// </summary>
        public string beforeusername { get; set; }

        /// <summary>
        ///业务员id
        /// </summary>
        public string beforeuser { get; set; }

        /// <summary>
        ///CNAS
        /// </summary>
        public bool? stampCNAS { get; set; }

        /// <summary>
        ///报告形式txt
        /// </summary>
        public string sp_cc_17_txt { get; set; }

        /// <summary>
        ///分包金额(元)
        /// </summary>
        public decimal? sub_package_pay { get; set; }

        /// <summary>
        ///系统总价
        /// </summary>
        public decimal? sys_totalprice { get; set; }

        /// <summary>
        ///检测领域value
        /// </summary>
        public string ct_type { get; set; }

        /// <summary>
        ///旧业务员
        /// </summary>
        public string oldBeforeUser { get; set; }

        /// <summary>
        ///报告领取方式txt
        /// </summary>
        public string ct_report_receive_txt { get; set; }

        /// <summary>
        ///所属年协议id
        /// </summary>
        public int? yearid { get; set; }

        /// <summary>
        ///技术要求
        /// </summary>
        public string ct_technical_requirements { get; set; }

        /// <summary>
        ///Method Deviate
        /// </summary>
        public string TempSample { get; set; }

        /// <summary>
        ///采样费
        /// </summary>
        public decimal? sample_cose { get; set; }

        /// <summary>
        ///完成时间
        /// </summary>
        public DateTime? donetime { get; set; }

        /// <summary>
        ///发票邮寄时间
        /// </summary>
        public string ct_license_number { get; set; }

        /// <summary>
        ///合同金额(元)
        /// </summary>
        public decimal? ct_pay_cose { get; set; }

        /// <summary>
        ///取样费
        /// </summary>
        public decimal? takesample_cose { get; set; }

        /// <summary>
        ///开户行账号
        /// </summary>
        public string opening_bank_number { get; set; }

        /// <summary>
        ///委托方指定标准
        /// </summary>
        public string CompanyStandardSpecification { get; set; }

        /// <summary>
        ///报告语言txt
        /// </summary>
        public string report_language_txt { get; set; }

        /// <summary>
        ///分包报告日期
        /// </summary>
        public DateTime? sub_report_time { get; set; }

        /// <summary>
        ///是否允许申请变更
        /// </summary>
        public bool? allowedit { get; set; }

        /// <summary>
        ///发票抬头
        /// </summary>
        public string ct_invoice_title { get; set; }

        /// <summary>
        ///是否回款
        /// </summary>
        public bool? money_back { get; set; }

        /// <summary>
        ///已开票金额
        /// </summary>
        public decimal? opencose { get; set; }

        /// <summary>
        ///报告份数value 
        /// </summary>
        public string sp_cc_18 { get; set; }

        /// <summary>
        ///所属部门
        /// </summary>
        public string beforeoffice { get; set; }

        /// <summary>
        ///现场记录id
        /// </summary>
        public int? xcorgid { get; set; }

        /// <summary>
        ///开户行
        /// </summary>
        public string opening_bank { get; set; }

        /// <summary>
        ///样品状态
        /// </summary>
        public string SampleState { get; set; }

        /// <summary>
        ///是否需要判定
        /// </summary>
        public bool? IsDecide { get; set; }

        /// <summary>
        ///开票未回款金额
        /// </summary>
        public decimal? openobackcose { get; set; }

        /// <summary>
        ///报告模板
        /// </summary>
        public int? ReportTemplate { get; set; }

        /// <summary>
        ///回款金额
        /// </summary>
        public decimal? backcose { get; set; }

        /// <summary>
        ///发票id
        /// </summary>
        public decimal? invoice_id { get; set; }

        /// <summary>
        ///协议类型txt
        /// </summary>
        public string ct_license_type_txt { get; set; }

        /// <summary>
        ///发票是否邮寄
        /// </summary>
        public bool? ct_inspection_email { get; set; }

        /// <summary>
        ///发票状态txt
        /// </summary>
        public string InvoiceStateTxt { get; set; }

        /// <summary>
        ///是否返还样品
        /// </summary>
        public bool? ct_return_sample { get; set; }

        /// <summary>
        ///合同状态value
        /// </summary>
        public string ct_state { get; set; }

        /// <summary>
        ///报告领取方式value
        /// </summary>
        public string ct_report_receive { get; set; }

        /// <summary>
        ///回款节点value(报告发送前回款，发送后回款)
        /// </summary>
        public string ct_payment_method { get; set; }

        /// <summary>
        ///委托单位地址
        /// </summary>
        public string ct_entrust_company_address { get; set; }

        /// <summary>
        ///分包项目
        /// </summary>
        public string SubpackageObject { get; set; }

        /// <summary>
        ///旧编号
        /// </summary>
        public string oldid { get; set; }

        /// <summary>
        ///样品储存
        /// </summary>
        public string SampleStored { get; set; }

        /// <summary>
        ///合同状态txt
        /// </summary>
        public string ct_state_txt { get; set; }

        /// <summary>
        ///结算人
        /// </summary>
        public string endcose_user { get; set; }

        /// <summary>
        ///项目采样地址
        /// </summary>
        public string ObjectSamplingAddress { get; set; }

        /// <summary>
        ///大写
        /// </summary>
        public string AmountMoney { get; set; }

        /// <summary>
        ///结果的解释及说明
        /// </summary>
        public string ct_explain { get; set; }

        /// <summary>
        ///其他
        /// </summary>
        public string TheOther { get; set; }

        /// <summary>
        ///结果判断
        /// </summary>
        public string ct_resule_judge { get; set; }

        /// <summary>
        ///二维码
        /// </summary>
        public string ct_sqr { get; set; }

        /// <summary>
        ///加急费
        /// </summary>
        public decimal? emergenct_fee { get; set; }

        /// <summary>
        ///报告形式value
        /// </summary>
        public string sp_cc_17 { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string ct_remarl { get; set; }

        /// <summary>
        ///报告邮寄地址
        /// </summary>
        public string ct_inspection_name { get; set; }

        /// <summary>
        ///提交时间
        /// </summary>
        public DateTime? CommitTime { get; set; }

        /// <summary>
        ///共同委托单位
        /// </summary>
        public string ct_entrust_company_comm { get; set; }

        /// <summary>
        ///未开票金额
        /// </summary>
        public decimal? no_opencose { get; set; }

        /// <summary>
        ///签收时间
        /// </summary>
        public DateTime? sign_time { get; set; }

        /// <summary>
        ///报告是否邮寄
        /// </summary>
        public bool? ct_business_license { get; set; }

        /// <summary>
        ///提交人id
        /// </summary>
        public int? commituser { get; set; }

        /// <summary>
        ///是否结算value
        /// </summary>
        public bool? endcose { get; set; }

        /// <summary>
        ///分包说明value
        /// </summary>
        public int? subpackage { get; set; }

        /// <summary>
        ///发票状态value
        /// </summary>
        public string InvoiceState { get; set; }

        /// <summary>
        ///报告语言value
        /// </summary>
        public string report_language { get; set; }

        /// <summary>
        ///变更属性列表
        /// </summary>
        public string changeProperties { get; set; }

        /// <summary>
        ///联系电话
        /// </summary>
        public string invoice_telephone { get; set; }

        /// <summary>
        ///分包项目是否纳入本单位检测报告
        /// </summary>
        public bool? IsExaminingReport { get; set; }

        /// <summary>
        ///合同扫描件
        /// </summary>
        public string ct_file { get; set; }

        /// <summary>
        ///受检单位
        /// </summary>
        public string ct_inspection_company { get; set; }

        /// <summary>
        ///发票类型value
        /// </summary>
        public string ct_invoice_type_value { get; set; }

        /// <summary>
        ///是否开票
        /// </summary>
        public bool? Invoice { get; set; }

        /// <summary>
        ///序号
        /// </summary>
        public int? ct_serial_number { get; set; }

        /// <summary>
        ///协议编号
        /// </summary>
        public string ct_number { get; set; }

        ///// <summary>
        /////删除标记
        ///// </summary>
        //public string del_flag { get; set; }

        ///// <summary>
        /////旧创建日期
        ///// </summary>
        //public DateTime? oldCreateDate { get; set; }

        ///// <summary>
        /////创建时间
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////更新时间
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////创建时间（后端操作）
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建时间（后端操作）
        ///// </summary>
        //public string CreateUserId { get; set; }
    }
}
