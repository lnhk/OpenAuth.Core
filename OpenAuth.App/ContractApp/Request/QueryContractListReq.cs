using System;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 加载合同列表请求
    /// </summary>
    public class QueryContractListReq : PageReq
    {
        /// <summary>
        /// 合同编号(可选)
        /// </summary>
        public string ct_number { get; set; }

        /// <summary>
        /// 委托单位(可选)
        /// </summary>
        public string ct_entrust_company { get; set; }

        /// <summary>
        /// 所属业务(可选)
        /// </summary>
        public string beforeusername{get;set;}

        /// <summary>
        /// 合同状态(可选)
        /// </summary>
        public string ct_state_txt { get; set;}

        /// <summary>
        /// 报告邮寄(可选)
        /// </summary>
        public bool? ct_inspection_email { get; set; }

        /// <summary>
        /// 协议类型(可选)
        /// </summary>
        public string ct_license_type_txt { get; set; }

        /// <summary>
        /// 是否开票(可选)
        /// </summary>
        public bool? Invoice { get; set; }

        /// <summary>
        /// 是否回款(可选)
        /// </summary>
        public bool? money_back { get; set; }

        /// <summary>
        /// 审批通过时间开始(可选)
        /// </summary>
        public DateTime? approvalStartTime { get; set; }

        /// <summary>
        ///  审批通过时间结束(可选)
        /// </summary>
        public DateTime? approvalEndTime { get; set; }

        /// <summary>
        /// 样品名称(可选)
        /// </summary>
        public string sp_s_7 { get; set; }

        /// <summary>
        /// 样品备注(可选)
        /// </summary>
        public string sp_remark { get; set; }

        /// <summary>
        /// 生产单位(可选)
        /// </summary>
        public string sp_cs_5 { get; set; }

        /// <summary>
        /// 受检单位(可选)
        /// </summary>
        public string sp_cs_w_1 { get; set; }

        /// <summary>
        /// 包含分包(可选)
        /// </summary>
        public bool? isFactorName { get; set; }

        /// <summary>
        /// 样品判断依据(可选)
        /// </summary>
        public string sp_s_32 { get; set; }

        /// <summary>
        /// 检验依据(可选)
        /// </summary>
        public string spdata_5 { get; set; }

        /// <summary>
        /// 检验项目(可选)
        /// </summary>
        public string spdata_1 { get; set; }

        /// <summary>
        /// 报告日期(可选)
        /// </summary>
        public DateTime? ReportDate { get; set; }
    }
}
