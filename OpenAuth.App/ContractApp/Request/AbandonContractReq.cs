﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 作废合同请求
    /// </summary>
    public class AbandonContractReq
    {
        /// <summary>
        /// 合同id
        /// </summary>
        public decimal ContractId { get; set; }

        /// <summary>
        /// 审批备注
        /// </summary>
        public string Messge { get; set; }

        //public string Code { get; set; }
        //public string CtNumber { get; set; }
        //public string Person { get; set; }
        //public string Remark { get; set; }
        //public ContractListRequest re { get; set; }
        ///// <summary>
        ///// 审批信息
        ///// </summary>
        //public string ApproveInfo { get; set; }
    }
}
