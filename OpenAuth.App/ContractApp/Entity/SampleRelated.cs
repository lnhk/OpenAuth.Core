﻿using OpenAuth.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Entity
{
    /// <summary>
    /// 样品及样品关联的检测项目
    /// </summary>
    public class SampleRelated
    {
        public SampleRelated()
        {
            Sample = null;
            SampleDatas = new List<SampleData>();
        }

        /// <summary>
        /// 样品
        /// </summary>
        public Sample Sample { get; set; }
        /// <summary>
        /// 样品关联的检测项目
        /// </summary>
        public List<SampleData> SampleDatas{get;set; }
    }
}
