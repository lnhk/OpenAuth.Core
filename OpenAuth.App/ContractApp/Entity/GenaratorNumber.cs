using System.Collections.Generic;
using OpenAuth.Repository.Domain;

namespace OpenAuth.App.Entity
{
    public static class GenaratorNumber
    {
        public static int Year { get; set; }
        public static Dictionary<int, int> DomainNumber { get; set; }
        public static List<Category> Categories { get; set; }
    }
}