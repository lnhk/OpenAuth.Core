﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Entity
{
    /// <summary>
    /// 样品及样品关联的检测项目和合同关联的属性
    /// </summary>
    public class SampleContractRelated
    {
        public SampleContractRelated()
        {
            SampleRelateds = new List<SampleRelated>();
            this.ct_report_time = null;
            this.emergenct_fee = 0;
            this.sub_package_pay = 0;
            this.ct_pay_cose = 0;
        }

        /// <summary>
        /// 样品及样品关联的检测项目
        /// </summary>
        public List<SampleRelated> SampleRelateds{ get; set; }
        /// <summary>
        /// 样品关联合同的报告日期
        /// </summary>
        public DateTime? ct_report_time { get; set; }
        /// <summary>
        /// 样品关联合同的系统总价
        /// </summary>
        public decimal? emergenct_fee { get; set; }
        /// <summary>
        /// 样品关联合同的分包价格
        /// </summary>
        public decimal? sub_package_pay { get; set; }
        /// <summary>
        ///样品关联合同的 合同金额
        /// </summary>
        public decimal? ct_pay_cose { get; set; }
    }
}
