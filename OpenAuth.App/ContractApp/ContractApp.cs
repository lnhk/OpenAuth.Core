using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Aspose.Words.Fonts;
using Aspose.Words.Loading;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using NPOI.OpenXmlFormats.Wordprocessing;
using NPOI.XWPF.UserModel;
using OpenAuth.App.Common.Entity;
using OpenAuth.App.Entity;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Enum;
using OpenAuth.Repository.Enum.ContractEnum;
using OpenAuth.Repository.Enum.SumpleEnum;
using OpenAuth.Repository.Interface;
using Spire.Pdf;
using Spire.Pdf.Graphics;

namespace OpenAuth.App
{
    public class ContractApp : BaseLongApp<Contract, OpenAuthDBContext>
    {
        private CategoryApp _categoryApp { get; set; }
        private ContractUpDataApp _contractUpDataApp { get; set; }
        private SampleUpDataApp _sampleUpDataApp { get; set; }
        private SampleApp _spApp { get; set; }
        private SysLogApp _sysLogApp { get; set; }

        public ContractApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Contract, OpenAuthDBContext> repository, IAuth auth,
    CategoryApp categoryApp, SampleApp sampleApp,SysLogApp sysLogApp, ContractUpDataApp contractUpDataApp, SampleUpDataApp sampleUpDataApp) : base(unitWork, repository, auth)
        {
            _categoryApp = categoryApp;
            _spApp = sampleApp;
            _sysLogApp = sysLogApp;
            _contractUpDataApp = contractUpDataApp;
            _sampleUpDataApp = sampleUpDataApp;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryContractListReq req)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            var result = new TableData();
            var contracts = GetDataPrivilege("c");

            if (!string.IsNullOrEmpty(req.ct_number))
            {
                contracts = contracts.Where(c => c.ct_number.Contains(req.ct_number));
            }

            if (!string.IsNullOrEmpty(req.ct_entrust_company))
            {
                contracts = contracts.Where(c => c.ct_entrust_company.Contains(req.ct_entrust_company));
            }

            if (!string.IsNullOrEmpty(req.beforeusername))
            {
                contracts = contracts.Where(c => c.beforeusername.Contains(req.beforeusername));
            }

            if (!string.IsNullOrEmpty(req.ct_state_txt))
            {
                contracts = contracts.Where(c => c.ct_state_txt.Contains(req.ct_state_txt));
            }

            if (req.ct_inspection_email != null)
            {
                contracts = contracts.Where(c => c.ct_inspection_email == (bool)req.ct_inspection_email);
            }

            if (!string.IsNullOrEmpty(req.ct_license_type_txt))
            {
                contracts = contracts.Where(c => c.ct_license_type_txt.Contains(req.ct_license_type_txt));
            }

            if (req.Invoice != null)
            {
                contracts = contracts.Where(c => c.Invoice == req.Invoice);
            }

            if (req.money_back != null)
            {
                contracts = contracts.Where(c => c.money_back == req.money_back);
            }

            if (req.approvalStartTime != null)
            {
                contracts.Where(c => c.approval >= req.approvalStartTime);
            }
            if (req.approvalEndTime != null)
            {
                req.approvalEndTime = ((DateTime)req.approvalEndTime).AddDays(1);
                contracts = contracts.Where(c => c.approval <= req.approvalEndTime);
            }

            if (!string.IsNullOrEmpty(req.sp_s_7))
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.sp_s_7.Contains(req.sp_s_7)
                            select c;
            }

            if (!string.IsNullOrEmpty(req.sp_remark))
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.remarks.Contains(req.sp_remark)
                            select c;
            }

            if (!string.IsNullOrEmpty(req.sp_cs_5))
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.sp_cs_5.Contains(req.sp_cs_5)
                            select c;
            }

            if (!string.IsNullOrEmpty(req.sp_cs_w_1))
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.sp_cs_w_1.Contains(req.sp_cs_w_1)
                            select c;
            }

            if (req.isFactorName != null && req.isFactorName == true)
            {
                var subQuery = from s in UnitWork.Find<Sample>(null)
                               join sd in UnitWork.Find<SampleData>(null)
                               on s.Id equals sd.sp_id
                               where sd.spdata_13_id == "fenbao"
                               || sd.subpackage == true
                               select s;
                contracts = from c in contracts
                            join sub in subQuery
                            on c.contract_sample equals sub.contract_sample
                            select c;
            }

            if (!string.IsNullOrEmpty(req.sp_s_32))
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.sp_s_32.Contains(req.sp_s_32)
                            select c;
            }

            if (!string.IsNullOrEmpty(req.spdata_5))
            {
                var subQuery = from s in UnitWork.Find<Sample>(null)
                               join sd in UnitWork.Find<SampleData>(null)
                               on s.Id equals sd.sp_id
                               where sd.spdata_5.Contains(req.spdata_5)
                               select s;
                contracts = from c in contracts
                            join sub in subQuery
                            on c.contract_sample equals sub.contract_sample
                            select c;
            }

            if (!string.IsNullOrEmpty(req.spdata_1))
            {
                var subQuery = from s in UnitWork.Find<Sample>(null)
                               join sd in UnitWork.Find<SampleData>(null)
                               on s.Id equals sd.sp_id
                               where sd.spdata_1.Contains(req.spdata_1)
                               select s;
                contracts = from c in contracts
                            join sub in subQuery
                            on c.contract_sample equals sub.contract_sample
                            select c;
            }

            if (req.ReportDate != null)
            {
                contracts = from c in contracts
                            join s in UnitWork.Find<Sample>(null)
                            on c.contract_sample equals s.contract_sample
                            where s.sp_s_43.Contains(Convert.ToDateTime(req.ReportDate).ToString("yyyy-MM-dd"))
                            select c;
            }

            result.data = contracts.OrderBy(c => c.Id).OrderByDescending(c => c.CreateTime)
            .Skip((req.page - 1) * req.limit)
            .Take(req.limit);
            result.count = await contracts.CountAsync();
            return result;
        }

        public void AddNoSave(Contract contract)
        {
            User user = _auth.GetCurrentUser().User;
            UnitWork.Add(contract);
            contract.CreateTime = DateTime.Now;
            contract.CreateUserId = user.Id;
            _sysLogApp.AddNoSave(new SysLog()
            {
                TypeId = "",
                TypeName = "合同",
                Content = "合同添加",
                CreateName = _auth.GetCurrentUser().User.Name,
                CreateId = _auth.GetCurrentUser().User.Id,
                Result = 1,
                RecordId = contract.Id
            });

        }

        public void Add(Contract contract)
        {
            AddNoSave(contract);
            UnitWork.Save();
        }
        
        public void UpdateNoSave(Contract contract)
        {
            User user = _auth.GetCurrentUser().User;
            UnitWork.Update(contract);
            contract.CreateTime = DateTime.Now;
            contract.CreateUserId = user.Id;
            _sysLogApp.AddNoSave(new SysLog()
            {
                TypeName = "合同",
                Content = "合同更改",
                CreateName = _auth.GetCurrentUser().User.Name,
                CreateId = _auth.GetCurrentUser().User.Id,
                RecordId = contract.Id
            });
        }

        public void Update(Contract contract)
        {
            UpdateNoSave(contract);
            UnitWork.Save();
        }

        public Contract GetBySampleId(decimal sampleId)
        {
            var query = from c in UnitWork.Find<Contract>(null) join s in UnitWork.Find<Sample>(null) on c.Id equals s.contract_sample
                        where s.Id ==sampleId select c;
            return query.FirstOrDefault();
        }

        /// <summary>
        /// 提交修改合同申请
        /// </summary>
        /// <param name="contract"></param>
        public void ChangeContractApply(UpdateContractReq obj) 
        {
            Contract contract = obj.MapTo<Contract>();
            // 存储合同修改内容
            AddOrUpdateContractUpDataReq req = new AddOrUpdateContractUpDataReq();
            req.ct_id = contract.Id;
            req.create_time = DateTime.Now;
            req.up_contract_data = contract.ToJson();
            _contractUpDataApp.Add(req);
        }

        /// <summary>
        /// 确认合同变更
        /// </summary>
        public void AckSubmitContract(decimal ContractId) 
        {
            // 获取contract原始数据
            // 获取合同内容
            var queryContract = from c in UnitWork.Find<Contract>(null)
                                where c.Id == ContractId
                                select c;
            Contract contract = queryContract.FirstOrDefault();
            if (contract == null)
            {
                throw new CommonException("未找到合同内容", 500);
            }

            // 获取contract需要修改的数据
            string sql = "SELECT * FROM [dbo].[hk_contract_up_data] WHERE ct_id = "+ ContractId + " AND ack_pass = 0 " +
                         "ORDER BY create_time DESC offset 0 ROWS FETCH NEXT 1 ROWS ONLY ";
            ContractUpData contractUpData = UnitWork.DynamicQuery<ContractUpData>(sql, null).FirstOrDefault();
            if (contractUpData == null) 
            {
                throw new CommonException("未找到修改内容", 500);
            }
            // 修改合同内容
            Contract upData = Json.ToObject<Contract>(contractUpData.up_contract_data);
            // 赋值不需要修改的内容
            upData.del_flag = contract.del_flag;
            upData.oldCreateDate = contract.oldCreateDate;
            upData.create_date = contract.create_date;
            upData.update_date = contract.update_date;
            upData.create_by = contract.create_by;
            upData.CreateTime = contract.CreateTime;
            upData.CreateUserId = contract.CreateUserId;
            upData.Id = contract.Id;
            upData.ct_number = contract.ct_number;
            upData.ct_serial_number = contract.ct_serial_number;
            // 赋值
            EntityToEntity(upData, contract);
            // 更新合同
            UnitWork.Update<Contract>(contract);
            // 确认合同修改内容为通过状态
            contractUpData.ack_pass = 1;
            UnitWork.Update<ContractUpData>(contractUpData);

            // 获取需要修改的样品数据
            string sql2 = "SELECT * FROM [dbo].[hk_sample_up_data] WHERE ct_id = "+ ContractId +" AND ack_pass = 0";
            List<SampleUpData> sampleUpDataList = UnitWork.DynamicQuery<SampleUpData>(sql2, null).ToList();
            if (sampleUpDataList.Count != 0) 
            {
                foreach (var item in sampleUpDataList)
                {
                    // 某个样品数据
                    decimal sampleId = (decimal) item.sample_id;
                    // 获取原始样品数据
                    var queryOldSample = from s in UnitWork.Find<Sample>(null)
                                         where s.Id == sampleId
                                         select s;
                    Sample oldSample = queryOldSample.FirstOrDefault();
                    if (oldSample == null)
                    {
                        throw new CommonException("未找到样品信息", 500);
                    }
                    // 格式化样品信息
                    Sample upSample = Json.ToObject<Sample>(item.sample_up_data);
                    // 不需要修改的字段
                    upSample.create_date = oldSample.create_date;
                    upSample.del_flag = oldSample.del_flag;
                    upSample.oldid = oldSample.oldid;
                    upSample.create_by = oldSample.create_by;
                    upSample.oldCreateDate = oldSample.oldCreateDate;
                    upSample.oldCreateBy = oldSample.oldCreateBy;
                    upSample.update_date = oldSample.update_date;
                    upSample.update_by = oldSample.update_by;
                    upSample.CreateTime = oldSample.CreateTime;
                    upSample.CreateUserId = oldSample.CreateUserId;
                    upSample.Id = oldSample.Id;
                    // 赋值
                    EntityToEntity(upSample, oldSample);
                    // 更新样品
                    UnitWork.Update<Sample>(oldSample);
                    // 更新样品关联的检测项目
                    if (!string.IsNullOrEmpty(item.sample_item_up_data)) 
                    {
                        // 把样品的检测项目 格式化为list
                        List<SampleData> sampleDataList = Json.ToList<SampleData>(item.sample_item_up_data);
                        if (sampleDataList.Count != 0) 
                        {
                            // 循环获取每个原始项目数据
                            foreach (var item2 in sampleDataList)
                            {
                                // 获取原始样品检测项目数据
                                var queryOldSampleData = from sd in UnitWork.Find<SampleData>(null)
                                                         where sd.Id == item2.Id
                                                         select sd;
                                SampleData oldSampleData = queryOldSampleData.FirstOrDefault();
                                if (oldSampleData == null)
                                {
                                    throw new CommonException("未找到样品检测项目信息", 500);
                                }
                                // 不需要修改的字段
                                item2.del_flag = oldSampleData.del_flag;
                                item2.oldCreateDate = oldSampleData.oldCreateDate;
                                item2.update_by = oldSampleData.update_by;
                                item2.update_date = oldSampleData.update_date;
                                item2.create_date = oldSampleData.create_date;
                                item2.oldCreateBy = oldSampleData.oldCreateBy;
                                item2.create_by = oldSampleData.create_by;
                                item2.CreateUserId = oldSampleData.CreateUserId;
                                item2.CreateTime = oldSampleData.CreateTime;
                                // 赋值
                                EntityToEntity(item2, oldSampleData);
                                // 更新样品检测项目
                                UnitWork.Update<SampleData>(oldSampleData);
                            }
                        }
                    }

                    // 更新每个修改状态字段 通过状态
                    item.ack_pass = 1;
                    UnitWork.Update<SampleUpData>(item);
                }

            }

            UnitWork.Save();

        }

        public new void Delete(decimal[] ids)
        {
            DeleteNoSave(ids);
        }

        public void DeleteNoSave(decimal[] ids)
        {
            UnitWork.Delete<Contract>(u => ids.Contains(u.Id));
            foreach (decimal id in ids)
            {
                _sysLogApp.AddNoSave(new SysLog()
                {
                    TypeName = "合同",
                    Content = "合同删除",
                    CreateName = _auth.GetCurrentUser().User.Name,
                    CreateId = _auth.GetCurrentUser().User.Id,
                    RecordId = id
                });
            }
        }

        public void AddWithRelatedNoSave(Contract contract)
        {
            int xh = 1;

            contract.AmountMoney = CmycurD(contract.ct_pay_cose);
            contract.no_opencose = contract.ct_pay_cose;
            contract.no_backcose = contract.ct_pay_cose;

            if (!String.IsNullOrEmpty(contract.TempSample))
            {
                string[] sampleIds = contract.TempSample.Split(",");
                foreach (string sampleId in sampleIds)
                {
                    Sample sample = _spApp.Get(Convert.ToDecimal(sampleId));
                    sample.contract_sample = contract.Id;
                    sample.xh = xh;
                    sample.sp_s_code = $"{contract.ct_number}-xh";
                    _spApp.UpdateNoSave(sample);
                    xh += 1;
                }
            }
            else
            {
                GeneSampleCode(contract);
            }
            AddNoSave(contract);
            UnitWork.Save();
        }

        public void GetLog(Contract contract)
        {
            _sysLogApp.Add(new SysLog());
        }

        // 撤销某合同下的所有修改
        public void RevokeSampleAllUp(decimal ContractId)
        {
            // 删除合同修改
            _contractUpDataApp.DelByContractId(ContractId);
            // 删除样品及其检测项目修改
            _sampleUpDataApp.DelByContractId(ContractId);
        }

        public List<Contract> GetByCtNum(string ctNumber)
        {
            var query = from c in UnitWork.Find<Contract>(null) where c.ct_number == ctNumber select c;
            return query.ToList();
        }

        /// <summary>
        /// 合同编号生成
        /// </summary>
        /// <param name="contract"></param>
        public void GeneratorBum(Contract contract)
        {
            GenaratorNumber.Categories = _categoryApp.LoadByTypeId("DOMAIN_TYPE");
            if (GenaratorNumber.DomainNumber == null || GenaratorNumber.Year != DateTime.Now.Year)
            {
                GenaratorNumber.Year = DateTime.Now.Year;
                GenaratorNumber.DomainNumber = new Dictionary<int, int>();
                var objs = Repository.Find(null);
                var groups = objs.Where(o => ((DateTime)o.CreateTime).Year == GenaratorNumber.Year).GroupBy(o => o.ct_type);
                foreach (var item in groups)
                {
                    GenaratorNumber.DomainNumber.Add(Convert.ToInt32(item.Key), (int)item.ToList().Max(o => o.ct_serial_number));
                }
            }
            if (!GenaratorNumber.DomainNumber.ContainsKey(Convert.ToInt32(contract.ct_type)))
            {
                GenaratorNumber.DomainNumber.Add(Convert.ToInt32(contract.ct_type), 1);
            }
            else
            {
                GenaratorNumber.DomainNumber[Convert.ToInt32(contract.ct_type)] = GenaratorNumber.DomainNumber[Convert.ToInt32(contract.ct_type)] + 1;
            }
            Category c = GenaratorNumber.Categories.Find(o => o.DtValue == Convert.ToString(contract.ct_type));
            contract.ct_number = $"HK{(c.DtCode.Equals("None") ? "" : c.DtCode)}{GenaratorNumber.Year.ToString().Substring(2)}{GenaratorNumber.DomainNumber[Convert.ToInt32(contract.ct_type)].ToString().PadLeft(5, '0')}";
            contract.ct_serial_number = GenaratorNumber.DomainNumber[Convert.ToInt32(contract.ct_type)];
        }

        /// <summary>
        /// 合同编号生成,用于合同复制
        /// </summary>
        /// <returns></returns>
        public void GetNextContractNumber( Contract contract,string pPrefix, int pLength)
        {
            var prefix = "HK{0}{1}{2}";

            DateTime firstDayOfYear= new DateTime(DateTime.Now.Year, 1, 1);
            var query = from c in UnitWork.Find<Contract>(null).OrderByDescending(c => c.ct_serial_number)
                        where firstDayOfYear <= c.CreateTime
                        select c.ct_serial_number;
            string max = query.FirstOrDefault().ToString();

            long l;
            string ct_serial_number =Convert.ToString(max == null || !long.TryParse(max, out l) ? 1 : l + 1) ;
            Type typ = typeof(CtTypeEnum);
            string ct_number = string.Format(prefix, typ.GetEnumName(int.Parse(pPrefix)) == "None" ? "" : typ.GetEnumName(int.Parse(pPrefix)), DateTime.Now.Year.ToString().Substring(2), ct_serial_number.PadLeft(pLength, '0'));
            contract.ct_number = ct_number;
            contract.ct_serial_number = Convert.ToInt32(ct_serial_number);
        }

        //public GetNextNumberResult GetNextContractYearNumber()
        //{
        //    var prefix = "HKHT{0}{1}";

        //    var query = from c in _unitWork.Find<Contract>(null).OrderByDescending(c => c.ct_serial_number)
        //                where new DateTime(DateTime.Now.Year, 1, 1) <= c.CreateTime
        //                select c.ct_serial_number;
        //    string max = query.FirstOrDefault().ToString();
        //    var result = new GetNextNumberResult();

        //    long l;
        //    result.Number = max == null ||
        //        !long.TryParse(max, out l) ? 1 : l + 1;
        //    Type typ = typeof(CtTypeEnum);
        //    result.Serial = string.Format(prefix, DateTime.Now.ToString("yy"), result.Number.ToString().PadLeft(3, '0'));

        //    return result;
        //}

        private string GetSampleState(OperateType operate, Sample sample)
        {
            if (operate == OperateType.提交 && sample.sp_state == Convert.ToString(SampleState.草稿))
            {
                return Convert.ToString(SampleState.审核中);
            }
            if (operate == OperateType.变更申请 && sample.sp_state == Convert.ToString(SampleState.草稿))
            {
                return Convert.ToString(SampleState.审核中);
            }
            return sample.sp_state;
        }

        /// <summary>
        /// 根据情况(保存、提交)需要更新样品编码和状态
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="request"></param>
        private void GeneSampleCode(Contract contract)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(contract.contract_sample)))
            {
                List<Sample> samples = _spApp.GetByContractId(contract.Id);

                bool issave = false;
                foreach (var sample in samples)
                {
                    if (String.IsNullOrEmpty(sample.sp_s_code) || (!sample.sp_s_code.Contains("-")))
                    {
                        sample.sp_s_code = string.Format("{0}-{1}", contract.ct_number, sample.xh);
                        issave = true;
                    }
                    if (contract.Operate == OperateType.提交 || contract.Operate == OperateType.变更申请)
                    {
                        sample.sp_state = GetSampleState((OperateType)contract.Operate, sample);
                        issave = true;
                    }
                    if (issave)
                    {
                        _spApp.Update(sample);
                    }
                }
            }
        }

        /// <summary>
        /// 数字转大写
        /// </summary>
        /// <param name="Num">数字</param>/// <returns></returns>
        public string CmycurD(decimal? money)
        {
            decimal num;
            if (money != null)
            {
                num = (decimal)money;
            }
            else
            {
                return null;
            }
            string str1 = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字 
            string str2 = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字 
            string str3 = "";    //从原num值中取出的值 
            string str4 = "";    //数字的字符串形式 
            string str5 = "";  //人民币大写金额形式 
            int i;    //循环变量 
            int j;    //num的值乘以100的字符串长度 
            string ch1 = "";    //数字的汉语读法 
            string ch2 = "";    //数字位的汉字读法 
            int nzero = 0;  //用来计算连续的零值是几个 
            int temp;            //从原num值中取出的值 

            num = Math.Round(Math.Abs(num), 2);    //将num取绝对值并四舍五入取2位小数 
            str4 = ((long)(num * 100)).ToString();        //将num乘100并转换成字符串形式 
            j = str4.Length;      //找出最高位 
            if (j > 15) { return "溢出"; }
            str2 = str2.Substring(15 - j);   //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分 

            //循环取出每一位需要转换的值 
            for (i = 0; i < j; i++)
            {
                str3 = str4.Substring(i, 1);          //取出需转换的某一位的值 
                temp = Convert.ToInt32(str3);      //转换为数字 
                if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15))
                {
                    //当所取位数不为元、万、亿、万亿上的数字时 
                    if (str3 == "0")
                    {
                        ch1 = "";
                        ch2 = "";
                        nzero = nzero + 1;
                    }
                    else
                    {
                        if (str3 != "0" && nzero != 0)
                        {
                            ch1 = "零" + str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                    }
                }
                else
                {
                    //该位是万亿，亿，万，元位等关键位 
                    if (str3 != "0" && nzero != 0)
                    {
                        ch1 = "零" + str1.Substring(temp * 1, 1);
                        ch2 = str2.Substring(i, 1);
                        nzero = 0;
                    }
                    else
                    {
                        if (str3 != "0" && nzero == 0)
                        {
                            ch1 = str1.Substring(temp * 1, 1);
                            ch2 = str2.Substring(i, 1);
                            nzero = 0;
                        }
                        else
                        {
                            if (str3 == "0" && nzero >= 3)
                            {
                                ch1 = "";
                                ch2 = "";
                                nzero = nzero + 1;
                            }
                            else
                            {
                                if (j >= 11)
                                {
                                    ch1 = "";
                                    nzero = nzero + 1;
                                }
                                else
                                {
                                    ch1 = "";
                                    ch2 = str2.Substring(i, 1);
                                    nzero = nzero + 1;
                                }
                            }
                        }
                    }
                }
                if (i == (j - 11) || i == (j - 3))
                {
                    //如果该位是亿位或元位，则必须写上 
                    ch2 = str2.Substring(i, 1);
                }
                str5 = str5 + ch1 + ch2;

                //if (i == j - 1 && str3 == "0")
                //{
                //    //最后一位（分）为0时，加上“整” 
                //    str5 = str5 + '整';
                //}
            }
            if (num == 0)
            {
                str5 = "零元";
            }
            return str5;
        }

        /// <summary>
        /// 更新合同与样品关联的属性(包括系统总价,报告日期,分包价格),并返回合同对象
        /// </summary>
        /// <param name="contractId"></param>
        public Contract UpdateRelatedSampleState(decimal contractId)
        {
            List<Sample> samples = _spApp.GetByContractId(contractId);
            Contract contract = Get(contractId);

            string maxSps43 = "";
            if (samples.Count != 0)
            {
                maxSps43 = samples.OrderByDescending((s) => Convert.ToDateTime(s.sp_s_43)).FirstOrDefault().sp_s_43;
            }

            decimal emergenct_fee = 0;
            decimal sub_package_pay = 0;
            foreach (Sample sample in samples)
            {
                if (sample.totalprice == null)
                {
                    emergenct_fee += 0;
                }
                else
                {
                    emergenct_fee += (decimal)sample.totalprice;
                }

                if (sample.lastprice == null)
                {
                    sub_package_pay += 0;
                }
                else
                {
                    sub_package_pay += (decimal)sample.lastprice;
                }
            }
            contract.ct_report_time = Convert.ToDateTime(maxSps43);
            contract.emergenct_fee = emergenct_fee;
            contract.sub_package_pay = sub_package_pay;
            contract.ct_pay_cose = (contract.sys_totalprice + contract.sample_cose + contract.takesample_cose + contract.express_cose + emergenct_fee)*contract.discount/10;
            Update(contract);
            return contract;
        }

        /// <summary>
        /// 将一个实体的值赋值到另外一个实体
        /// </summary>
        /// <param name="objectsrc">源实体</param>
        /// <param name="objectdest">目标实体</param>
        private void EntityToEntity(object objectsrc, object objectdest)
        {
            var sourceType = objectsrc.GetType();
            var destType = objectdest.GetType();
            foreach (var source in sourceType.GetProperties())
            {
                foreach (var dest in destType.GetProperties())
                {
                    if (dest.Name == source.Name)
                    {
                        Console.WriteLine("---------"+ dest.Name + "-----------");
                        if ("subpackage".Equals(dest.Name)) 
                        {
                            dest.SetValue(objectdest, source.GetValue(objectsrc));
                        }
                        dest.SetValue(objectdest, source.GetValue(objectsrc));
                    }
                }
            }
        }


    }
}