using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class FactorStandaZFApp : BaseStringApp<FactorStandaZF,OpenAuthDBContext>
    {
        private CacheContext _cache;

        public FactorStandaZFApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<FactorStandaZF, OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions())); ;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryFactorStandaZFListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(FactorStandaZF obj)
        {
            AddNoSave(obj);
            UnitWork.Save();
        }

        public void AddNoSave(FactorStandaZF obj)
        {
            UnitWork.Add(obj);
            obj.CreateTime = DateTime.Now;
            obj.CreateUserId = _auth.GetCurrentUser().User.Id;
            _cache.Set("zfbasedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }

        public void Update(FactorStandaZF obj)
        {
            UpdateNoSave(obj);
            UnitWork.Save();
        }

        public void UpdateNoSave(FactorStandaZF obj)
        {
            UnitWork.Update(obj);
            _cache.Set("zfbasedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }
    }
}