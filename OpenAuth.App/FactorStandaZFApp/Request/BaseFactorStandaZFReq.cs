﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseFactorStandaZFReq
    {
        /// <summary>
        ///创建者
        /// </summary>
        public string create_by { get; set; }

        /// <summary>
        ///CMA
        /// </summary>
        public bool? StampCma { get; set; }

        /// <summary>
        ///更新者
        /// </summary>
        public string update_by { get; set; }

        /// <summary>
        ///备注信息
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///删除标记
        /// </summary>
        public string del_flag { get; set; }

        /// <summary>
        ///是否维护
        /// </summary>
        public bool? is_maintain { get; set; }

        /// <summary>
        ///检测价格
        /// </summary>
        public double? price { get; set; }

        /// <summary>
        ///更新时间
        /// </summary>
        public string update_date { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        public string create_date { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        ///CATL
        /// </summary>
        public bool? stampCATL { get; set; }

        /// <summary>
        ///CNAS
        /// </summary>
        public bool? stampCNAS { get; set; }

        /// <summary>
        ///检测项目id
        /// </summary>
        public string factorid { get; set; }

        /// <summary>
        ///政府项目方法关联id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        ///创建用户id
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        ///项目对应的检测标准id
        /// </summary>
        public string standardid { get; set; }

        /// <summary>
        ///项目名称
        /// </summary>
        public string factorname { get; set; }

        /// <summary>
        ///判定依据
        /// </summary>
        public string pdyj { get; set; }

        /// <summary>
        ///方法名称
        /// </summary>
        public string standardname { get; set; }
    }
}
