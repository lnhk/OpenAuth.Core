﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SysLogApp : BaseStringApp<SysLog,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySysLogListReq request)
        {
            var result = new TableData();
            var objs = UnitWork.Find<SysLog>(null);
            if (!string.IsNullOrEmpty(request.key))
            {
                objs = objs.Where(u => u.Content.Contains(request.key) || u.Id.Contains(request.key));
            }

            result.data = await objs.OrderByDescending(u => u.CreateTime)
                .Skip((request.page - 1) * request.limit)
                .Take(request.limit).ToListAsync();
            result.count = await objs.CountAsync();
            return result;
        }

        public List<SysLog> GetByRecordId(decimal recordId)
        {
            var query = from s in UnitWork.Find<SysLog>(null) where s.RecordId== recordId select s;
            return query.ToList();
        }

        public void AddNoSave(SysLog obj)
        {
            obj.Application = Assembly.GetEntryAssembly().FullName.Split(',')[0];
            UnitWork.Add(obj);
        }

        public void Add(SysLog obj)
        {
            AddNoSave(obj);
            UnitWork.Save();
        }
        
        public void UpdateNoSave(SysLog obj)
        {
            UnitWork.Update<SysLog>(u => u.Id == obj.Id, u => obj);
        }

        public void Update(SysLog obj)
        {
            UpdateNoSave(obj);
            UnitWork.Save();
        }

        public SysLogApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SysLog,OpenAuthDBContext> repository) : base(unitWork, repository, null)
        {
        }
    }
}