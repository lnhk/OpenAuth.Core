﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseSampleReportReq
    {
        /// <summary>
        ///是否需要查看（批准审核但不签名）标识
        /// </summary>
        public bool IsNeedView { get; set; }

        /// <summary>
        ///合同id用于公共场所
        /// </summary>
        public int? contractid { get; set; }

        /// <summary>
        ///原始记录文件
        /// </summary>
        public string OrgFile { get; set; }

        /// <summary>
        ///作用同hk_reporttemplate表的isnew字段一样
        /// </summary>
        public string isnew { get; set; }

        /// <summary>
        ///生成次数
        /// </summary>
        public int? xp { get; set; }

        /// <summary>
        ///pdf文档名称
        /// </summary>
        public string portpdf { get; set; }

        /// <summary>
        ///营养标签
        /// </summary>
        public string ReportTag { get; set; }

        /// <summary>
        ///审批信息
        /// </summary>
        public string ApproveInfo { get; set; }

        /// <summary>
        ///模板id
        /// </summary>
        public string template_id { get; set; }

        /// <summary>
        ///修改报告的上一个报告的id
        /// </summary>
        public int? PreId { get; set; }

        /// <summary>
        ///备注信息
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///样品id
        /// </summary>
        public decimal? sampleid { get; set; }

        /// <summary>
        ///版本号
        /// </summary>
        public int? version { get; set; }

        /// <summary>
        ///pdf文档名称
        /// </summary>
        public string report_pdf_name { get; set; }

        /// <summary>
        ///实验室0理化1微生物2现场
        /// </summary>
        public string librarys { get; set; }

        /// <summary>
        ///编辑版本标识
        /// </summary>
        public string editreport { get; set; }

        /// <summary>
        ///报告名称
        /// </summary>
        public string report_name { get; set; }

        /// <summary>
        ///多项目id
        /// </summary>
        public string ProjectIds { get; set; }

        /// <summary>
        ///批准时间
        /// </summary>
        public string PizTime { get; set; }

        /// <summary>
        ///多样品id
        /// </summary>
        public string sampleids { get; set; }

        /// <summary>
        ///生成类
        /// </summary>
        public string reportclass { get; set; }

        /// <summary>
        ///Issend
        /// </summary>
        public bool issend { get; set; }

        /// <summary>
        ///盖章类型
        /// </summary>
        public string seal { get; set; }

        /// <summary>
        ///Genemodel
        /// </summary>
        public int? genemodel { get; set; }

        /// <summary>
        ///批准领域
        /// </summary>
        public string domain { get; set; }

        /// <summary>
        ///报告标识
        /// </summary>
        public string reportonly { get; set; }

        /// <summary>
        ///批准书签
        /// </summary>
        public string auditmark { get; set; }

        /// <summary>
        ///批准用户名称
        /// </summary>
        public string PizUserName { get; set; }

        /// <summary>
        ///批准用户id
        /// </summary>
        public int? PizUserId { get; set; }

        /// <summary>
        ///模板名称
        /// </summary>
        public string template_name { get; set; }

        /// <summary>
        ///报告状态
        /// </summary>
        public string statu { get; set; }

        /// <summary>
        ///报告语言
        /// </summary>
        public string luanguage { get; set; }

        /// <summary>
        ///关联报告申请
        /// </summary>
        public string EditAuditId { get; set; }

        /// <summary>
        ///样品编号
        /// </summary>
        public string sp_s_code { get; set; }

        ///// <summary>
        /////更新时间
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////旧样品编号
        ///// </summary>
        //public decimal oldsampleId { get; set; }

        ///// <summary>
        /////逻辑删除标记（0：显示；1：隐藏）
        ///// </summary>
        //public string del_flag { get; set; }

        ///// <summary>
        /////更新者
        ///// </summary>
        //public string update_by { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////创建时间
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////旧编号
        ///// </summary>
        //public string oldid { get; set; }

        ///// <summary>
        /////创建时间（后端处理）
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建用户编号（后端处理）
        ///// </summary>
        //public string CreateUserId { get; set; }
    }
}
