using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SampleReportApp : BaseLongApp<SampleReport,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleReportListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
            
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }
            
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(SampleReport sampleReport)
        {
            AddNoSave(sampleReport);
            UnitWork.Save();
        }
        
        public void AddNoSave(SampleReport sampleReport)
        {
            User user = _auth.GetCurrentUser().User;
            sampleReport.CreateUserId = user.Id;
            sampleReport.CreateTime = DateTime.Now;
            UnitWork.Add(sampleReport);
        }

        public void Update(SampleReport sampleReport)
        {
            Repository.Update(u => u.Id == sampleReport.Id, u => sampleReport);
        }

        public SampleReportApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SampleReport,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}