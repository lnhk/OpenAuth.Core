using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ObsoleteReasonApp : BaseLongApp<ObsoleteReason,OpenAuthDBContext>
    {
        /// <summary>
        /// 加载作废原因列表
        /// </summary>
        public async Task<TableData> Load(QueryObsoleteReasonListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        /// <summary>
        /// 添加作废原因列表
        /// </summary>
        /// <param name="obsoleteReason"></param>
        public void Add(ObsoleteReason obsoleteReason)
        {
            Repository.Add(obsoleteReason);
        }
        
        /// <summary>
        /// 更新作废原因列表
        /// </summary>
        /// <param name="obj"></param>
        public void Update(ObsoleteReason obsoleteReason)
        {
            UnitWork.Update<ObsoleteReason>(o => o.Id == o.Id, u => obsoleteReason);
            UnitWork.Save();
        }

        public ObsoleteReasonApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ObsoleteReason,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}