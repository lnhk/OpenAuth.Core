﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseObsoleteReasonReq
    {
        /// <summary>
        ///作废原因
        /// </summary>
        public string obsolete_reason { get; set; }

        /// <summary>
        ///样品作废前状态
        /// </summary>
        public string sample_state { get; set; }

        /// <summary>
        ///合同作废前状态
        /// </summary>
        public string contract_state { get; set; }

        /// <summary>
        ///状态_txt
        /// </summary>
        public string state_txt { get; set; }

        /// <summary>
        ///样品编号
        /// </summary>
        public string sample_code { get; set; }

        /// <summary>
        ///样品名称
        /// </summary>
        public string sample_name { get; set; }

        /// <summary>
        ///协议号
        /// </summary>
        public string code { get; set; }

        /// <summary>
        ///合同id
        /// </summary>
        public string contract_id { get; set; }

        /// <summary>
        ///样品状态code_txt
        /// </summary>
        public string sample_state_code_txt { get; set; }

        /// <summary>
        ///样品ids
        /// </summary>
        public string sample_ids { get; set; }

        /// <summary>
        ///驳回原因
        /// </summary>
        public string reject_reason { get; set; }

        /// <summary>
        ///作废类型_txt
        /// </summary>
        public string obsolete_type_txt { get; set; }

        /// <summary>
        ///样品状态code
        /// </summary>
        public string sample_state_code { get; set; }

        /// <summary>
        ///作废类型，样品作废还是合同作废
        /// </summary>
        public string obsolete_type { get; set; }

        /// <summary>
        ///状态
        /// </summary>
        public string state { get; set; }

        /// <summary>
        ///创建用户id
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        ///创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////更新时间
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////创建时间
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////更新者
        ///// </summary>
        //public string update_by { get; set; }
    }
}
