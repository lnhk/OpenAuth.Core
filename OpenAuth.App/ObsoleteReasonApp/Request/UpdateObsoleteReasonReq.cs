﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateObsoleteReasonReq: BaseObsoleteReasonReq
    {
        /// <summary>
        ///作废原因id
        /// </summary>
        public decimal id { get; set; }
    }
}
