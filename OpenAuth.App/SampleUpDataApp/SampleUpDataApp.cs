using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SampleUpDataApp : BaseLongApp<SampleUpData,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleUpDataListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateSampleUpDataReq obj)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = obj.MapTo<SampleUpData>();
            //addObj.Time = DateTime.Now;
            addObj.ack_pass = 0;
            Repository.Add(addObj);
        }

        public void Update(AddOrUpdateSampleUpDataReq obj)
        {
            /*UnitWork.Update<SampleUpData>(u => u.Id == obj.Id, u => new SampleUpData
            {
               //todo:要修改的字段赋值
            });*/

        }

        public void DelByContractId(decimal ContractId)
        {

            Repository.Delete(u => u.ct_id == ContractId);
        }

        /// <summary>
        /// 根据合同id及样品id获取修改的样品信息和检测项目列表
        /// </summary>
        /// <returns></returns>
        public SampleUpDataRes getUpSampleAndDataList(decimal ContractId, decimal SampleId)
        {
            var querySample = from s in UnitWork.Find<SampleUpData>(null)
                              where s.ct_id == ContractId && s.sample_id == SampleId
                              orderby s.create_time descending
                              select s;
            SampleUpData contractUpData = querySample.FirstOrDefault();
            if (contractUpData == null) return null;
            SampleUpDataRes resInfo = new SampleUpDataRes();
            resInfo.sampleUpInfo = null;
            resInfo.sampleDataUpInfo = null;
            // 转换样品信息
            string sampleJsonStr = contractUpData.sample_up_data;
            if (!"".Equals(sampleJsonStr) && sampleJsonStr != null)
            {
                Sample sampleInfo = Json.ToObject<Sample>(sampleJsonStr);
                resInfo.sampleUpInfo = sampleInfo;
            }
            // 转换样品项目信息
            string sampleDataJsonStr = contractUpData.sample_item_up_data;
            if (!"".Equals(sampleDataJsonStr) && sampleDataJsonStr != null)
            {
                List<SampleData> sampleDatas = Json.ToList<SampleData>(sampleDataJsonStr);
            }
            return resInfo;
        }

        public SampleUpDataApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SampleUpData,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}