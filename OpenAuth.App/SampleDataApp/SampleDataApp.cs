using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SampleDataApp : BaseLongApp<SampleData,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleDataListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(SampleData spData)
        {
            AddNoSave(spData);
            UnitWork.Save();
        }
        
        public void AddNoSave(SampleData spData)
        {
            var user = _auth.GetCurrentUser().User;
            spData.CreateUserId = user.Id;
            spData.CreateTime = DateTime.Now;
            UnitWork.Add<SampleData>(spData);
        }

        public List<SampleData> GetBySampleId(decimal sampleId)
        {
            var query = from s in UnitWork.Find<Sample>(null)
                        join sd in UnitWork.Find<SampleData>(null)
                      on s.Id equals sd.sp_id
                        where s.Id == sampleId
                        select sd;
            return query.ToList<SampleData>();
        }

        public void Update(UpdateSampleDataReq req)
        {
            SampleData sampleData = req.MapTo<SampleData>();
            Repository.Update(u => u.Id == req.id, u => sampleData);
        }

        public SampleDataApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SampleData,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}