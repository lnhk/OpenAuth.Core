﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateSampleDataReq:BaseSampleDataReq
    {
        /// <summary>
        ///检测项目id
        /// </summary>
        public decimal id { get; set; }

        /// <summary>
        ///抽样单编号
        /// </summary>
        public decimal? sp_id { get; set; }
    }
}
