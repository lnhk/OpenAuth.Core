﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseSampleDataReq
    {
        /// <summary>
        ///检验项目id
        /// </summary>
        public string spdata_1_id { get; set; }

        /// <summary>
        ///项目状态
        /// </summary>
        public string project_status { get; set; }

        /// <summary>
        ///结果单位
        /// </summary>
        public string spdata_3 { get; set; }

        /// <summary>
        ///结果录入人员
        /// </summary>
        public string testing_personnel { get; set; }

        /// <summary>
        ///制样要求
        /// </summary>
        public string mucount { get; set; }

        /// <summary>
        ///消毒前菌落总数
        /// </summary>
        public string BeforeDisinfectionCFU { get; set; }

        /// <summary>
        ///分包公司id
        /// </summary>
        public int? subpackage_company_id { get; set; }

        /// <summary>
        ///*备注
        /// </summary>
        public string subpackage_company_remark { get; set; }

        /// <summary>
        ///自然菌消亡率
        /// </summary>
        public string NaturalExtinctionRate { get; set; }

        /// <summary>
        ///原始记录id
        /// </summary>
        public int? orgfileid { get; set; }

        /// <summary>
        ///CNAS
        /// </summary>
        public bool? stampCNAS { get; set; }

        /// <summary>
        ///是否分包
        /// </summary>
        public bool? subpackage { get; set; }

        /// <summary>
        ///折扣
        /// </summary>
        public string cose_discount { get; set; }

        /// <summary>
        ///任务单项目状态txt
        /// </summary>
        public string tasklist_projectStatusTxt { get; set; }

        /// <summary>
        ///优惠后的金额
        /// </summary>
        public string realprice { get; set; }

        /// <summary>
        ///旧抽样单编号
        /// </summary>
        public string oldspid { get; set; }

        /// <summary>
        ///样品用量(二次)
        /// </summary>
        public string spdata_15_2 { get; set; }

        /// <summary>
        ///修约值
        /// </summary>
        public string spdata_nvr { get; set; }

        /// <summary>
        ///最大允许限
        /// </summary>
        public string spdata_8 { get; set; }

        /// <summary>
        ///*Catl
        /// </summary>
        public bool? subpackage_company_catl { get; set; }

        /// <summary>
        ///变更属性列表
        /// </summary>
        public string changeProperties { get; set; }

        /// <summary>
        ///样品用量(二次)
        /// </summary>
        public string spdata_15_4 { get; set; }

        /// <summary>
        ///样品用量
        /// </summary>
        public string spdata_15 { get; set; }

        /// <summary>
        ///是否涉及采样
        /// </summary>
        public bool? IsCaiyang { get; set; }

        /// <summary>
        ///价格
        /// </summary>
        public string cose { get; set; }

        /// <summary>
        ///CATL
        /// </summary>
        public bool? stampCATL { get; set; }

        /// <summary>
        ///分包价格
        /// </summary>
        public double? subpackage_company_price { get; set; }

        /// <summary>
        ///原始记录文件名
        /// </summary>
        public string OrgFileName { get; set; }

        /// <summary>
        ///序号
        /// </summary>
        public int? px { get; set; }

        /// <summary>
        ///项目及方法
        /// </summary>
        public string ct_id { get; set; }

        /// <summary>
        ///检测日期
        /// </summary>
        public string spdata_time { get; set; }

        /// <summary>
        ///旧编号
        /// </summary>
        public string oldid { get; set; }

        /// <summary>
        ///任务单项目审批备注
        /// </summary>
        public string tasklist_projectRemark { get; set; }

        /// <summary>
        ///检验项目
        /// </summary>
        public string spdata_1 { get; set; }

        /// <summary>
        ///修约值
        /// </summary>
        public string spdata_2x { get; set; }

        /// <summary>
        ///是否平行
        /// </summary>
        public bool? parallel { get; set; }

        /// <summary>
        ///原始价格
        /// </summary>
        public string price { get; set; }

        /// <summary>
        ///任务单项目状态value
        /// </summary>
        public string tasklist_projectStatus { get; set; }

        /// <summary>
        ///检验结果
        /// </summary>
        public string result { get; set; }

        /// <summary>
        ///生成的原始记录id
        /// </summary>
        public string OrgRecordId { get; set; }

        /// <summary>
        ///*Cnas
        /// </summary>
        public bool? subpackage_company_cnas { get; set; }

        /// <summary>
        ///检测部门
        /// </summary>
        public string spdata_13 { get; set; }

        /// <summary>
        ///检测部门ID
        /// </summary>
        public string spdata_13_id { get; set; }

        /// <summary>
        ///仪器编号
        /// </summary>
        public string spdata_14_number { get; set; }

        /// <summary>
        ///使用仪器
        /// </summary>
        public string spdata_14 { get; set; }

        /// <summary>
        ///最小允许限
        /// </summary>
        public string spdata_7 { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///原始记录文件日期
        /// </summary>
        public string OrgFileDate { get; set; }

        /// <summary>
        ///定量限单位
        /// </summary>
        public string quantitation_unit { get; set; }

        /// <summary>
        ///单位
        /// </summary>
        public string spdata_11 { get; set; }

        /// <summary>
        ///使用范围
        /// </summary>
        public string range_of_application { get; set; }

        /// <summary>
        ///限量
        /// </summary>
        public string spdata_10 { get; set; }

        /// <summary>
        ///*Cma
        /// </summary>
        public bool? subpackage_company_cma { get; set; }

        /// <summary>
        ///定量限
        /// </summary>
        public string quantitation { get; set; }

        /// <summary>
        ///样品用量单位
        /// </summary>
        public string spdata_16 { get; set; }

        /// <summary>
        ///分包公司
        /// </summary>
        public string subpackage_company_name { get; set; }

        /// <summary>
        ///生成编号
        /// </summary>
        public string generated_number { get; set; }

        /// <summary>
        ///折扣
        /// </summary>
        public double? discount { get; set; }

        /// <summary>
        ///允许限单位
        /// </summary>
        public string spdata_9 { get; set; }

        /// <summary>
        ///说明
        /// </summary>
        public string spdata_12 { get; set; }

        /// <summary>
        ///检验依据
        /// </summary>
        public string spdata_5_id { get; set; }

        /// <summary>
        ///判断当前检测项目是否是历史检测项目或者手动录入的
        /// </summary>
        public int? spdata_label { get; set; }

        /// <summary>
        ///检测周期(工作日)
        /// </summary>
        public string cycle { get; set; }

        /// <summary>
        ///样品量
        /// </summary>
        public string sample_count { get; set; }

        /// <summary>
        ///样品用量(二次)
        /// </summary>
        public string spdata_15_3 { get; set; }

        /// <summary>
        ///预警值
        /// </summary>
        public string prewarning_value { get; set; }

        /// <summary>
        ///平行价格
        /// </summary>
        public string parallel_cose { get; set; }

        /// <summary>
        ///消毒前后单位(微生物)
        /// </summary>
        public string DisinfectionUnit { get; set; }

        /// <summary>
        ///消毒后菌落总数(微生物)
        /// </summary>
        public string AfterDisinfectionCFU { get; set; }

        /// <summary>
        ///检验依据
        /// </summary>
        public string spdata_5 { get; set; }

        /// <summary>
        ///判定依据
        /// </summary>
        public string spdata_6 { get; set; }

        /// <summary>
        ///*周期
        /// </summary>
        public string subpackage_company_cycle { get; set; }

        /// <summary>
        ///实测值
        /// </summary>
        public string spdata_2 { get; set; }

        /// <summary>
        ///反馈意见
        /// </summary>
        public string feedback { get; set; }

        /// <summary>
        ///CMA
        /// </summary>
        public bool? stampCMA { get; set; }

        /// <summary>
        ///检验依据简称
        /// </summary>
        public string spdata_4 { get; set; }

        ///// <summary>
        /////删除标志
        ///// </summary>
        //public int? del_flag { get; set; }

        ///// <summary>
        /////旧创建日期
        ///// </summary>
        //public DateTime? oldCreateDate { get; set; }

        ///// <summary>
        /////更新者
        ///// </summary>
        //public string update_by { get; set; }

        ///// <summary>
        /////更新日期
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////创建日期
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////旧创建者
        ///// </summary>
        //public string oldCreateBy { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////创建用户编号（后端处理）
        ///// </summary>
        ////public string CreateUserId { get; set; }

        ///// <summary>
        /////创建时间（后端处理）
        ///// </summary>
        //public DateTime? CreateTime { get; set; }
    }
}
