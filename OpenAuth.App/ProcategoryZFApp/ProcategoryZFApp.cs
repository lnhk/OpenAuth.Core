using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ProcategoryZFApp : BaseLongApp<ProcategoryZF, OpenAuthDBContext>
    {
        private CacheContext _cache;

        public ProcategoryZFApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ProcategoryZF, OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions()));
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryProcategoryZFListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public void AddNoSave(ProcategoryZF obj)
        {
            UnitWork.Add(obj);
            obj.CreateTime = DateTime.Now;
            obj.CreateUserId = _auth.GetCurrentUser().User.Id;
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(),DateTime.Now.AddDays(3));
        }

        public void Add(ProcategoryZF obj)
        {
            AddNoSave(obj);
            UnitWork.Save();
        }

        public void UpdateNoSave(ProcategoryZF obj)
        {
            UnitWork.Update(obj);
            obj.CreateTime = DateTime.Now;
            obj.CreateUserId = _auth.GetCurrentUser().User.Id;
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }

        public void Update(ProcategoryZF obj)
        {
            UpdateNoSave(obj);
            UnitWork.Save();
        }
    }
}