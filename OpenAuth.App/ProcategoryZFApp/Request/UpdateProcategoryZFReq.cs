﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 政府产品项目标准更新请求
    /// </summary>
    public class UpdateProcategoryZFReq: BaseProcategoryZFReq
    {
        /// <summary>
        ///政府产品项目标准id
        /// </summary>
        public decimal id { get; set; }
    }
}
