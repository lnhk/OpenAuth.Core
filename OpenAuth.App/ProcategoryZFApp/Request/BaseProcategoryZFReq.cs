﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseProcategoryZFReq
    {
        /// <summary>
        ///政府产品项目父编码
        /// </summary>
        public string code { get; set; }

        /// <summary>
        ///Enname
        /// </summary>
        public string enname { get; set; }

        /// <summary>
        ///产品名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        ///创建用户id
        /// </summary>
        public string CreateUserId { get; set; }

        /// <summary>
        ///Level
        /// </summary>
        public int? level { get; set; }

        /// <summary>
        ///产品标准代号
        /// </summary>
        public string testingcode { get; set; }

        /// <summary>
        ///产品标准全称
        /// </summary>
        public string testing { get; set; }

        /// <summary>
        ///周期
        /// </summary>
        public string tat { get; set; }

        /// <summary>
        ///政府产品项目标准父id
        /// </summary>
        public decimal? pid { get; set; }

        /// <summary>
        ///政府产品项目编码
        /// </summary>
        public string pcode { get; set; }

        ///// <summary>
        /////创建时间(后端处理)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }
    }
}
