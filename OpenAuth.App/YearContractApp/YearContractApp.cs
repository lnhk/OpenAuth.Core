using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class YearContractApp : BaseLongApp<YearContract,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryYearContractListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(YearContract yearContract)
        {
            AddNoSave(yearContract);
            UnitWork.Save();
        }
        
        public void AddNoSave(YearContract yearContract)
        {
            User user = _auth.GetCurrentUser().User;
            yearContract.CreateTime = DateTime.Now;
            yearContract.CreateUserId = user.Id;
            UnitWork.Add(yearContract);
        }

        public void Update(YearContract yearContract)
        {
            UnitWork.Update<YearContract>(yearContract);
        }

        public YearContractApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<YearContract,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}