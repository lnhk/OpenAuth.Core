﻿using OpenAuth.App.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateYearContractReq: BaseYearContractReq
    {
        /// <summary>
        ///年协议id
        /// </summary>
        public decimal id { get; set; }
    }
}
