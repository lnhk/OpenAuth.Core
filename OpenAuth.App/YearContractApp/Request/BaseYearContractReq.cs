﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseYearContractReq
    {
        /// <summary>
        ///已下单金额
        /// </summary>
        public int? haveorder { get; set; }

        /// <summary>
        ///开票金额占比
        /// </summary>
        public double? openall { get; set; }

        /// <summary>
        ///合同终止日期
        /// </summary>
        public DateTime? contract_endtime { get; set; }

        /// <summary>
        ///统计时间：时间点：最后更新时间，时间段：上个月统计日期
        /// </summary>
        public DateTime? statudate { get; set; }

        /// <summary>
        ///合同名称
        /// </summary>
        public string contract_name { get; set; }

        /// <summary>
        ///未回款金额
        /// </summary>
        public double? nobackcose { get; set; }

        /// <summary>
        ///序号字段
        /// </summary>
        public int? ct_serial_number { get; set; }

        /// <summary>
        ///开票未回款
        /// </summary>
        public double? open_noback { get; set; }

        /// <summary>
        ///合同是否接收
        /// </summary>
        public bool? is_acc_contract { get; set; }

        /// <summary>
        ///合同类型
        /// </summary>
        public int? isselect { get; set; }

        /// <summary>
        ///回款金额占比
        /// </summary>
        public double? backall { get; set; }

        /// <summary>
        ///附件
        /// </summary>
        public string files { get; set; }

        /// <summary>
        ///完成比例%
        /// </summary>
        public double? doneprocess { get; set; }

        /// <summary>
        ///开票已回款
        /// </summary>
        public double? open_back { get; set; }

        /// <summary>
        ///合同类型value(0政府食品，1政府非食品)
        /// </summary>
        public int? ct_type { get; set; }

        /// <summary>
        ///合同号
        /// </summary>
        public string ct_number { get; set; }

        /// <summary>
        ///合同类型txt
        /// </summary>
        public string ct_type_txt { get; set; }

        /// <summary>
        ///合同开始日期
        /// </summary>
        public DateTime? contract_starttime { get; set; }

        /// <summary>
        ///完成方式txt
        /// </summary>
        public string donetypeTxt { get; set; }

        /// <summary>
        ///已完成报告数
        /// </summary>
        public int? doneportcount { get; set; }

        /// <summary>
        ///已开票金额
        /// </summary>
        public double? opencose { get; set; }

        /// <summary>
        ///完成金额
        /// </summary>
        public double? doneprice { get; set; }

        /// <summary>
        ///业务员
        /// </summary>
        public string beforeuser { get; set; }

        /// <summary>
        ///接收人
        /// </summary>
        public string acc_user { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///完成方式value(0时间点，1时间段)
        /// </summary>
        public int? donetype { get; set; }

        /// <summary>
        ///合同金额
        /// </summary>
        public double? contract_cose { get; set; }

        /// <summary>
        ///委托单位名称
        /// </summary>
        public string company_name { get; set; }

        /// <summary>
        ///协同业务员
        /// </summary>
        public string second_beforeuser { get; set; }

        /// <summary>
        ///已回款金额
        /// </summary>
        public double? backcose { get; set; }

        /// <summary>
        ///未开票金额
        /// </summary>
        public double? nopencose { get; set; }

        /// <summary>
        ///总报告数
        /// </summary>
        public int? totalportcount { get; set; }

        /// <summary>
        ///是否完成
        /// </summary>
        public bool? is_end { get; set; }

        /// <summary>
        ///未下单金额
        /// </summary>
        public int? norder { get; set; }

        /// <summary>
        ///合同数量
        /// </summary>
        public int? contract_count { get; set; }

        /// <summary>
        /// 客户id
        /// </summary>
        public decimal? company_id { get; set; }

        /// <summary>
        /// 业务员名称
        /// </summary>
        public string beforeusername { get; set; }
        
        /// <summary>
        /// 接收人id
        /// </summary>
        public int? acc_user_id { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////更新者
        ///// </summary>
        //public string update_by { get; set; }

        ///// <summary>
        /////更新时间
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////创建时间
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////创建用户编号（后端处理）
        ///// </summary>
        //public string CreateUserId { get; set; }

        ///// <summary>
        /////创建时间（后端处理）
        ///// </summary>
        //public DateTime? CreateTime { get; set; }
    }
}
