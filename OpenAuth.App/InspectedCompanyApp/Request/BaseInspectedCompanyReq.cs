﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseInspectedCompanyReq
    {
        /// <summary>
        ///所属人
        /// </summary>
        public string person { get; set; }

        /// <summary>
        ///受检联系人电话
        /// </summary>
        public string personphone { get; set; }

        /// <summary>
        ///生产单位
        /// </summary>
        public string productionName { get; set; }

        /// <summary>
        ///受检联系人
        /// </summary>
        public string companyperson { get; set; }

        /// <summary>
        ///受检单位
        /// </summary>
        public string companyname { get; set; }

        /// <summary>
        ///区域名称
        /// </summary>
        public string addressAreaTxt { get; set; }

        /// <summary>
        ///区域代码
        /// </summary>
        public string addressAreaCode { get; set; }

        /// <summary>
        ///县
        /// </summary>
        public int? xian { get; set; }

        /// <summary>
        ///受检单位地址(详细)
        /// </summary>
        public string companyaddress { get; set; }

        /// <summary>
        ///备注信息
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///受检单位地址(省市县区代码)
        /// </summary>
        public string companyaddress_area { get; set; }

        /// <summary>
        ///所属部门
        /// </summary>
        public string office { get; set; }

        /// <summary>
        ///生产单位地址
        /// </summary>
        public string productionAddress { get; set; }

        /// <summary>
        ///旧主键
        /// </summary>
        public string oldid { get; set; }

        ///// <summary>
        /////逻辑删除标记(迁移用)
        ///// </summary>
        //public string del_flag { get; set; }

        ///// <summary>
        /////更新时间(迁移用)
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////创建时间(迁移用)
        ///// </summary>
        //public DateTime? create_date { get; set; }

        /////// <summary>
        ///////创建者(迁移用)
        /////// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////更新者(迁移用)
        ///// </summary>
        //public string update_by { get; set; }

        /// <summary>
        ///创建时间(后端)
        /// </summary>
        //public DateTime? CreateTime { get; set; }

        /// <summary>
        ///创建人ID(后端)
        /// </summary>
        //public string CreateUserId { get; set; }
    }
}
