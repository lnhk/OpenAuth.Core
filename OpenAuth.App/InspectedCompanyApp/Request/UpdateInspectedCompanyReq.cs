﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateInspectedCompanyReq: BaseInspectedCompanyReq
    {
        /// <summary>
        ///主键
        /// </summary>
        public decimal id { get; set; }
    }
}
