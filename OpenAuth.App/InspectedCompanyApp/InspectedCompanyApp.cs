using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class InspectedCompanyApp : BaseLongApp<InspectedCompany,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryInspectedCompanyListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(InspectedCompany obj)
        {
            AddNoSave(obj);
        }
        
        public void AddNoSave(InspectedCompany obj)
        {
            obj.CreateUserId = _auth.GetCurrentUser().User.Id;
            obj.CreateTime = DateTime.Now;
            Repository.Add(obj);
        }

        public void Update(UpdateInspectedCompanyReq req)
        {
            var obj = req.MapTo<InspectedCompany>();
            Repository.Update(obj);
        }

        public InspectedCompanyApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<InspectedCompany,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}