﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class GetDevUserNumReq
    {
        /// <summary>
        /// 领域value值
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// 客户id
        /// </summary>
        public decimal CompanyId { get; set; }
    }
}
