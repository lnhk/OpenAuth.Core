﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseCustomerUserReq
    {
        /// <summary>
        ///归属类型名称
        /// </summary>
        public string UserTypeName { get; set; }

        /// <summary>
        ///归属领域
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        ///业务员id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        ///业务员姓名
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///归属类型：开发，维护
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        ///领域名称
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// 客户归属人电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 客户归属人电子邮箱
        /// </summary>
        public string Email { get; set; }
        ///// <summary>
        /////创建者(迁移用)
        ///// </summary>
        //public string CreateBy { get; set; }

        ///// <summary>
        /////创建时间(迁移用)
        ///// </summary>
        //public DateTime? CreateDate { get; set; }

        ///// <summary>
        /////更新者(迁移用)
        ///// </summary>
        //public string UpdateBy { get; set; }

        ///// <summary>
        /////更新时间(迁移用)
        ///// </summary>
        //public DateTime? UpdateDate { get; set; }

        ///// <summary>
        /////创建时间(后端操作)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建时间(后端操作)
        ///// </summary>
        //public string CreateUserId { get; set; }
    }
}
