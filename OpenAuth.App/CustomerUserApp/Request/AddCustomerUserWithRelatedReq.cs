﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public  class AddCustomerUserWithRelatedReq : BaseCustomerUserReq
    {
        /// <summary>
        ///客户id
        /// </summary>
        public decimal CompanyId { get; set; }
        /// <summary>
        /// 客户联系人集合
        /// </summary>
        public List<BaseCustomerContractReq> CustomerContracts { get; set; }
    }
}
