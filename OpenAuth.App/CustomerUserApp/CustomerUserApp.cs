using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class CustomerUserApp : BaseLongApp<CustomerUser, OpenAuthDBContext>
    {
        private CustomerContractApp _cstmrContractApp;

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryCustomerUserListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public List<CustomerUser> GetByComapnyId(decimal? companyId)
        {
            List<CustomerUser> customerUsers = UnitWork.Find<CustomerUser>(null).Where((cu) => cu.CompanyId == companyId).ToList();
            return customerUsers;
        }

        public void AddNoSave(CustomerUser cstmrUser)
        {
            User user = _auth.GetCurrentUser().User;
            cstmrUser.CreateUserId = user.Id;
            cstmrUser.CreateTime = DateTime.Now;
            UnitWork.Add(cstmrUser);
        }

        public void Add(CustomerUser cstmrUser)
        {
            AddNoSave(cstmrUser);
            UnitWork.Save();
        }

        public void AddWithRelated(CustomerUser cstmrUser, List<CustomerContract> cstmrContracts)
        {
            AddNoSave(cstmrUser);
            foreach (CustomerContract cstmrContract in cstmrContracts)
            {
                cstmrContract.CompanyId = cstmrUser.CompanyId;
                _cstmrContractApp.AddNoSave(cstmrContract);
            }
            UnitWork.Save();
        }

        public void Update(UpdateCustomerUserReq updateCstmrUserReq)
        {
            CustomerUser cstmrUser = updateCstmrUserReq.MapTo<CustomerUser>();
            UnitWork.Update(cstmrUser);
            UnitWork.Save();
        }

        public CustomerUserApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<CustomerUser, OpenAuthDBContext> repository, IAuth auth,
            CustomerContractApp cstmrContractApp) : base(unitWork, repository, auth)
        {
            _cstmrContractApp = cstmrContractApp;
        }
    }
}