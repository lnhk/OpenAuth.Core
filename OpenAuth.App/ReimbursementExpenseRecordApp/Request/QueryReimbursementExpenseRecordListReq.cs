using System;

namespace OpenAuth.App.Request
{
    public class QueryReimbursementExpenseRecordListReq : PageReq
    {
        /// <summary>
        /// 销售id
        /// </summary>
        public string salesmanId { get; set; }

        /// <summary>
        ///费用类型key
        /// </summary>
        public string costTypeK { get; set; }

        /// <summary>
        ///费用说明
        /// </summary>
        public string costDesc { get; set; }

        /// <summary>
        ///录入时间-开始
        /// </summary>
        public DateTime? beginTime { get; set; }

        /// <summary>
        ///录入时间-结束
        /// </summary>
        public DateTime? endTime { get; set; }

    }
}
