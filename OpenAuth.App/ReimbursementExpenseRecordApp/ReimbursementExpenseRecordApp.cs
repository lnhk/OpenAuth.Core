using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ReimbursementExpenseRecordApp : BaseLongApp<ReimbursementExpenseRecord,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryReimbursementExpenseRecordListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            
            if (!string.IsNullOrEmpty(request.salesmanId)) 
            {
                objs = objs.Where(u => u.salesman_id.Equals(request.salesmanId));
            }
            if (!string.IsNullOrEmpty(request.costTypeK)) 
            {
                objs = objs.Where(u => u.cost_type_k.Equals(request.costTypeK));
            }
            if (!string.IsNullOrEmpty(request.costDesc))
            {
                objs = objs.Where(u => u.cost_desc.Contains(request.costDesc));
            }
            if(request.beginTime != null && request.endTime != null) 
            {
                DateTime endT = (DateTime)request.endTime;
                endT = endT.AddDays(1);
                objs = objs.Where(u => u.cost_time >= request.beginTime && u.cost_time <= endT);
            }

            result.data = objs.OrderByDescending(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddReimbursementExpenseRecordReq obj)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            if (obj.reimbursementExpenseRecordVoList.Count == 0) { throw new CommonException("报销费用数据为空", 500); }

            string userName = loginContext.User.Name;
            string userId = loginContext.User.Id;

            foreach (var item in obj.reimbursementExpenseRecordVoList) 
            {
                ReimbursementExpenseRecord record = new ReimbursementExpenseRecord();
                EntityToEntity(item, record);
                // 赋值销售人员id和录入人员
                record.salesman_id = obj.salesman_id;
                record.salesman_name = obj.salesman_name;
                record.enter_user_id = userId;
                record.enter_user_name = userName;
                UnitWork.Add(record);
            }
            UnitWork.Save();
        }
        
        public void Update(UpdateReimbursementExpenseRecordReq obj)
        {
            // 根据id获取费用报销明细
            var queryCost = from r in UnitWork.Find<ReimbursementExpenseRecord>(null)
                            where r.Id == obj.id
                            select r;
            ReimbursementExpenseRecord record = queryCost.FirstOrDefault();
            if (record == null) { throw new CommonException("未找到报销费用数据", 500); }
            // 赋值修改数据
            EntityToEntity(obj, record);
            UnitWork.Update(record);
            UnitWork.Save();
        }

        /// <summary>
        /// 将一个实体的值赋值到另外一个实体
        /// </summary>
        /// <param name="objectsrc">源实体</param>
        /// <param name="objectdest">目标实体</param>
        private void EntityToEntity(object objectsrc, object objectdest)
        {
            var sourceType = objectsrc.GetType();
            var destType = objectdest.GetType();
            foreach (var source in sourceType.GetProperties())
            {
                foreach (var dest in destType.GetProperties())
                {
                    if (dest.Name == source.Name)
                    {
                        dest.SetValue(objectdest, source.GetValue(objectsrc));
                    }
                }
            }
        }

        public ReimbursementExpenseRecordApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ReimbursementExpenseRecord,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}