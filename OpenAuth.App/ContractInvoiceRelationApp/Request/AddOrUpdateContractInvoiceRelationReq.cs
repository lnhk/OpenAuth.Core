﻿//------------------------------------------------------------------------------
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
//     Author:Yubao Li
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using OpenAuth.Repository.Core;

namespace OpenAuth.App.Request
{
    /// <summary>
       ///
       /// </summary>

    public class AddOrUpdateContractInvoiceRelationReq
    {
        /// <summary>
       ///发票id
       /// </summary>
       public decimal? invoiceid { get; set; }

       /// <summary>
       ///合同id
       /// </summary>
       public decimal? contract_id { get; set; }

       /// <summary>
       ///
       /// </summary>
       public decimal id { get; set; }

       
    }
}