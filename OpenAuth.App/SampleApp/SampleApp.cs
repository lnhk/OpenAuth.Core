using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SampleApp : BaseLongApp<Sample, OpenAuthDBContext>
    {
        private SampleDataApp _sampleDataApp;
        private SampleUpDataApp _sampleUpDataApp;

        public SampleApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Sample, OpenAuthDBContext> repository,
            IAuth auth, SampleDataApp sampleDataApp, SampleUpDataApp sampleUpDataApp) : base(unitWork, repository, auth)
        {
            _sampleDataApp = sampleDataApp;
            _sampleUpDataApp = sampleUpDataApp;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleListReq req)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(req.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            result.data = objs.OrderBy(u => u.Id)
            .Skip((req.page - 1) * req.limit)
            .Take(req.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public void AddNoSave(Sample sample)
        {
            var user = _auth.GetCurrentUser().User;
            sample.CreateUserId = user.Id;
            sample.CreateTime = DateTime.Now;
            UnitWork.Add(sample);
        }

        public void Add(Sample sample)
        {
            AddNoSave(sample);
            UnitWork.Save();
        }

        public void UpdateNoSave(Sample sample)
        {
            var user = _auth.GetCurrentUser().User;
            sample.CreateUserId = user.Id;
            sample.CreateTime = DateTime.Now;
            UnitWork.Update(sample);
        }

        public void Update(Sample sample)
        {
            UpdateNoSave(sample);
            UnitWork.Save();
        }

        public void AddWithRelatedNoSave(Sample sample, List<SampleData> sampleDatas)
        {
            AddNoSave(sample);
            foreach (SampleData sampleData in sampleDatas)
            {
                sampleData.sp_id = sample.Id;
                _sampleDataApp.AddNoSave(sampleData);
            }
        }

        public List<Sample> GetByContractId(decimal contractId)
        {
            var query = from s in UnitWork.Find<Sample>(null) 
                        where s.contract_sample == contractId 
                        select s;
            return query.ToList();
        }

        /// <summary>
        /// 修改样品数据和样品检测项目数据
        /// </summary>
        /// <param name="sampleId"></param>
        /// <returns></returns>
        public void ChangeSampleAndData(Sample sample, List<SampleData> sampleDatas) 
        {
            //获取原始数据对比

            string[] noDbSampleFields = new string[] { "create_date", "del_flag", "oldid", "create_by", "oldCreateDate", "oldCreateBy", "update_date", "update_by", "CreateTime", "CreateUserId" };

            // 获取样品数据
            var querySample = from s in UnitWork.Find<Sample>(null)
                              where s.Id == sample.Id
                              select s;
            Sample old_sample = querySample.FirstOrDefault();
            if (old_sample == null) 
            {
                throw new CommonException("未找到原始样品信息", 500);
            }
            // 标识样品是否修改了
            Boolean samplUpFlag = false;
            // 对比数据 并决定是否存储修改的数据
            System.Reflection.PropertyInfo[] samplePi = typeof(Sample).GetProperties();
            for (int i = 0; i < samplePi.Length; i++)
            {
                System.Reflection.PropertyInfo pi = samplePi[i];
                // 过滤不需要对比的字段
                if (noDbSampleFields.Contains(pi.Name)) { continue; }
                string get_old = pi.GetValue(old_sample, null).ToString();
                string get_new = pi.GetValue(sample, null).ToString();
                if (!get_old.Equals(get_new))
                {
                    samplUpFlag = true;
                    break;
                }
            }
            // 获取样品检测项目数据list
            var query = from sd in UnitWork.Find<SampleData>(null)
                        where sd.sp_id == sample.Id
                        select sd;
            List<SampleData> oldSampleDataList = query.ToList<SampleData>();
            // 样品检测项目修改标识
            Boolean sampleDataUpFlag = false;

            // 情况1 新增，原始无
            if (oldSampleDataList.Count == 0 && sampleDatas.Count != 0) 
            {
                sampleDataUpFlag = true;
            }
            // 情况2 新增，原始有 -- 对比
            if (oldSampleDataList.Count > 0 && sampleDatas.Count > 0)
            {
                if (oldSampleDataList.Count == sampleDatas.Count)
                {
                    // 相等 变更
                    for (int i = 0; i < oldSampleDataList.Count; i++)
                    {
                        SampleData oldData = oldSampleDataList[i];
                        // 是否找到标识
                        Boolean hasFindBeanFlag = false;
                        for (int j = 0; j < sampleDatas.Count; j++)
                        {
                            SampleData newData = sampleDatas[j];
                            if (oldData.Id == newData.Id) 
                            {
                                hasFindBeanFlag = true;
                                // 对比整体数据
                                System.Reflection.PropertyInfo[] sampleDataPi = typeof(SampleData).GetProperties();
                                for (int k = 0; k < sampleDataPi.Length; k++)
                                {
                                    System.Reflection.PropertyInfo pi = sampleDataPi[k];
                                    string get_old = pi.GetValue(oldData, null).ToString();
                                    string get_new = pi.GetValue(newData, null).ToString();
                                    if (!get_old.Equals(get_new))
                                    {
                                        sampleDataUpFlag = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!hasFindBeanFlag) 
                        {
                            sampleDataUpFlag = true;
                            break;
                        }
                    }
                }
                else 
                {
                    // 新增 or // 减少
                    sampleDataUpFlag = true;
                }
            }

            // 情况3 原始有，减少无 - 这种情况默认false即可 不更新

            // 样品修改或者样品下检测项目修改都需存
            AddOrUpdateSampleUpDataReq req = new AddOrUpdateSampleUpDataReq();
            req.ct_id = old_sample.contract_sample;
            req.sample_id = old_sample.Id;
            if (samplUpFlag) 
            {
                req.sample_up_data = sample.ToJson();
            }
            if (sampleDataUpFlag)
            {
                req.sample_item_up_data = sampleDatas.ToJson();
            }
            req.create_time = DateTime.Now;

            if (samplUpFlag || sampleDataUpFlag) 
            {
                // 存在不同 存储变化
                _sampleUpDataApp.Add(req);
            }

        }

        public List<SampleData> GetRelatedSpData(decimal sampleId)
        {
            var query = from s in UnitWork.Find<Sample>(null) join sd in UnitWork.Find<SampleData>(null)
                      on s.Id equals sd.sp_id where s.Id == sampleId select sd;
            return query.ToList<SampleData>();
        }
    }
}