﻿using System;
using System.Collections.Generic;
using OpenAuth.Repository.Domain;
using System.Text;

namespace OpenAuth.App.Request
{
    public class SampleUpDataRes
    {
        public Sample sampleUpInfo { get; set; }
        public List<SampleData> sampleDataUpInfo { get; set; }
    }
}
