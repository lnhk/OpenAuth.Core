﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class ChangeSpDataReq : BaseSampleDataReq
    {
        /// <summary>
        ///检测项目id
        /// </summary>
        public decimal id { get; set; }
    }
}
