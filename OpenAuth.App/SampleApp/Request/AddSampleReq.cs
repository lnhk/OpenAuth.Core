﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class AddSampleReq: BaseSampleReq
    {

        /// <summary>
        /// 关联的检测项目
        /// </summary>
        public List<AddNewSampleDataReq> addNewSpDataReq { get; set; }

        /// <summary>
        /// 与合同关联
        /// </summary>
        public decimal? contract_sample { get; set; }
    }
}
