namespace OpenAuth.App.Request
{
    public class QuerySampleListReq : PageReq
    {
        public string IsSubpackage { get; set; }
        public string CtEntrustCompany { get; set; }
        public string ForSale { get; set; }
        public string ForSaleId { get; set; }
        public string Library { get; set; }
        public string LibraryAcc { get; set; }
        public bool IsWarn { get; set; }
        public string LibraryName { get; set; }
        public string ApproveInfo { get; set; }
        public int ContractId { get; set; }
    }
}
