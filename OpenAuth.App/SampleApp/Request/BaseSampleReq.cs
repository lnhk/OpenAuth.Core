﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseSampleReq
    {
        /// <summary>
        ///样品编号
        /// </summary>
        public string sp_s_51 { get; set; }

        /// <summary>
        ///受检单位id
        /// </summary>
        public string inspectedcompanyid { get; set; }

        /// <summary>
        ///采样人员
        /// </summary>
        public string sp_cs_w_8 { get; set; }

        /// <summary>
        ///采样地址
        /// </summary>
        public string sp_cd_2 { get; set; }

        /// <summary>
        ///数量单位
        /// </summary>
        public int? wrapcount { get; set; }

        /// <summary>
        ///大类
        /// </summary>
        public string sp_s_2_id { get; set; }

        /// <summary>
        ///快递单号
        /// </summary>
        public string couriernumber { get; set; }

        /// <summary>
        ///是否已反馈txt
        /// </summary>
        public string is_feedback_txt { get; set; }

        /// <summary>
        ///样品报告日期
        /// </summary>
        public string? sp_s_43 { get; set; }

        /// <summary>
        ///二维码
        /// </summary>
        public string sp_s_qr { get; set; }

        /// <summary>
        ///样品照片
        /// </summary>
        public string sp_s_pic { get; set; }

        /// <summary>
        ///是否加盖CMA
        /// </summary>
        public bool? StampCma { get; set; }

        /// <summary>
        ///是否需要重新流转 1-需要，2-已重新流转过
        /// </summary>
        public int? IsNeedFlowAgain { get; set; }

        /// <summary>
        ///样品数量
        /// </summary>
        public string sp_s_28 { get; set; }

        /// <summary>
        ///是否采样
        /// </summary>
        public bool? sp_s_44 { get; set; }

        /// <summary>
        ///是否全分包
        /// </summary>
        public string sp_cc_1 { get; set; }

        /// <summary>
        ///复制包/样品编号
        /// </summary>
        public string copySample { get; set; }

        /// <summary>
        ///任务单样品状态
        /// </summary>
        public int? tasklist_sampleStatus { get; set; }

        /// <summary>
        ///是否加盖CNAS
        /// </summary>
        public bool? StampCnas { get; set; }

        /// <summary>
        ///抽样人员
        /// </summary>
        public string sp_cs_w_6 { get; set; }

        /// <summary>
        ///保质期至
        /// </summary>
        public DateTime? Expire { get; set; }

        /// <summary>
        ///分包价格
        /// </summary>
        public decimal? lastprice { get; set; }

        /// <summary>
        ///收检日期
        /// </summary>
        public string sp_s_36 { get; set; }

        /// <summary>
        ///需要资质
        /// </summary>
        public string qualifications { get; set; }

        /// <summary>
        ///原产地
        /// </summary>
        public string sp_s_21 { get; set; }

        /// <summary>
        ///受检联系人
        /// </summary>
        public string sp_cs_w_3 { get; set; }

        /// <summary>
        ///流转批次号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        ///规格型号
        /// </summary>
        public string sp_s_18 { get; set; }

        /// <summary>
        ///临时串码
        /// </summary>
        public int? sp_s_26 { get; set; }

        /// <summary>
        ///包装类型txt
        /// </summary>
        public string wraptypetxt { get; set; }

        /// <summary>
        ///是否已经统计完成
        /// </summary>
        public bool? is_statistics { get; set; }

        /// <summary>
        ///结论
        /// </summary>
        public string sp_s_r { get; set; }

        /// <summary>
        ///联系人
        /// </summary>
        public string sp_cc_10 { get; set; }

        /// <summary>
        ///到货时间
        /// </summary>
        public string sp_s_45 { get; set; }

        /// <summary>
        ///变更属性列表
        /// </summary>
        public string changeProperties { get; set; }

        /// <summary>
        ///是否加盖CATL
        /// </summary>
        public bool? StampCatl { get; set; }

        /// <summary>
        ///原始记录文件
        /// </summary>
        public string OrgFile { get; set; }

        /// <summary>
        ///报告书编号
        /// </summary>
        public string sp_s_37 { get; set; }

        /// <summary>
        ///样品包装
        /// </summary>
        public string sp_s_25 { get; set; }

        /// <summary>
        ///流程实例id
        /// </summary>
        public string proc_ins_id { get; set; }

        /// <summary>
        ///序号
        /// </summary>
        public int? xh { get; set; }

        /// <summary>
        ///单位
        /// </summary>
        public string wrapunit { get; set; }

        /// <summary>
        ///附件
        /// </summary>
        public string sp_s_35 { get; set; }

        /// <summary>
        ///是否进口
        /// </summary>
        public string sp_s_20 { get; set; }

        /// <summary>
        ///预警报告日期
        /// </summary>
        public DateTime? warn_report_time { get; set; }

        /// <summary>
        ///抽样单号
        /// </summary>
        public string sp_scc_code { get; set; }

        /// <summary>
        ///样品级别
        /// </summary>
        public string sp_cc_2 { get; set; }

        /// <summary>
        ///包装类型value
        /// </summary>
        public string wraptype { get; set; }

        /// <summary>
        ///系统流转日期
        /// </summary>
        public string sp_s_41 { get; set; }

        /// <summary>
        ///受检单位
        /// </summary>
        public string sp_cs_w_1 { get; set; }

        /// <summary>
        ///企业名称
        /// </summary>
        public string sp_cc_12 { get; set; }

        /// <summary>
        ///报告语言
        /// </summary>
        public int? report_language { get; set; }

        /// <summary>
        ///抽样数量
        /// </summary>
        public string sampcount { get; set; }

        /// <summary>
        ///折扣
        /// </summary>
        public decimal? discount { get; set; }

        /// <summary>
        ///最大周期
        /// </summary>
        public string maxCycle { get; set; }

        /// <summary>
        ///样品类型txt
        /// </summary>
        public string sp_s_3_id_txt { get; set; }

        /// <summary>
        ///批量添加序号
        /// </summary>
        public string sp_batch_add_no { get; set; }

        /// <summary>
        ///付费方式
        /// </summary>
        public string courierpay { get; set; }

        /// <summary>
        ///联系电话
        /// </summary>
        public string sp_cs_w_4 { get; set; }

        /// <summary>
        ///报告份数
        /// </summary>
        public int? sp_cc_18 { get; set; }

        /// <summary>
        ///总价
        /// </summary>
        public decimal? totalprice { get; set; }

        /// <summary>
        ///抽样方式
        /// </summary>
        public string sp_s_23 { get; set; }

        /// <summary>
        ///保质期
        /// </summary>
        public string sp_s_16 { get; set; }

        /// <summary>
        ///是否判定结果
        /// </summary>
        public bool? sp_s_50 { get; set; }

        /// <summary>
        ///企业名称
        /// </summary>
        public string sp_cs_5 { get; set; }

        /// <summary>
        ///原始记录日期
        /// </summary>
        public string OrgFileDate { get; set; }

        /// <summary>
        ///全部实验室提交的时间
        /// </summary>
        public DateTime? all_commit_time { get; set; }

        /// <summary>
        ///抽样地点
        /// </summary>
        public string sp_cs_w_5 { get; set; }

        /// <summary>
        ///样品类型value
        /// </summary>
        public string sp_s_3_id { get; set; }

        /// <summary>
        ///流转单id
        /// </summary>
        public string tranferlist_id { get; set; }

        /// <summary>
        ///电话
        /// </summary>
        public string sp_cc_11 { get; set; }

        /// <summary>
        ///报告批准完成日期
        /// </summary>
        public DateTime? reportDoneDate { get; set; }

        /// <summary>
        ///备样数量
        /// </summary>
        public string sp_s_29 { get; set; }

        /// <summary>
        ///报告形式
        /// </summary>
        public int? sp_cc_17 { get; set; }

        /// <summary>
        ///报告时间要求
        /// </summary>
        public string sp_s_46 { get; set; }

        /// <summary>
        ///样品存储条件
        /// </summary>
        public string sp_s_31 { get; set; }

        /// <summary>
        ///样品存储条件value
        /// </summary>
        public int? sp_s_31_value { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///所属业务
        /// </summary>
        public int? sp_s_42 { get; set; }

        /// <summary>
        ///快递公司
        /// </summary>
        public string couriercompany { get; set; }

        /// <summary>
        ///检测类别
        /// </summary>
        public string TestCategory { get; set; }

        /// <summary>
        ///业务员
        /// </summary>
        public string SpS42Name { get; set; }

        /// <summary>
        ///分包报告
        /// </summary>
        public string SubpackageReport { get; set; }

        /// <summary>
        ///样品编号
        /// </summary>
        public string sp_s_code { get; set; }

        /// <summary>
        ///抽样日期
        /// </summary>
        public string sp_s_22 { get; set; }

        /// <summary>
        ///样品商标
        /// </summary>
        public string sp_s_9 { get; set; }

        /// <summary>
        ///企业地址
        /// </summary>
        public string sp_cs_4 { get; set; }

        /// <summary>
        ///样品状态
        /// </summary>
        public string sp_state { get; set; }

        /// <summary>
        ///样品状态txt
        /// </summary>
        public string sp_state_txt { get; set; }

        /// <summary>
        ///是否已反馈value
        /// </summary>
        public string is_feedback { get; set; }

        /// <summary>
        ///样品形态
        /// </summary>
        public string sp_s_24 { get; set; }

        /// <summary>
        ///生产日期/批号
        /// </summary>
        public string sp_s_17 { get; set; }

        /// <summary>
        ///执行标准/技术文件
        /// </summary>
        public string sp_s_32 { get; set; }

        /// <summary>
        ///抽样单位名称
        /// </summary>
        public string sp_cc_7 { get; set; }

        /// <summary>
        ///受检单位地址
        /// </summary>
        public string sp_cs_w_2 { get; set; }

        /// <summary>
        ///合同变更备注
        /// </summary>
        public string ContractRemark { get; set; }

        /// <summary>
        ///样品下达日期
        /// </summary>
        public string release_time_hksample { get; set; }

        /// <summary>
        ///样品名称
        /// </summary>
        public string sp_s_7 { get; set; }

        /// <summary>
        ///质量等级
        /// </summary>
        public string sp_s_19 { get; set; }

        /// <summary>
        ///ProgrammeId
        /// </summary>
        public string programmeId { get; set; }

        /// <summary>
        ///快递费用
        /// </summary>
        public decimal? courierprice { get; set; }

        /// <summary>
        ///采样地点
        /// </summary>
        public string sp_cs_w_7 { get; set; }

        ///// <summary>
        /////合同创建日期
        ///// </summary>
        //public DateTime? create_date { get; set; }

        ///// <summary>
        /////删除标志
        ///// </summary>
        //public int? del_flag { get; set; }

        ///// <summary>
        /////旧编号
        ///// </summary>
        //public string oldid { get; set; }

        ///// <summary>
        /////创建者
        ///// </summary>
        //public string create_by { get; set; }

        ///// <summary>
        /////旧创建日期
        ///// </summary>
        //public DateTime? oldCreateDate { get; set; }

        ///// <summary>
        /////旧创建者
        ///// </summary>
        //public string oldCreateBy { get; set; }

        ///// <summary>
        /////更新日期
        ///// </summary>
        //public DateTime? update_date { get; set; }

        ///// <summary>
        /////更新者
        ///// </summary>
        //public string update_by { get; set; }

        ///// <summary>
        /////创建时间(后端赋值)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建用户编号(后端赋值)
        ///// </summary>
        //public string CreateUserId { get; set; }
    }
}
