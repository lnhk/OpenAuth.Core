﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class ChangeSampleAndSpDataReq : BaseSampleReq
    {
        /// <summary>
        ///样品id
        /// </summary>
        public decimal id { get; set; }

        /// <summary>
        /// 关联的检测项目
        /// </summary>
        public List<ChangeSpDataReq> addNewSpDataReq { get; set; }
    }
}
