using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class FactorApp : BaseStringApp<Factor, OpenAuthDBContext>
    {
        private CacheContext _cache;
        public FactorApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Factor, OpenAuthDBContext> repository,IAuth auth) : base(unitWork, repository, auth)
        {
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions())); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FactorResp FindSingle(string id)
        {
            var form = Get(id);
            return form.MapTo<FactorResp>();
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryFactorListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }


            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            if (!string.IsNullOrEmpty(request.FactorName))
            {
                //增加筛选条件,如：
                objs = objs.Where(u => u.factorname.Contains(request.FactorName));
            }

            if (request.DataSource.HasValue)
            {
                objs = objs.Where(u => u.DataSource == request.DataSource.Value);
            }

            result.data = objs.OrderByDescending(u => u.create_date)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateFactorReq req)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = req.MapTo<Factor>();
            //addObj.Time = DateTime.Now;
            addObj.CreateUserId = _auth.GetCurrentUser().User.Id;
            Repository.Add(addObj);
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }

        public void Update(AddOrUpdateFactorReq req)
        {
            var aa = new Factor();
            var factor = req.MapTo(aa);
            Repository.Update(factor);
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }

        public List<Factor> GetFactorForCache()
        {
            List<Factor> factors = new List<Factor>();
            var query = from fs in UnitWork.Find<Factor>(null).Take(10000) select fs;
            return query.ToList();
        }

        public List<FactorStandard> GetFactorStdForCache(){
            List<FactorStandard> factorStandards = new List<FactorStandard>();
            var query = from fs in UnitWork.Find<FactorStandard>(null).Take(10000) select fs;
            return query.ToList();
        }

        public List<Standard> GetStdForCache()
        {
            List<Standard> factorStandards = new List<Standard>();
            var query = from fs in UnitWork.Find<Standard>(null).Take(10000) select fs;
            return query.ToList();
        }

        public List<BaseQualition> GetBaseQualitionForCache()
        {
            List<BaseQualition> baseQualitions = new List<BaseQualition>();
            var query = from fs in UnitWork.Find<BaseQualition>(null).Take(10000) select fs;
            return query.ToList();
        }

        public List<BaseQualition> GetBaseQualitionForSampleData(string ct_id)
        {
            var baseQualitionsQuery = from bq in UnitWork.Find<BaseQualition>(null) where bq.factor_standa_id == ct_id select bq;
            return baseQualitionsQuery.ToList();
        }

        public async Task<List<FactorStandardSelectResp>> GetFactorStandardSelect()
        {
            var data = new List<FactorStandardSelectResp>();
            var query = from fs in UnitWork.Find<FactorStandard>(null)
                        join f in UnitWork.Find<Factor>(null) on fs.factorid equals f.Id
                        join s in UnitWork.Find<Standard>(null) on fs.standardid equals s.Id
                        select new FactorStandardSelectResp()
                        {
                            Id = fs.Id,
                            Name = $"{f.factorname}【{s.standardname}】"
                        };
            return data = await query.ToListAsync();
        }


        public async Task<FactorStandardDetailResp> GetFactorStandardDetail(string id)
        {
            var data = new FactorStandardDetailResp();
            var query = from fs in UnitWork.Find<FactorStandard>(null)
                        join f in UnitWork.Find<Factor>(null) on fs.factorid equals f.Id into temp1
                        from t1 in temp1.DefaultIfEmpty()
                        join s in UnitWork.Find<Standard>(null) on fs.standardid equals s.Id into temp2
                        from t2 in temp2.DefaultIfEmpty()
                        join l in UnitWork.Find<Library>(null) on t2.department equals l.Id into temp3
                        from t3 in temp3.DefaultIfEmpty()
                        where fs.Id == id
                        select new FactorStandardDetailResp()
                        {
                            Id = fs.Id,
                            FactorId = t1.Id,
                            FactorName = t1.factorname,
                            StandardId = t2.Id,
                            StandardName = t2.standardname,
                            StandardShortName = t2.testfunc,
                            CMA = fs.StampCma.HasValue ? Convert.ToBoolean(fs.StampCma.Value) : false,
                            CMARemark = fs.cmaremarks,
                            CNAS = fs.stampCNAS.HasValue ? Convert.ToBoolean(fs.stampCNAS.Value) : false,
                            CNASRemark = fs.cnasremarks,
                            CATL = fs.stampCATL.HasValue ? Convert.ToBoolean(fs.stampCATL.Value) : false,
                            CATLRemark = fs.cnasremarks,
                            Remark = fs.remarks,
                            IsStop = fs.IsStop.HasValue ? Convert.ToBoolean(fs.IsStop.Value) : false,
                            IsSampling = fs.IsStop.HasValue ? Convert.ToBoolean(fs.IsCaiyang.Value) : false,
                            DepartmentId = t2.department,
                            DepartmentName = t3.departname,
                            RangeOfApplication = t2.range_of_application,
                            Quantitation = string.IsNullOrEmpty(fs.quantitation) ? fs.detection : fs.quantitation,
                            SampleRequired = fs.mucount,
                            IsSubpackage = t2.subpackage.HasValue ? Convert.ToBoolean(t2.subpackage.Value) : false,
                            Cose = t2.cose,
                            ParallelCose = t2.parallel_cose,
                            ErrorReason = fs.IsStop.Value ? "项目已停用" : "",
                            Scose = t2.cose.ToString(),
                            Coses = t2.coses.ToString(),
                            Coseremark = t2.coseremark
                        };
            data = await query.FirstOrDefaultAsync();
            if (data != null)
            {
                var querySubpackge = await UnitWork.Find<Subpackage>(w => w.factor_standa_id == data.Id).ToListAsync();
                if (querySubpackge.Any())
                {
                    data.SubpackageCompanyName = querySubpackge.FirstOrDefault().name;
                    data.SubpackageCose = querySubpackge.FirstOrDefault().price.ToString();
                    data.SubpackageCycle = querySubpackge.FirstOrDefault().cycle;
                    data.SubpackageCMA = Convert.ToBoolean(querySubpackge.FirstOrDefault().StampCma);
                    data.SubpackageCNAS = Convert.ToBoolean(querySubpackge.FirstOrDefault().stampCNAS);
                    data.SubpackageCATL = Convert.ToBoolean(querySubpackge.FirstOrDefault().stampCATL);
                    data.SubpackageRemark = querySubpackge.FirstOrDefault().remark;
                    data.SubpackageCompanyList = querySubpackge.Select(s => new FactorStandardDetailSubpackageCompany()
                    {
                        Id = s.Id,
                        SubpackageCompanyName = s.name,
                        SubpackageCose = s.price.ToString(),
                        SubpackageCycle = s.cycle,
                        SubpackageCMA = Convert.ToBoolean(s.StampCma),
                        SubpackageCNAS = Convert.ToBoolean(s.stampCNAS),
                        SubpackageCATL = Convert.ToBoolean(s.stampCATL),
                        SubpackageRemark = s.remark
                    }).ToList();
                }
                data.QualitionList = await (
                    from q in UnitWork.Find<BaseQualition>(null)
                    join u in UnitWork.Find<Unit>(null) on q.unit.Value equals u.Id into temp1
                    from t1 in temp1.DefaultIfEmpty()
                    where q.factor_standa_id == data.Id
                    select new FactorStandardDetailQualition()
                    {
                        Id = q.Id,
                        Sort = q.xh.Value,
                        Type = q.ntype.Value,
                        SampleType = q.sampletype,
                        TypeValue = q.qualition,
                        Unit = t1.unit,
                        Remark = q.remarks
                    }).ToListAsync();
            }
            return data;
        }


        public async Task<List<ProcategorySelectResp>> GetProcategorySelect()
        {
            var data = new List<ProcategorySelectResp>();
            var query = UnitWork.Find<Procategory>(null).Select(s => new ProcategorySelectResp()
            {
                Id = s.Id,
                Name = $"{s.name}【{s.testing}】"
            });
            return data = await query.ToListAsync();
        }


        public async Task<List<ProcategoryItemdataListResp>> GetProcategoryItemdataList(int id)
        {
            var data = new List<ProcategoryItemdataListResp>();
            var query = from p in UnitWork.Find<Procategory>(null)
                        join i in UnitWork.Find<Itemdata>(null) on p.Id.ToString() equals i.categoryid
                        join fs in UnitWork.Find<FactorStandard>(null) on i.facstandid equals fs.Id
                        join f in UnitWork.Find<Factor>(null) on i.itemid equals f.Id
                        join s in UnitWork.Find<Standard>(null) on i.methodid equals s.Id
                        join l in UnitWork.Find<Library>(null) on s.department equals l.Id
                        where p.Id == id
                        select new ProcategoryItemdataListResp()
                        {
                            Id = p.Id,
                            FactorId = f.Id,
                            FactorName = f.factorname,
                            StandardId = s.Id,
                            StandardName = s.standardname,
                            StandardShortName = s.testfunc,
                            FactorStandardId = fs.Id,
                            CMA = Convert.ToBoolean(fs.StampCma.Value),
                            CNAS = Convert.ToBoolean(fs.stampCNAS.Value),
                            CATL = Convert.ToBoolean(fs.stampCATL.Value),
                            Remark = fs.remarks,
                            DepartmentId = s.department,
                            DepartmentName = l.departname,
                            RangeOfApplication = s.range_of_application,
                            Quantitation = string.IsNullOrEmpty(fs.quantitation) ? fs.detection : fs.quantitation,
                            SampleRequired = fs.mucount,
                            IsSubpackage = Convert.ToBoolean(s.subpackage.Value),
                            Limit = i.zb,
                            UnitId = i.UnitId.HasValue ? i.UnitId.Value : 0,
                            Unit = i.zbunit,
                            SampleCout = s.sample_count,
                            Cose = (!i.parallel.HasValue || i.parallel.Value == 0) ? s.cose : s.parallel_cose,
                            IsSampling = Convert.ToBoolean(fs.IsCaiyang.Value),
                            IsParallel = i.parallel.HasValue ? Convert.ToBoolean(i.parallel.Value) : false,
                            ParallelCose = i.parallel.HasValue ? i.parallel.Value.ToString() : "",
                            Scose = s.cose.ToString(),
                            Coses = s.coses.ToString(),
                            Coseremark = s.coseremark,
                            Cycle = i.Tat,
                        };
            data = await query.ToListAsync();
            if (data.Any())
            {
                foreach (var item in data)
                {
                    var querySubpackge = UnitWork.Find<Subpackage>(w => w.factor_standa_id == item.FactorStandardId).ToList();
                    if (querySubpackge.Any())
                    {
                        item.SubpackageCompanyId = querySubpackge.FirstOrDefault().Id;
                        item.SubpackageCompanyName = querySubpackge.FirstOrDefault().name;
                        item.SubpackageCose = querySubpackge.FirstOrDefault().price.ToString();
                        item.SubpackageCycle = querySubpackge.FirstOrDefault().cycle;
                        item.SubpackageCMA = Convert.ToBoolean(querySubpackge.FirstOrDefault().StampCma);
                        item.SubpackageCNAS = Convert.ToBoolean(querySubpackge.FirstOrDefault().stampCNAS);
                        item.SubpackageCATL = Convert.ToBoolean(querySubpackge.FirstOrDefault().stampCATL);
                        item.SubpackageRemark = querySubpackge.FirstOrDefault().remark;
                    }
                }
            }
            return data;
        }

        public async Task<List<CategorytreeListResp>> GetCategorytreeList(string name)
        {
            var data = new List<CategorytreeListResp>();
            var query = UnitWork.Find<Categorytree>(null);
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(w => w.name.Contains(name));
            }
            return data = await query.Select(s => new CategorytreeListResp()
            {
                Id = s.Id,
                PId = s.pid.Value,
                Level = s.level.Value,
                Name = s.name,
                Fx = s.Fx,
                Remarks = s.remarks,
                DataSource = s.DataSource.ToString()
            }).ToListAsync();
        }

        public async Task<List<ProcategoryListResp>> GetProcategoryList(int categorytreeId)
        {
            var data = new List<ProcategoryListResp>();
            return data = await UnitWork.Find<Procategory>(w => w.pid.Value == categorytreeId).Select(s => new ProcategoryListResp()
            {
                Id = s.Id,
                Name = s.name,
                StandardName = s.testing,
                StandardCode = s.testingcode,
                Cycle = s.tat,
                PackageCose = s.package_price.ToString(),
                SampleCount = s.SampleCount
            }).ToListAsync();
        }


        public async Task<(int totalCount, List<FactorStandardListResp> data)> GetFactorStandardList(string factorName, string standardName, int? dataSource, int? dataStatus, bool? isSubpakege, string lab, string factorOtherName, int page, int limit)
        {
            var data = new List<FactorStandardListResp>();
            int totalCount = 0;
            var query = from fs in UnitWork.Find<FactorStandard>(null)
                        join f in UnitWork.Find<Factor>(null) on fs.factorid equals f.Id into temp1
                        from t1 in temp1.DefaultIfEmpty()
                        join s in UnitWork.Find<Standard>(null) on fs.standardid equals s.Id into temp2
                        from t2 in temp2.DefaultIfEmpty()
                        join l in UnitWork.Find<Library>(null) on t2.department equals l.Id into temp3
                        from t3 in temp3.DefaultIfEmpty()
                        join u in UnitWork.Find<Users>(null) on fs.create_by equals u.UserId.ToString() into temp4
                        from t4 in temp4.DefaultIfEmpty()
                        orderby fs.create_date descending
                        select new FactorStandardListResp()
                        {
                            Id = fs.Id,
                            FactorId = t1.Id,
                            FactorName = t1.factorname,
                            DataSource = fs.DataSource.ToString(),
                            StandardId = t2.Id,
                            StandardName = t2.standardname,
                            FactorOtherName = t1.displayname,
                            CMA = fs.StampCma.HasValue ? Convert.ToBoolean(fs.StampCma.Value) : false,
                            CNAS = fs.stampCNAS.HasValue ? Convert.ToBoolean(fs.stampCNAS.Value) : false,
                            CATL = fs.stampCATL.HasValue ? Convert.ToBoolean(fs.stampCATL.Value) : false,
                            Remark = fs.remarks,
                            LabId = t2.department,
                            LabName = t3.departname,
                            RangeOfApplication = t2.range_of_application,
                            Quantitation = string.IsNullOrEmpty(fs.quantitation) ? fs.detection : fs.quantitation,
                            SampleRequired = fs.mucount,
                            IsSubpackage = t2.subpackage.HasValue ? Convert.ToBoolean(t2.subpackage.Value) : false,
                            Cose = t2.cose,
                            ParallelCose = t2.parallel_cose,
                            Scose = t2.cose,
                            Coses = (t2 != null && t2.coses.HasValue) ? t2.coses.Value.ToString() : "",
                            Coseremark = t2.coseremark,
                            Cycle = t2.cycle,
                            DataStatus = fs.status.HasValue ? fs.status.Value : 0,
                            StopReason = fs.stopremark,
                            IsStop = fs.IsStop.HasValue ? fs.IsStop.Value : false,
                            IsSampling = fs.IsCaiyang.HasValue ? Convert.ToBoolean(fs.IsCaiyang) : false,
                            SampleCount = t2.sample_count,
                            Detection = fs.detection,
                            CreateUser = t4.DisplayName,
                            CreateTime = fs.create_date.HasValue ? fs.create_date.Value.ToString("yyyy-MM-dd HH:mm:ss") : "",
                            UpdateTime = fs.update_date.HasValue ? fs.update_date.Value.ToString("yyyy-MM-dd HH:mm:ss") : ""
                        };
            if (!string.IsNullOrEmpty(factorName))
            {
                query = query.Where(w => w.FactorName.Contains(factorName));
            }
            if (!string.IsNullOrEmpty(standardName))
            {
                query = query.Where(w => w.StandardName.Contains(standardName));
            }
            if (dataSource.HasValue)
            {
                query = query.Where(w => w.DataSource == dataSource.Value.ToString());
            }
            if (dataStatus.HasValue)
            {
                query = query.Where(w => w.DataStatus == dataStatus.Value);
            }
            if (isSubpakege.HasValue)
            {
                query = query.Where(w => w.IsSubpackage == isSubpakege.Value);
            }
            if (!string.IsNullOrEmpty(lab))
            {
                query = query.Where(w => w.LabName.Contains(lab));
            }
            if (!string.IsNullOrEmpty(factorOtherName))
            {
                query = query.Where(w => w.FactorName.Contains(factorOtherName));
            }
            totalCount = await query.CountAsync();
            if (totalCount > 0)
            {
                data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            }
            return (totalCount, data);
        }


        public async Task<List<FactorStandardSelectResp>> GetFactorSelect()
        {
            var data = new List<FactorStandardSelectResp>();
            data = await UnitWork.Find<Factor>(null).Select(s => new FactorStandardSelectResp()
            {
                Id = s.Id,
                Name = s.factorname,
                OtherName = !string.IsNullOrEmpty(s.displayname) ? s.displayname : ""
            }).ToListAsync();
            return data;
        }

        public async Task<List<FactorStandardSelectResp>> GetStandardSelect()
        {
            var data = new List<FactorStandardSelectResp>();
            data = await UnitWork.Find<Standard>(null).Select(s => new FactorStandardSelectResp()
            {
                Id = s.Id,
                Name = s.standardname
            }).ToListAsync();
            return data;
        }


        public async Task<bool> UpdateOrAddFactorStandard(AddOrUpdateFactorStandardReq req)
        {
            int res = 0;
            var fs = new FactorStandard()
            {
                factorid = req.FactorId,
                standardid = req.StandardId,
                create_by = _auth.GetCurrentUser().User.Id,
                StampCma = req.CMA,
                cmaremarks = req.CMARemark,
                stampCNAS = req.CNAS,
                cnasremarks = req.CNASRemark,
                stampCATL = req.CATL,
                catlremarks = req.CATLRemark,
                fdepartment = req.SubpackageLabType,
                stopremark = req.StopReason,
                remarks = req.Remark,
                quantitation = req.Quantitation,
                detection = req.Detection,
                mucount = req.SampleRequired,
                DataSource = req.DataSource,
                IsCaiyang = req.IsSampling,
                IsStop = req.IsStop,
                isnew = true,
            };
            if (string.IsNullOrEmpty(req.Id))
            {
                fs.Id = Guid.NewGuid().ToString();
                UnitWork.Add(fs);
                if (req.SubpackageCompanyList?.Any() ?? false)
                {
                    var subpackageCompanyList = new List<Subpackage>();
                    req.SubpackageCompanyList.ForEach(f =>
                    {
                        subpackageCompanyList.Add(new Subpackage()
                        {
                            name = f.Name,
                            price = Convert.ToDouble(f.Price),
                            cycle = f.Cycle,
                            StampCma = f.CMA,
                            stampCNAS = f.CNAS,
                            stampCATL = f.CATL,
                            remark = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    UnitWork.BatchAdd(subpackageCompanyList.ToArray());
                }

                if (req.QuantitationList?.Any() ?? false)
                {
                    var quantitationList = new List<BaseQualition>();
                    req.QuantitationList.ForEach(f =>
                    {
                        quantitationList.Add(new BaseQualition()
                        {
                            xh = f.Sort,
                            ntype = f.Type,
                            sampletype = f.SampleType,
                            qualition = f.TypeValue,
                            unit = f.UnitId,
                            remarks = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    UnitWork.BatchAdd(quantitationList.ToArray());
                }
                res = await UnitWork.SaveAsync();
            }
            else
            {
                fs.Id = req.Id;
                var subpackageCompanyUpdateList = new List<Subpackage>();
                var subpackageCompanyAddList = new List<Subpackage>();
                if (req.SubpackageCompanyList?.Any() ?? false)
                {
                    var add = req.SubpackageCompanyList.Where(w => !w.Id.HasValue).ToList();
                    add.ForEach(f =>
                    {
                        subpackageCompanyAddList.Add(new Subpackage()
                        {
                            name = f.Name,
                            price = Convert.ToDouble(f.Price),
                            cycle = f.Cycle,
                            StampCma = f.CMA,
                            stampCNAS = f.CNAS,
                            stampCATL = f.CATL,
                            remark = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    if (subpackageCompanyAddList.Any())
                    {
                        UnitWork.BatchAdd(subpackageCompanyAddList.ToArray());
                    }
                    var update = req.SubpackageCompanyList.Where(w => w.Id.HasValue).ToList();
                    update.ForEach(f =>
                    {
                        subpackageCompanyUpdateList.Add(new Subpackage()
                        {
                            Id = f.Id.Value,
                            name = f.Name,
                            price = Convert.ToDouble(f.Price),
                            cycle = f.Cycle,
                            StampCma = f.CMA,
                            stampCNAS = f.CNAS,
                            stampCATL = f.CATL,
                            remark = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    if (subpackageCompanyUpdateList.Any())
                    {
                        foreach (var item in subpackageCompanyUpdateList)
                        {
                            var subpackage = UnitWork.FirstOrDefault<Subpackage>(u => u.Id == item.Id);
                            if (subpackage != null)
                            {
                                subpackage.name = item.name;
                                subpackage.price = item.price;
                                subpackage.cycle = item.cycle;
                                subpackage.StampCma = item.StampCma;
                                subpackage.stampCNAS = item.stampCNAS;
                                subpackage.stampCATL = item.stampCATL;
                                subpackage.remark = item.remark;
                                subpackage.factor_standa_id = item.factor_standa_id;
                                UnitWork.Update(subpackage);
                            }
                        }
                    }
                }
                var quantitationAddList = new List<BaseQualition>();
                var quantitationUpdateList = new List<BaseQualition>();
                if (req.QuantitationList?.Any() ?? false)
                {
                    var add = req.QuantitationList.Where(w => !w.Id.HasValue).ToList();
                    add.ForEach(f =>
                    {
                        quantitationAddList.Add(new BaseQualition()
                        {
                            xh = f.Sort,
                            ntype = f.Type,
                            sampletype = f.SampleType,
                            qualition = f.TypeValue,
                            unit = f.UnitId,
                            remarks = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    if (quantitationAddList.Any())
                    {
                        UnitWork.BatchAdd(quantitationAddList.ToArray());
                    }

                    var update = req.QuantitationList.Where(w => w.Id.HasValue).ToList();
                    update.ForEach(f =>
                    {
                        quantitationUpdateList.Add(new BaseQualition()
                        {
                            Id = f.Id.Value,
                            xh = f.Sort,
                            ntype = f.Type,
                            sampletype = f.SampleType,
                            qualition = f.TypeValue,
                            unit = f.UnitId,
                            remarks = f.Remark,
                            factor_standa_id = fs.Id
                        });
                    });
                    if (quantitationUpdateList.Any())
                    {
                        foreach (var item in quantitationUpdateList)
                        {
                            var quantitation = UnitWork.FirstOrDefault<BaseQualition>(u => u.Id == item.Id);
                            if (quantitation != null)
                            {
                                quantitation.Id = item.Id;
                                quantitation.xh = item.xh;
                                quantitation.ntype = item.ntype;
                                quantitation.sampletype = item.sampletype;
                                quantitation.qualition = item.qualition;
                                quantitation.unit = item.unit;
                                quantitation.remarks = item.remarks;
                                quantitation.factor_standa_id = item.factor_standa_id;
                                UnitWork.Update(quantitation);
                            }
                        }
                    }
                }
                res = await UnitWork.SaveAsync();
            }
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
            _cache.Set("procategoryzf_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
            return res > 0;
        }

        public async Task<List<SubpackageCompanySelectResp>> GetSubpackageCompanySelect()
        {
            var data = new List<SubpackageCompanySelectResp>();
            var query = UnitWork.Find<Subpackage>(w => !string.IsNullOrEmpty(w.name)).Select(s => s.name);
            return data = (await query.ToListAsync()).Distinct().Select(s => new SubpackageCompanySelectResp()
            {
                Name = s
            }).ToList();
        }


        public async Task<bool> BatchDeleteFactorStandard(DeleteFactorStandardReq req)
        {
            UnitWork.Delete<FactorStandard>(w => req.Ids.Contains(w.Id));
            int res = await UnitWork.SaveAsync();
            return res > 0;
        }


        public async Task<(int totalCount, List<UnitSelectResp> data)> GetUnitList(string name, int page, int limit)
        {
            var data = new List<UnitSelectResp>();
            var query = UnitWork.Find<Unit>(null).Select(s => new UnitSelectResp()
            {
                Id = s.Id,
                Name = s.unit
            });
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(w => w.Name.Contains(name));
            }
            data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            int totalCount = await query.CountAsync();
            return (totalCount, data);
        }

        public async Task<bool> AddOrUpdateUnit(AddOrUpdateUnit req)
        {
            if (req.Id > 0)
            {
                UnitWork.Update(new Unit()
                {
                    Id = req.Id,
                    unit = req.Name,
                    create_by = _auth.GetCurrentUser().User.Id,
                    create_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                });
            }
            else
            {
                UnitWork.Add(new Unit()
                {
                    unit = req.Name,
                    create_by = _auth.GetCurrentUser().User.Id,
                    create_date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                });
            }
            return (await UnitWork.SaveAsync()) > 0;
        }


        public async Task<UnitSelectResp> GetUnitDetail(int id)
        {
            var data = new UnitSelectResp();
            data = await UnitWork.Find<Unit>(null).Where(w => w.Id == id).Select(s => new UnitSelectResp()
            {
                Id = s.Id,
                Name = s.unit
            }).FirstOrDefaultAsync();

            return data;
        }

        public async Task<bool> BatchDeleteUnit(BatchDeleteUnitReq req)
        {
            UnitWork.Delete<Unit>(w => req.Id.Contains(w.Id));
            int res = await UnitWork.SaveAsync();
            return res > 0;
        }

        public async Task<List<UnitSelectResp>> GetUnitSelect()
        {
            var data = new List<UnitSelectResp>();
            data = await UnitWork.Find<Unit>(null).Select(s => new UnitSelectResp()
            {
                Id = s.Id,
                Name = s.unit
            }).ToListAsync();
            return data;
        }

        public async Task<bool> AddOrUpdateCategoryTree(AddOrUpdateCategoryTreeReq req)
        {
            int res = 0;
            var categoryTree = new Categorytree()
            {
                name = req.Name,
                pid = req.PId,
                level = req.Level,
                remarks = req.Remark,
                DataSource = req.DataSource
            };
            if (!req.Id.HasValue)
            {
                UnitWork.Add(categoryTree);
            }
            else
            {
                categoryTree.Id = req.Id.Value;
                UnitWork.Update(categoryTree);
            }
            res = await UnitWork.SaveAsync();
            return (res > 0);
        }

        public async Task<Response<bool>> DeleteCategoryTree(DeleteCategoryTreeReq req)
        {
            var result = new Response<bool>();
            try
            {
                UnitWork.Delete<Categorytree>(w => w.Id == req.Id);
                int res = await UnitWork.SaveAsync();
                result.Result = res > 0;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }


        public Response<bool> DeleteCategoryTreeProcategory(DeleteCategoryTreeReq req)
        {
            var result = new Response<bool>();
            int res = 0;
            try
            {
                var query = UnitWork.FirstOrDefault<Procategory>(w => w.Id == req.Id);
                if (query != null)
                {
                    query.pid = new int?();
                    UnitWork.Update(query);
                    res = UnitWork.SaveReturnRows();
                }
                result.Result = res > 0;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        public async Task<PageResponse<List<ProcategoryListResp>>> GetProcategoryList(string name, string standardCode, int? dataSource, string standardId, string factorId, int page, int limit)
        {
            var result = new PageResponse<List<ProcategoryListResp>>();
            try
            {
                string whereStr1 = "WHERE 1=1";
                string whereStr2 = "";
                var paramList = new List<SqlParameter>()
                {
                    new SqlParameter("Page",page - 1),
                    new SqlParameter("Limit",limit),
                };
                if (!string.IsNullOrEmpty(name))
                {
                    whereStr1 += " AND name like @ProcategoryName";
                    paramList.Add(new SqlParameter("ProcategoryName", $"%{name}%"));
                }
                if (!string.IsNullOrEmpty(standardCode))
                {
                    whereStr1 += " AND testingcode like @standardCode";
                    paramList.Add(new SqlParameter("standardCode", $"%{standardCode}%"));
                }
                if (dataSource.HasValue)
                {
                    whereStr1 += " AND DataSource = @DataSource";
                    paramList.Add(new SqlParameter("DataSource", dataSource.Value));
                }
                if (!string.IsNullOrEmpty(factorId) || !string.IsNullOrEmpty(standardId))
                {
                    whereStr2 += @" AND id IN  (SELECT t1.categoryid FROM dbo.itemdata AS t1  LEFT JOIN dbo.hk_factor_standa AS t2 ON t1.facstandid = t2.id @Where2)";
                    string whereStr3 = "WHERE 1=1";
                    if (!string.IsNullOrEmpty(factorId))
                    {
                        whereStr3 += " AND t2.factorid = @factorId";
                        paramList.Add(new SqlParameter("factorId", factorId));
                    }
                    if (!string.IsNullOrEmpty(standardId))
                    {
                        whereStr3 += " AND t2.standardid = @standardId";
                        paramList.Add(new SqlParameter("standardId", standardId));
                    }
                    whereStr2 = whereStr2.Replace("@Where2", whereStr3);
                }

                string countSqlStr = @"SELECT * FROM dbo.procategory @Where1 @Where2".Replace("@Where1", whereStr1).Replace("@Where2", whereStr2);
                string sqlStr = $@"SELECT * FROM dbo.procategory @Where1 @Where2 ORDER BY id DESC  OFFSET @Page ROW  FETCH NEXT @Limit ROW ONLY;".Replace("@Where1", whereStr1).Replace("@Where2", whereStr2);
                int totalCount = await UnitWork.Query<Procategory>(countSqlStr, paramList.ToArray()).CountAsync();
                var query = UnitWork.FromSql<Procategory>(sqlStr, paramList.ToArray());
                var data = await query.ToListAsync();
                result.Result = data.Select(s => new ProcategoryListResp()
                {
                    Id = s.Id,
                    Name = s.name,
                    StandardName = s.testing,
                    StandardCode = s.testingcode,
                    Cycle = s.tat,
                    PackageCose = s.package_price.HasValue ? s.package_price.Value.ToString() : "",
                    SampleCount = s.SampleCount,
                    DataSource = s.DataSource.ToString()
                }).ToList();
                result.TotalCount = totalCount;
                result.limit = limit;
                result.page = page;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }


        public async Task<Response<ProcategoryDetailResp>> GetProcategoryDetail(int id)
        {
            var result = new Response<ProcategoryDetailResp>();
            var data = new ProcategoryDetailResp();
            try
            {
                var queryProcategory = await UnitWork.Find<Procategory>(w => w.Id == id).FirstOrDefaultAsync();
                if (queryProcategory != null)
                {
                    data.Id = queryProcategory.Id;
                    data.Name = queryProcategory.name;
                    data.StandarFullName = queryProcategory.testing;
                    data.StandarCode = queryProcategory.testingcode;
                    data.Cycle = queryProcategory.tat;
                    data.PackagePrice = queryProcategory.package_price.HasValue ? queryProcategory.package_price.Value : 0;
                    data.TotalPrice = queryProcategory.total_price.HasValue ? queryProcategory.total_price.Value : 0;
                    data.SampleCount = queryProcategory.SampleCount;
                    data.DataSource = queryProcategory.DataSource.ToString();
                }
                var query = from i in UnitWork.Find<Itemdata>(null)
                            join fs in UnitWork.Find<FactorStandard>(null) on i.facstandid equals fs.Id into temp1
                            from t1 in temp1.DefaultIfEmpty()
                            join f in UnitWork.Find<Factor>(null) on i.itemid equals f.Id into temp2
                            from t2 in temp2.DefaultIfEmpty()
                            join s in UnitWork.Find<Standard>(null) on i.methodid equals s.Id into temp3
                            from t3 in temp3.DefaultIfEmpty()
                            join l in UnitWork.Find<Library>(null) on t3.department equals l.Id into temp4
                            from t4 in temp4.DefaultIfEmpty()
                            where i.categoryid == id.ToString()
                            select new ProcategoryDetailItemdata()
                            {
                                Id = i.Id,
                                Sort = i.ino.HasValue ? i.ino.Value : 0,
                                FactorId = t2.Id,
                                FactorName = t2.factorname,
                                StandardId = t3.Id,
                                StandardShortName = t3.testfunc,
                                FactorStandardId = t1.Id,
                                JudgmentBasis = i.PdYj,
                                DepartmentId = t3.department,
                                DepartmentName = t3.departmentName,
                                Limit = i.zb,
                                UnitId = i.UnitId.HasValue ? i.UnitId.Value : 0,
                                Unit = i.zbunit,
                                CMA = t1.StampCma.HasValue ? Convert.ToBoolean(t1.StampCma.Value) : false,
                                CNAS = t1.stampCNAS.HasValue ? Convert.ToBoolean(t1.stampCNAS.Value) : false,
                                CATL = t1.stampCATL.HasValue ? Convert.ToBoolean(t1.stampCATL.Value) : false,
                                FactorRemark = t2.remarks,
                                StandardRemark = t3.remarks,
                                IsStop = t1.IsStop.HasValue ? Convert.ToBoolean(t1.IsStop.Value) : false,
                                MuCount = t1.mucount,
                                Cose = (!i.parallel.HasValue || i.parallel.Value == 0) ? t3.cose : t3.parallel_cose,
                                ParallelCose = t3.parallel_cose,
                                IsParallel = i.parallel.HasValue ? Convert.ToBoolean(i.parallel.Value) : false,
                                RangeOfApplication = t3.range_of_application,
                                Quantitation = string.IsNullOrEmpty(t1.quantitation) ? t1.detection : t1.quantitation,
                                Detection = t1.detection,
                                IsSampling = t1.IsCaiyang.HasValue ? Convert.ToBoolean(t1.IsCaiyang.Value) : false,
                                SampleCout = t3.sample_count,
                            };
                data.ProcategoryDetailItemdata = await query.OrderBy(o => o.Sort).ToListAsync();
                result.Result = data;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        public async Task<bool> AddOrUpdateProcategory(AddOrUpdateProcategoryReq req)
        {
            int res = 0;
            var procategory = new Procategory()
            {
                name = req.Name,
                testing = req.StandarFullName,
                testingcode = req.StandarCode,
                tat = req.Cycle,
                package_price = req.PackagePrice,
                total_price = Convert.ToDouble(req.TotalPrice),
                SampleCount = req.SampleCount,
                DataSource = req.DataSource,
                pid = req.CategoryTreeId.HasValue ? req.CategoryTreeId.Value : new int?()
            };
            var addItemdata = new List<Itemdata>();
            var updateItemdata = new List<Itemdata>();
            if (req.Itemdata?.Any() ?? false)
            {
                var addData = req.Itemdata.Where(w => !w.Id.HasValue).ToList();
                addItemdata = addData.Select(s => new Itemdata()
                {
                    ino = s.Sort,
                    UnitId = s.UnitId,
                    facstandid = s.FactorStandardId,
                    itemid = s.FactorId,
                    methodid = s.StandardId,
                    itemremark = s.FactorRemark,
                    methodremark = s.StandardRemark,
                    zb = s.Limit,
                    zbunit = s.Unit,
                    PdYj = s.JudgmentBasis
                }).ToList();

                var updateData = req.Itemdata.Where(w => w.Id.HasValue).ToList();
                updateItemdata = updateData.Select(s => new Itemdata()
                {
                    Id = s.Id.Value,
                    ino = s.Sort,
                    UnitId = s.UnitId,
                    facstandid = s.FactorStandardId,
                    itemid = s.FactorId,
                    methodid = s.StandardId,
                    itemremark = s.FactorRemark,
                    methodremark = s.StandardRemark,
                    zb = s.Limit,
                    zbunit = s.Unit,
                    PdYj = s.JudgmentBasis
                }).ToList();
            }
            if (!req.Id.HasValue)
            {
                UnitWork.Add(procategory);
                await UnitWork.SaveAsync();
            }
            else
            {
                procategory.Id = req.Id.Value;
                UnitWork.Update(procategory);
            }

            if (addItemdata.Any())
            {
                addItemdata.ForEach(f => f.categoryid = procategory.Id.ToString());
                UnitWork.BatchAdd(addItemdata.ToArray());
            }
            if (updateItemdata.Any())
            {
                updateItemdata.ForEach(f => f.categoryid = procategory.Id.ToString());
                foreach (var item in updateItemdata)
                {
                    var itemData = UnitWork.FirstOrDefault<Itemdata>(w => w.Id == item.Id);
                    if (itemData != null)
                    {
                        itemData.ino = item.ino;
                        itemData.UnitId = item.UnitId;
                        itemData.facstandid = item.facstandid;
                        itemData.itemid = item.itemid;
                        itemData.methodid = item.methodid;
                        itemData.itemremark = item.itemremark;
                        itemData.methodremark = item.methodremark;
                        itemData.zb = item.zb;
                        itemData.zbunit = item.zbunit;
                        itemData.PdYj = item.PdYj;
                        UnitWork.Update(itemData);
                    }
                }
            }
            res = await UnitWork.SaveAsync();
            return res > 0;
        }

        public async Task<bool> BatchDeleteProcategory(BatchDeleteProcategoryReq req)
        {
            UnitWork.Delete<Procategory>(w => req.Ids.Contains(w.Id));
            int res = await UnitWork.SaveAsync();
            return res > 0;
        }


        public PageResponse<List<ProcategoryListResp>> UpdateProcategoryBySelect(UpdateProcategoryBySelectReq req)
        {
            var result = new PageResponse<List<ProcategoryListResp>>();
            try
            {
                var procategory = UnitWork.FirstOrDefault<Procategory>(w => w.Id == req.ProcategoryId);
                if (procategory != null)
                {
                    procategory.pid = req.CategoryTreeId;
                    UnitWork.Update(procategory);
                }
                int res = UnitWork.SaveReturnRows();
                var queryProcategoryList = new List<ProcategoryListResp>();
                if (res > 0)
                {
                    var query = UnitWork.Find<Procategory>(w => w.pid == req.CategoryTreeId);
                    int totalCount = query.Count();
                    if (totalCount > 0)
                    {
                        queryProcategoryList = query.Select(s => new ProcategoryListResp()
                        {
                            Id = s.Id,
                            Name = s.name,
                            StandardName = s.testing,
                            StandardCode = s.testingcode,
                            Cycle = s.tat,
                            PackageCose = s.package_price.HasValue ? s.package_price.Value.ToString() : "",
                            SampleCount = s.SampleCount,
                            DataSource = s.DataSource.ToString()
                        }).Skip((req.Page - 1) * req.Limit).Take(req.Limit).ToList();
                    }
                    result.TotalCount = totalCount;
                    result.Result = queryProcategoryList;
                    result.page = req.Page;
                    result.limit = req.Limit;
                }
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

        public PageResponse<List<ProcategoryListResp>> GetProcategoryPageListByCategoryTreeId(int categoryTreeId, int page, int limit)
        {
            var result = new PageResponse<List<ProcategoryListResp>>();
            try
            {
                var query = UnitWork.Find<Procategory>(w => w.pid == categoryTreeId);
                int totalCount = query.Count();
                var queryProcategoryList = new List<ProcategoryListResp>();
                if (totalCount > 0)
                {
                    queryProcategoryList = query.Select(s => new ProcategoryListResp()
                    {
                        Id = s.Id,
                        Name = s.name,
                        StandardName = s.testing,
                        StandardCode = s.testingcode,
                        Cycle = s.tat,
                        PackageCose = s.package_price.HasValue ? s.package_price.Value.ToString() : "",
                        SampleCount = s.SampleCount,
                        DataSource = s.DataSource.ToString()
                    }).Skip((page - 1) * limit).Take(limit).ToList();
                }
                result.TotalCount = totalCount;
                result.Result = queryProcategoryList;
                result.page = page;
                result.limit = limit;
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return result;
        }

    }
}