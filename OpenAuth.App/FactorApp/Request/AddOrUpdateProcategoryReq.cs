﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 添加或修改产品项目标准
    /// </summary>
    public class AddOrUpdateProcategoryReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 产品分类id
        /// </summary>
        public int? CategoryTreeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 产品标准全称
        /// </summary>
        public string StandarFullName { get; set; }
        /// <summary>
        /// 产品标准代号
        /// </summary>
        public string StandarCode { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal PackagePrice { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// 样品总量
        /// </summary>
        public string SampleCount { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public int DataSource { get; set; }
        /// <summary>
        /// 产品项目分类
        /// </summary>
        public List<AddOrUpdateProcategoryItemdata> Itemdata { get; set; }
    }


    /// <summary>
    /// 产品项目分类
    /// </summary>
    public class AddOrUpdateProcategoryItemdata
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 项目方法关联id
        /// </summary>
        public string FactorStandardId { get; set; }
        /// <summary>
        /// 项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 项目备注
        /// </summary>
        public string FactorRemark { get; set; }
        /// <summary>
        /// 方法id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 方法备注
        /// </summary>
        public string StandardRemark { get; set; }
        /// <summary>
        /// 限值
        /// </summary>
        public string Limit { get; set; }
        /// <summary>
        /// 单位id
        /// </summary>
        public int UnitId { get; set; }
        /// <summary>
        /// 限值单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 判定依据
        /// </summary>
        public string JudgmentBasis { get; set; }
    }
}
