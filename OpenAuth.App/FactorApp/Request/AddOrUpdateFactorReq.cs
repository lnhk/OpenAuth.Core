﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using OpenAuth.Repository.Core;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 添加修改项目请求类
    /// </summary>

    public class AddOrUpdateFactorReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string Factorname { get; set; }

        /// <summary>
        /// 项目别名
        /// </summary>
        public string Displayname { get; set; }

        /// <summary>
        /// 项目英文名
        /// </summary>
        public string EFactorname { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        ///数据源（1-委托，2-抽检，3-环境）
        /// </summary>
        public int DataSource { get; set; }
    }
}