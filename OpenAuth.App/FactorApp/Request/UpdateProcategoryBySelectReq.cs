﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateProcategoryBySelectReq
    {
        /// <summary>
        /// 产品分类id
        /// </summary>
        public int CategoryTreeId { get; set; }
        /// <summary>
        /// 产品ID
        /// </summary>
        public int ProcategoryId { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int Page { get; set; } = 1;
        /// <summary>
        /// 每页数据
        /// </summary>
        public int Limit { get; set; } = 20;
    }
}
