﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 添加或修改产品分类
    /// </summary>
    public class AddOrUpdateCategoryTreeReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 父类id
        /// </summary>
        public int PId { get; set; }
        /// <summary>
        /// 分类级别
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 数据源
        /// </summary>
        public int DataSource { get; set; }
    }
}
