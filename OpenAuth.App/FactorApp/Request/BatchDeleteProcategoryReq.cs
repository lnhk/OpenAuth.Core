﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BatchDeleteProcategoryReq
    {
        /// <summary>
        /// 数据id列表
        /// </summary>
        public List<int> Ids { get; set; }
    }
}
