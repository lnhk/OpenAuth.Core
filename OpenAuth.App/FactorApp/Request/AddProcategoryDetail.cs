﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 添加产品分类明细
    /// </summary>
    public class AddProcategoryDetail
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 产品分类id
        /// </summary>
        public int CategoryTreeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 产品标准全称
        /// </summary>
        public string StandarFullName { get; set; }
        /// <summary>
        /// 产品标准代号
        /// </summary>
        public string StandarCode { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal PackagePrice { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// 样品总量
        /// </summary>
        public string SampleCount { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public int DataSource { get; set; }
        /// <summary>
        /// 产品项目分类
        /// </summary>
        public List<AddOrUpdateProcategoryItemdata> Itemdata { get; set; }
    }
}
