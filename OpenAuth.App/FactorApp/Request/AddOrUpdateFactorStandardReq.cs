﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 添加或者修改项目方法关联
    /// </summary>
    public class AddOrUpdateFactorStandardReq
    {
        //public AddOrUpdateFactorStandardReq()
        //{
        //    SubpackageCompanyList = new List<AddOrUpdateFactorStandardSubpackageReq>();
        //    QuantitationList = new List<AddOrUpdateFactorStandardQuantitationReq>();
        //}

        /// <summary>
        /// 数据id
        /// </summary>
        public string Id { get; set; }        
        /// <summary>
        /// 检测项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 检测依据id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 定量限
        /// </summary>
        public string Quantitation { get; set; }
        /// <summary>
        /// 检出限
        /// </summary>
        public string Detection { get; set; }
        /// <summary>
        /// 制样要求
        /// </summary>
        public string SampleRequired { get; set; }
        /// <summary>
        /// 分包实验室类型
        /// </summary>
        public int? SubpackageLabType { get; set; }
        /// <summary>
        /// CMA
        /// </summary>
        public bool? CMA { get; set; }
        /// <summary>
        /// CMA备注
        /// </summary>
        public string CMARemark { get; set; }
        /// <summary>
        /// CNAS
        /// </summary>
        public bool? CNAS { get; set; }
        /// <summary>
        /// CNAS备注
        /// </summary>
        public string CNASRemark { get; set; }
        /// <summary>
        /// CATL
        /// </summary>
        public bool? CATL { get; set; }
        /// <summary>
        /// CATL备注
        /// </summary>
        public string CATLRemark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否停用
        /// </summary>
        public bool? IsStop { get; set; }
        /// <summary>
        /// 是否涉及采样
        /// </summary>
        public bool? IsSampling { get; set; }
        /// <summary>
        /// 停用原因
        /// </summary>
        public string StopReason { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public int DataSource { get; set; }
        ///// <summary>
        ///// 创建人
        ///// </summary>
        //public string CreateUser { get; set; }
        ///// <summary>
        ///// 修改人
        ///// </summary>
        //public string UpdateUserId { get; set; }

        /// <summary>
        /// 分包公司
        /// </summary>
        public List<AddOrUpdateFactorStandardSubpackageReq> SubpackageCompanyList { get; set; }
        
        /// <summary>
        /// 检出限/定量限
        /// </summary>
        public List<AddOrUpdateFactorStandardQuantitationReq> QuantitationList { get; set; }
    }

    public class AddOrUpdateFactorStandardSubpackageReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 分包公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 检测周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// CMA
        /// </summary>
        public bool CMA { get; set; }
        /// <summary>
        /// CNAS
        /// </summary>
        public bool CNAS { get; set; }
        public bool CATL { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    public class AddOrUpdateFactorStandardQuantitationReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 样品类型
        /// </summary>
        public string SampleType { get; set; }
        /// <summary>
        /// 类型值
        /// </summary>
        public string TypeValue { get; set; }
        /// <summary>
        /// 单位id
        /// </summary>
        public int UnitId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
