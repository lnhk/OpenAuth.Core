﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 删除项目方法关联
    /// </summary>
    public class DeleteFactorStandardReq
    {
        /// <summary>
        /// 数据id列表
        /// </summary>
        public List<string> Ids { get; set; }
    }
}
