namespace OpenAuth.App.Request
{
    public class QueryFactorListReq : PageReq
    {
        /// <summary>
        /// 项目名
        /// </summary>
        public string FactorName { get; set; }

        /// <summary>
        /// 数据来源
        /// </summary>
        public int? DataSource { get; set; }
    }
}
