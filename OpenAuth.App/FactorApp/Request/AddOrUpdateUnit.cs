﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 新增或修改单位
    /// </summary>
    public class AddOrUpdateUnit
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string Name { get; set; }
    }
}
