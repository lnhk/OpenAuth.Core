﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class DeleteCategoryTreeReq
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
    }
}
