﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 产品项目分类响应类
    /// </summary>
    public class ProcategoryItemdataListResp
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 检测项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 检测项目名称
        /// </summary>
        public string FactorName { get; set; }
        /// <summary>
        /// 检测依据id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 检测依据名称
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// 检测依据简称
        /// </summary>
        public string StandardShortName { get; set; }
        /// <summary>
        /// 项目方法关联id
        /// </summary>
        public string FactorStandardId { get; set; }
        /// <summary>
        /// 限量
        /// </summary>
        public string Limit { get; set; }
        /// <summary>
        /// 单位Id
        /// </summary>
        public int UnitId { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 是否CMA
        /// </summary>
        public bool CMA { get; set; }
        /// <summary>
        /// 是否CNAS
        /// </summary>
        public bool CNAS { get; set; }
        /// <summary>
        /// 是否CATL
        /// </summary>
        public bool CATL { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 样品量
        /// </summary>
        public string SampleCout { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 部门id
        /// </summary>
        public string DepartmentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// 适用范围
        /// </summary>
        public string RangeOfApplication { get; set; }
        /// <summary>
        /// 定量限
        /// </summary>
        public string Quantitation { get; set; }
        /// <summary>
        /// 制样要求
        /// </summary>
        public string SampleRequired { get; set; }
        /// <summary>
        /// 是否分包
        /// </summary>
        public bool IsSubpackage { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Cose { get; set; }
        /// <summary>
        /// 是否采样
        /// </summary>
        public bool IsSampling { get; set; }
        /// <summary>
        /// 是否平行
        /// </summary>
        public bool IsParallel { get; set; }
        /// <summary>
        /// 平行价格
        /// </summary>
        public string ParallelCose { get; set; }
        /// <summary>
        /// 单项价格
        /// </summary>
        public string Scose { get; set; }
        /// <summary>
        /// 多余价格
        /// </summary>
        public string Coses { get; set; }
        /// <summary>
        /// 价格说明
        /// </summary>
        public string Coseremark { get; set; }
        /// <summary>
        /// 分包公司id
        /// </summary>
        public int SubpackageCompanyId { get; set; }

        /// <summary>
        /// 分包公司
        /// </summary>
        public string SubpackageCompanyName { get; set; }
        /// <summary>
        /// 分包价格
        /// </summary>
        public string SubpackageCose { get; set; }
        /// <summary>
        /// *周期
        /// </summary>
        public string SubpackageCycle { get; set; }
        /// <summary>
        /// 是否*CMA
        /// </summary>
        public bool SubpackageCMA { get; set; }
        /// <summary>
        /// 是否*CNAS
        /// </summary>
        public bool SubpackageCNAS { get; set; }
        /// <summary>
        /// 是否*CATL
        /// </summary>
        public bool SubpackageCATL { get; set; }
        /// <summary>
        /// *备注
        /// </summary>
        public string SubpackageRemark { get; set; }
    }
}
