﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 检测项目响应类
    /// </summary>
    public class FactorResp
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string factorname { get; set; }

        /// <summary>
        /// 项目别名
        /// </summary>
        public string displayname { get; set; }

        /// <summary>
        /// 项目英文名
        /// </summary>
        public string efactorname { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        ///数据源
        /// </summary>
        public string DataSource { get; set; }
    }
}
