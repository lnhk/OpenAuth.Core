﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 项目方法关联响应类
    /// </summary>
    public class FactorStandardListResp
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public string DataSource { get; set; }
        /// <summary>
        /// 检测项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 检测项目名称
        /// </summary>
        public string FactorName { get; set; }
        /// <summary>
        /// 项目别名
        /// </summary>
        public string FactorOtherName { get; set; }
        /// <summary>
        /// 检测依据id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 检测依据名称
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// CMA
        /// </summary>
        public bool CMA { get; set; }
        /// <summary>
        /// CMA备注
        /// </summary>
        public string CMARemark { get; set; }
        /// <summary>
        /// CNAS
        /// </summary>
        public bool CNAS { get; set; }
        /// <summary>
        /// CNAS备注
        /// </summary>
        public string CNASRemark { get; set; }
        /// <summary>
        /// CATL
        /// </summary>
        public bool CATL { get; set; }
        /// <summary>
        /// CATL备注
        /// </summary>
        public string CATLRemark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 部门id
        /// </summary>
        public string LabId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string LabName { get; set; }
        /// <summary>
        /// 适用范围
        /// </summary>
        public string RangeOfApplication { get; set; }
        /// <summary>
        /// 定量限
        /// </summary>
        public string Quantitation { get; set; }
        /// <summary>
        /// 制样要求
        /// </summary>
        public string SampleRequired { get; set; }
        /// <summary>
        /// 是否分包
        /// </summary>
        public bool IsSubpackage { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Cose { get; set; }
        /// <summary>
        /// 平行价格
        /// </summary>
        public string ParallelCose { get; set; }
        /// <summary>
        /// 单项价格
        /// </summary>
        public string Scose { get; set; }
        /// <summary>
        /// 多余价格
        /// </summary>
        public string Coses { get; set; }
        /// <summary>
        /// 价格说明
        /// </summary>
        public string Coseremark { get; set; }
        /// <summary>
        /// 检测周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 数据状态(0-扩项中,1-可用数据)
        /// </summary>
        public int DataStatus { get; set; }
        /// <summary>
        /// 是否停用
        /// </summary>
        public bool IsStop { get; set; }
        /// <summary>
        /// 是否涉及采样
        /// </summary>
        public bool IsSampling { get; set; }
        /// <summary>
        /// 样品量
        /// </summary>
        public string SampleCount { get; set; }
        /// <summary>
        /// 检出限
        /// </summary>
        public string Detection { get; set; }
        /// <summary>
        /// 停用原因
        /// </summary>
        public string StopReason { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 最后更新时间
        /// </summary>
        public string UpdateTime { get; set; }
    }
}
