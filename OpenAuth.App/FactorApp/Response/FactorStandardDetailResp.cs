﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 项目方法关联明细响应类
    /// </summary>
    public class FactorStandardDetailResp
    {
        public FactorStandardDetailResp()
        {
            SubpackageCompanyList = new List<FactorStandardDetailSubpackageCompany>();
            QualitionList = new List<FactorStandardDetailQualition>();
        }
        /// <summary>
        /// 数据id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 检测项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 检测项目名称
        /// </summary>
        public string FactorName { get; set; }
        /// <summary>
        /// 检测依据id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 检测依据名称
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// 检测依据简称
        /// </summary>
        public string StandardShortName { get; set; }
        ///// <summary>
        ///// 判断依据
        ///// </summary>
        //public string JudgmentData { get; set; }
        /// <summary>
        ///// 限量
        ///// </summary>
        //public string Limit { get; set; }
        ///// <summary>
        ///// 单位
        ///// </summary>
        //public string Unit { get; set; }
        /// <summary>
        /// 是否CMA
        /// </summary>
        public bool CMA { get; set; }
        /// <summary>
        /// CMA备注
        /// </summary>
        public string CMARemark { get; set; }
        /// <summary>
        /// 是否CNAS
        /// </summary>
        public bool CNAS { get; set; }
        /// <summary>
        /// CNAS备注
        /// </summary>
        public string CNASRemark { get; set; }
        /// <summary>
        /// 是否CATL
        /// </summary>
        public bool CATL { get; set; }
        /// <summary>
        /// CATL备注
        /// </summary>
        public string CATLRemark { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 是否停用
        /// </summary>
        public bool IsStop { get; set; }
        /// <summary>
        /// 是否涉及采样
        /// </summary>
        public bool IsSampling { get; set; }
        /// <summary>
        /// 停用原因
        /// </summary>
        public string StopReason { get; set; }
        /// <summary>
        /// 部门id
        /// </summary>
        public string DepartmentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// 适用范围
        /// </summary>
        public string RangeOfApplication { get; set; }
        /// <summary>
        /// 定量限
        /// </summary>
        public string Quantitation { get; set; }
        /// <summary>
        /// 制样要求
        /// </summary>
        public string SampleRequired { get; set; }
        /// <summary>
        /// 是否分包
        /// </summary>
        public bool IsSubpackage { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Cose { get; set; }
        /// <summary>
        /// 错误原因
        /// </summary>
        public string ErrorReason { get; set; }
        /// <summary>
        /// 单项价格
        /// </summary>
        public string Scose { get; set; }
        /// <summary>
        /// 多余价格
        /// </summary>
        public string Coses { get; set; }
        /// <summary>
        /// 平行价格
        /// </summary>
        public string ParallelCose { get; set; }
        /// <summary>
        /// 价格说明
        /// </summary>
        public string Coseremark { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }

        /// <summary>
        /// 分包公司
        /// </summary>
        public string SubpackageCompanyName { get; set; }
        /// <summary>
        /// 分包价格
        /// </summary>
        public string SubpackageCose { get; set; }
        /// <summary>
        /// *周期
        /// </summary>
        public string SubpackageCycle { get; set; }
        /// <summary>
        /// 是否*CMA
        /// </summary>
        public bool SubpackageCMA { get; set; }
        /// <summary>
        /// 是否*CNAS
        /// </summary>
        public bool SubpackageCNAS { get; set; }
        /// <summary>
        /// 是否*CATL
        /// </summary>
        public bool SubpackageCATL { get; set; }
        /// <summary>
        /// *备注
        /// </summary>
        public string SubpackageRemark { get; set; }
        /// <summary>
        /// 分包公司列表
        /// </summary>
        public List<FactorStandardDetailSubpackageCompany> SubpackageCompanyList { get; set; }
        /// <summary>
        /// 检出限/定量限
        /// </summary>
        public List<FactorStandardDetailQualition> QualitionList { get; set; }
    }

    public class FactorStandardDetailSubpackageCompany
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 分包公司
        /// </summary>
        public string SubpackageCompanyName { get; set; }
        /// <summary>
        /// 分包价格
        /// </summary>
        public string SubpackageCose { get; set; }
        /// <summary>
        /// *周期
        /// </summary>
        public string SubpackageCycle { get; set; }
        /// <summary>
        /// 是否*CMA
        /// </summary>
        public bool SubpackageCMA { get; set; }
        /// <summary>
        /// 是否*CNAS
        /// </summary>
        public bool SubpackageCNAS { get; set; }
        /// <summary>
        /// 是否*CATL
        /// </summary>
        public bool SubpackageCATL { get; set; }
        /// <summary>
        /// *备注
        /// </summary>
        public string SubpackageRemark { get; set; }
    }

    public class FactorStandardDetailQualition
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 样品类型
        /// </summary>
        public string SampleType { get; set; }
        /// <summary>
        /// 类型值
        /// </summary>
        public string TypeValue { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
