﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 分包公司下拉框
    /// </summary>
    public class SubpackageCompanySelectResp
    {
        /// <summary>
        /// 分包公司名称
        /// </summary>
        public string Name { get; set; }
    }
}
