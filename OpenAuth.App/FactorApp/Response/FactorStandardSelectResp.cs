﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 项目方法关联下拉框
    /// </summary>
    public class FactorStandardSelectResp
    {
        /// <summary>
        /// id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 项目别名
        /// </summary>
        public string OtherName { get; set; }
    }
}
