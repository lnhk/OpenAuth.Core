﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 产品项目标准 响应类
    /// </summary>
    public class ProcategoryListResp
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 产品标准全称
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// 产品标准代号
        /// </summary>
        public string StandardCode { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 打包价格
        /// </summary>
        public string PackageCose { get; set; }
        /// <summary>
        /// 样品总量
        /// </summary>
        public string SampleCount { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public string DataSource { get; set; }
    }
}
