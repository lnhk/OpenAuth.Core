﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 产品项目标准明细
    /// </summary>
    public class ProcategoryDetailResp
    {
        public ProcategoryDetailResp()
        {
            ProcategoryDetailItemdata = new List<ProcategoryDetailItemdata>();
        }
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 产品标准全称
        /// </summary>
        public string StandarFullName { get; set; }
        /// <summary>
        /// 产品标准代号
        /// </summary>
        public string StandarCode { get; set; }
        /// <summary>
        /// 周期
        /// </summary>
        public string Cycle { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal PackagePrice { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public double TotalPrice { get; set; }
        /// <summary>
        /// 样品总量
        /// </summary>
        public string SampleCount { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public string DataSource { get; set; }
        /// <summary>
        /// 检测项目方法和限值
        /// </summary>
        public List<ProcategoryDetailItemdata> ProcategoryDetailItemdata { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ProcategoryDetailItemdata
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 项目方法关联id
        /// </summary>
        public string FactorStandardId { get; set; }
        /// <summary>
        /// 项目id
        /// </summary>
        public string FactorId { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string FactorName { get; set; }
        /// <summary>
        /// 项目备注
        /// </summary>
        public string FactorRemark { get; set; }
        /// <summary>
        /// 方法id
        /// </summary>
        public string StandardId { get; set; }
        /// <summary>
        /// 检测依据简称
        /// </summary>
        public string StandardShortName { get; set; }
        /// <summary>
        /// 方法备注
        /// </summary>
        public string StandardRemark { get; set; }
        /// <summary>
        /// 限值
        /// </summary>
        public string Limit { get; set; }
        /// <summary>
        /// 单位id
        /// </summary>
        public int UnitId { get; set; }
        /// <summary>
        /// 限值单位
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// 判定依据
        /// </summary>
        public string JudgmentBasis { get; set; }
        /// <summary>
        /// 部门id
        /// </summary>
        public string DepartmentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// 是否停用
        /// </summary>
        public bool IsStop { get; set; }
        /// <summary>
        /// 目数
        /// </summary>
        public string MuCount { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Cose { get; set; }
        /// <summary>
        /// 平行价格
        /// </summary>
        public string ParallelCose { get; set; }
        /// <summary>
        /// 是否平行
        /// </summary>
        public bool IsParallel { get; set; }
        /// <summary>
        /// 适用范围
        /// </summary>
        public string RangeOfApplication { get; set; }
        /// <summary>
        /// 定量限
        /// </summary>
        public string Quantitation { get; set; }
        /// <summary>
        /// 检出限
        /// </summary>
        public string Detection { get; set; }
        /// <summary>
        /// 是否CMA
        /// </summary>
        public bool CMA { get; set; }
        /// <summary>
        /// 是否CNAS
        /// </summary>
        public bool CNAS { get; set; }
        /// <summary>
        /// 是否CATL
        /// </summary>
        public bool CATL { get; set; }
        /// <summary>
        /// 是否采样
        /// </summary>
        public bool IsSampling { get; set; }
        /// <summary>
        /// 样品量
        /// </summary>
        public string SampleCout { get; set; }
    }
}
