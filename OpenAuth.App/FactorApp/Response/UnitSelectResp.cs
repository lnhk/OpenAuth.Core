﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 单位
    /// </summary>
    public class UnitSelectResp
    {
        /// <summary>
        /// 单位Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        public string Name { get; set; }
    }
}
