﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    /// <summary>
    /// 产品标准分类响应类
    /// </summary>
    public class CategorytreeListResp
    {
        /// <summary>
        /// 数据id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 父id
        /// </summary>
        public int PId { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 风险
        /// </summary>
        public string Fx { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public string DataSource { get; set; }
    }
}
