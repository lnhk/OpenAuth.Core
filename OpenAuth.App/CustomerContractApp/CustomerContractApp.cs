using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class CustomerContractApp : BaseLongApp<CustomerContract,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryCustomerContractListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public List<CustomerContract> GetByComapnyId(decimal? companyId)
        {
            List<CustomerContract> cstmrContracts=UnitWork.Find<CustomerContract>(null).Where((cc) => cc.CompanyId == companyId).ToList();
            return cstmrContracts;
        }

        public void Add(CustomerContract cstmrContract)
        {
            AddNoSave(cstmrContract);
            UnitWork.Save();
        }
        
        public void AddNoSave(CustomerContract cstmrContract)
        {
            var user = _auth.GetCurrentUser().User;
            cstmrContract.CreateUserId = user.Id;
            cstmrContract.CreateTime=DateTime.Now;
            UnitWork.Add(cstmrContract);
        }

        public void Update(UpdateCustomerContractReq obj)
        {
            UnitWork.Update<CustomerContract>(u => u.Id == obj.Id, u => new CustomerContract
            {
                CompanyId=u.CompanyId,
                Phone=u.Phone,
                Username=u.Username,
                CreateUserId=u.CreateUserId,
                CreateTime=u.CreateTime,
                Email=u.Email,
                Remarks=u.Remarks,
            });
        }

        public CustomerContractApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<CustomerContract,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}