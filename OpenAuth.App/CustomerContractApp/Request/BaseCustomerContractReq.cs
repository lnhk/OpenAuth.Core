﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseCustomerContractReq
    {
        /// <summary>
        ///联系人邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///联系人姓名
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        ///联系人电话
        /// </summary>
        public string Phone { get; set; }

        ///// <summary>
        /////创建时间(后端操作)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建者编号(后端操作)
        ///// </summary>
        //public string CreateUserId { get; set; }

    }
}
