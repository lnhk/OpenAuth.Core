﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Entity
{
    public class EmailMsg
    {
        public bool success { get; set; }
        public string msg { get; set; }
    }
}
