﻿using OpenAuth.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Entity
{
    public class BaseData
    {
        public string version { get; set; }
        public List<Factor> Factors { get; set; }
        public List<Standard> Standards { get; set; }
        public List<FactorStandard> FactorStandards { get; set; }
        //public List<ItemdataZf> ZfItemdatas { get; set; }
        public List<BaseQualition> ZfQualitions { get; set; }
    }
}
