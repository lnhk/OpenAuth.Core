﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OpenAuth.App.Common.Entity
{
    public class ChatHub : Hub
    {
        public static ChatHub hub = new ChatHub();
        public async Task SendMessage(string spSCode)
        {
            hub.Clients = Clients;
            hub.Context = Context;
            hub.Groups = Groups;
            await Clients.All.SendAsync("ReceiveMessage", spSCode);
        }
    }
}