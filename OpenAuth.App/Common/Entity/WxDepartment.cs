﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Entity
{
    public class WxDepartment
    {
        public static Dictionary<string, string> LibraryDic = new Dictionary<string, string>();
        static WxDepartment()
        {
            LibraryDic.Add("lihuawujichanggui", "14");
            LibraryDic.Add("lihuawujijinshu", "15");
            LibraryDic.Add("lihuayouji", "16");
            LibraryDic.Add("weishengwu", "13");
            LibraryDic.Add("xianchang", "45");
            LibraryDic.Add("fenbao", "42");
            LibraryDic.Add("huanjing", "42");
            LibraryDic.Add("qixiang", "17");
        }
        public const string 合同评审 = "8";
        public const string 样品管理 = "58";
        public const string 环境样品管理 = "59";
        public const string 报告编制 = "7";
        public const string 报告审核 = "10";
        public const string 报告批准 = "11";
        public const string 报告发送 = "12";
        public const string 客服 = "36";
        public const string 抽检样品接收 = "60";
        public const string 政务主管 = "62";
        public const string 采样主管 = "61";
    }
}