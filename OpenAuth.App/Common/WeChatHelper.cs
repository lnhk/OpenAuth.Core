﻿using OpenAuth.App.Common.Entity;
using OpenAuth.Repository.Domain;
using Senparc.Weixin.Entities;
using Senparc.Weixin.Work.AdvancedAPIs;
using Senparc.Weixin.Work.Containers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace OpenAuth.App.Common
{
    public  class WeChatHelper
    {
        public SenparcWeixinSetting senparcWeixinSetting { get; set; }
        private  readonly HttpClient _httpClient = new HttpClient();
        public  async void HttpAsync(string url)
        {
            try
            {
                var result = await _httpClient.GetAsync(url);
            }
            catch (Exception w)
            {

            }
        }

        private SampleReportApp _spReportApp { get; set; }
        private SampleApp _spApp { get; set; }
        private UserManagerApp _userApp { get; set; }

        public WeChatHelper(SampleReportApp spReportApp, SampleApp spApp, UserManagerApp userApp)
        {
            _spReportApp = spReportApp;
            _spApp = spApp;
            _userApp = userApp;
        }

        public async void SendLIMSMessage(string content, string partid, string user)
        {
            try
            {
                string accessToken = AccessTokenContainer.TryGetToken(senparcWeixinSetting.WeixinCorpId, senparcWeixinSetting.WeixinCorpSecret);

                await MassApi.SendTextAsync(accessToken, "1000002", content, user, partid);
                if (!content.Contains("已流转至您的实验室"))
                {
                    await ChatHub.hub.SendMessage(content);
                }
            }
            catch (Exception e)
            {
                //暂不处理
            }
        }
        public async void SendStatuMessage(string content, string partid, string user)
        {
            try
            {
                string accessToken = AccessTokenContainer.TryGetToken(senparcWeixinSetting.WeixinCorpId, senparcWeixinSetting.WeixinCorpSecret);
                await MassApi.SendMarkdownAsync(accessToken, "1000002", content, user, partid);
            }
            catch (Exception e)
            {
                //暂不处理
            }
        }

        public void SendMailPort(decimal portid, string kehu, string chaos, string userid)
        {
            SampleReport spReport = _spReportApp.Get(portid);

            Sample sample = _spApp.Get((decimal)spReport.sampleid);
            User user = _userApp.Get(userid);

            // string[] sendemail = ((string)user.SendEmail).Split('*');
            string[] sendemail = ("zhaotong @lnhkjc.cn* Qweasd123").Split('*');
            // string[] kehus = kehu.Split('*');
            string[] kehus = ("1*1534878883@qq.com").Split('*');
            //string[] chaoss = chaos.Split(',');
            string[] chaoss = ("贺弘扬*1534878883@qq.com").Split(',');
            List<String> clist = new List<string>();
            string subject = sample.sp_s_code + "-" + /*kehus[0]*/"客户名字" + "-" + sample.sp_s_7 + " 报告";

            foreach (var item in chaoss)
            {
                if (String.IsNullOrWhiteSpace(item))
                {
                    continue;
                }
                string email = item.Split('*')[1];
                if ((!email.Contains("null")) && (!string.IsNullOrEmpty(email)))
                {
                    clist.Add(email);
                }

            }
            string content = @"尊敬的客户：

       您好！

       非常感谢您接受辽宁惠康检测的服务，请查收附件中您所送样品的检测报告，如有任何的疑问或建议，请联系为您服务的销售代表，惠康检测将竭诚为您提供服务！

顺祝商祺！";
            string attachFile = @"D:\hklims\reportpdf\" + DateTime.Parse(spReport.CreateTime + "").ToString("yyyyMM") + "/pdf/" + spReport.report_pdf_name;
            EmailMsg succ = SmtpHelper.SendMail(kehus[1],clist,subject, content, attachFile,sendemail);
            spReport.issend = false;

            _spReportApp.Update(spReport);
        }


    }
}
