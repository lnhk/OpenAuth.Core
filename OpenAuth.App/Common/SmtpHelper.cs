﻿using OpenAuth.App.Common.Entity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace OpenAuth.App.Common
{
    public class SmtpHelper
    {
        //定义默认的 邮件服务器、帐户、密码、发邮件地址
        private static readonly string mailSvr = "smtp.exmail.qq.com"; //域名也是OK的mail.163.com
        private static readonly string account = "heqinghuan@lnhkjc.cn";
        private static readonly string pwd = "4iL4F9RxFjfFTu9W";
        private static readonly string addr = "heqinghuan@lnhkjc.cn";

        /// <summary>
        /// 调用SMTP发送邮件
        /// </summary>
        /// <param name="ReciveAddrList">收件人列表</param>
        /// <param name="Subject">主题</param>
        /// <param name="Content">正文</param>
        /// <param name="AttachFile">附件，Dictionary<格件名，详细地址></param>
        /// <returns></returns>
        public static EmailMsg SendMail(string ReciveAddr, List<string> ccAddr, string Subject, string Content, string AttachFile, string[] sendemail)
        {
            EmailMsg emailMsg = new EmailMsg() { success = true, msg = string.Format("主题【{0}】发送状态：", Subject) };
            string[] toAddrs = ReciveAddr.Split(',');
            using (SmtpClient smtp = new SmtpClient(mailSvr, 587))
            {
                try
                {
                    smtp.EnableSsl = true;
                    smtp.Credentials = new NetworkCredential(sendemail[0], sendemail[1]);//身份认证

                    MailMessage mail = new MailMessage();//建立邮件
                    mail.SubjectEncoding = Encoding.GetEncoding("GBK");//主题编码
                    mail.BodyEncoding = Encoding.GetEncoding("GBK");//正文编码
                    mail.Priority = MailPriority.Normal;//邮件的优先级为中等
                    mail.IsBodyHtml = false;//正文为纯文本，如果需要用HTML则为true
                    mail.From = new MailAddress(sendemail[0]);//发件人
                    foreach (var item in ccAddr)
                    {
                        mail.CC.Add(item);
                    }
                    foreach (var item in toAddrs)
                    {
                        mail.To.Add(item);
                    }

                    mail.Subject = Subject;//主题
                    mail.Body = Content;//正文 
                    Attachment file = new Attachment(AttachFile);
                    file.Name = System.IO.Path.GetFileName(AttachFile);
                    mail.Attachments.Add(file);

                    smtp.Send(mail);//正式发邮件
                    mail.Dispose();
                    smtp.Dispose();
                    emailMsg.msg += "发送成功";
                }

                catch (Exception ex)
                {
                    emailMsg.success = false;
                    emailMsg.msg += "发送失败：原因-" + ex.Message + "-" + ex.InnerException;
                    Console.WriteLine(ex.ToString());
                }

            }
            return emailMsg;
        }
    }
}
