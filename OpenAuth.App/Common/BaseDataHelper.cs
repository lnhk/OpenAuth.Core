﻿using Infrastructure.Cache;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App.Common.Entity;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace OpenAuth.App.Common
{
    public class BaseDataHelper
    {
        private IUnitWork<OpenAuthDBContext> _unitWork;
        private CacheContext _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions()));

        public BaseDataHelper(IUnitWork<OpenAuthDBContext> unitWork)
        {
            _unitWork = unitWork;
        }

        public BaseData GetBaseData()
        {
            BaseData data = _cache.Get<BaseData>("basedata");//缓存基础数据
            string newversion = _cache.Get<string>("basedata_new_version");//最新基础数据版本号
            string tversion = DateTime.Now.Ticks.ToString();
            if (data == null)
            {
                data = GetBase();
                if (string.IsNullOrEmpty(newversion))
                {
                    _cache.Set("basedata_new_version", tversion, DateTime.Now.AddDays(3));
                    data.version = tversion;
                    _cache.Set("basedata", data, DateTime.Now.AddDays(3));
                }
                else
                {
                    data.version = newversion;
                    _cache.Set("basedata", data, DateTime.Now.AddDays(3));
                }
                return data;
            }
            else
            {
                if (data.version == newversion)
                {
                    return data;
                }
                else
                {
                    data = GetBase();
                    data.version = newversion;
                    _cache.Set("basedata", data, DateTime.Now.AddDays(3));
                    return data;
                }
            }
        }

        public BaseData GetZfBaseData()
        {
            BaseData data = _cache.Get<BaseData>("procategoryzf");//缓存基础数据
            string newversion = _cache.Get<string>("procategoryzf_new_version");//最新基础数据版本号
            string tversion = DateTime.Now.Ticks.ToString();
            if (data == null)
            {
                data = GetZfBase();
                if (string.IsNullOrEmpty(newversion))
                {
                    _cache.Set("procategoryzf_new_version", tversion, DateTime.Now.AddDays(3));
                    data.version = tversion;
                    _cache.Set("procategoryzf", data, DateTime.Now.AddDays(3));
                }
                else
                {
                    data.version = newversion;
                    _cache.Set("procategoryzf", data, DateTime.Now.AddDays(3));
                }
                return data;
            }
            else
            {
                if (data.version == newversion)
                {
                    return data;
                }
                else
                {
                    data = GetZfBase();
                    data.version = newversion;
                    _cache.Set("procategoryzf", data, DateTime.Now.AddDays(3));
                    return data;
                }
            }
        }

        private BaseData GetBase()
        {
            BaseData data = new BaseData();
            var query = from c in _unitWork.Find<Contract>(null) select c;

            var factorQuery = from fs in _unitWork.Find<Factor>(null).Take(10000) select fs;
            data.Factors = factorQuery.ToList();
            var standardQuery = from fs in _unitWork.Find<Standard>(null).Take(10000) select fs;
            data.Standards = standardQuery.ToList();
            var factorStandardQuery = from fs in _unitWork.Find<FactorStandard>(null).Take(10000) select fs;
            data.FactorStandards = factorStandardQuery.ToList();
            return data;
        }

        private BaseData GetZfBase()
        {
            BaseData data = new BaseData();
            //data.ZfItemdatas =
            var baseQualitionQuery = from fs in _unitWork.Find<BaseQualition>(null).Take(10000) select fs;
            data.ZfQualitions = baseQualitionQuery.ToList();
            return data;
        }
    }
}
