﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Request
{
    /// <summary>
    /// 导出报价单请求
    /// </summary>
    public class PriceExcelReq
    {
        /// <summary>
        /// 合同id
        /// </summary>
        public int ContractId { get; set; }
    }
}
