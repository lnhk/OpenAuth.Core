﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Request
{
    /// <summary>
    /// 复制合同请求
    /// </summary>
    public class CopyContractReq
    {
        /// <summary>
        /// 协议编号
        /// </summary>
        public string CtNumber { get; set; }
        public CopyContractEnum CopyContractEnum { get; set; } 
    }

    /// <summary>
    ///用于区分操作,复制合同操作为0,引用合同操作为1
    /// </summary>
    public enum CopyContractEnum 
    {
        复制合同=0,
        引用合同=1
    }
}
