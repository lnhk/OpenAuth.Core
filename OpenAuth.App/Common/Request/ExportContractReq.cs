﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common.Request
{
    /// <summary>
    /// 导出合同请求
    /// </summary>
    public class ExportContractReq
    {
        /// <summary>
        /// 合同id
        /// </summary>
        public decimal ContractId { get; set; }

        /// <summary>
        /// 该协议样品信息是否全部一致
        /// </summary>
        public bool? IsSame { get; set; }

        /// <summary>
        /// 合同签订日期
        /// </summary>
        public string SignDate { get; set; }

        /// <summary>
        /// 是否加盖骑缝章
        /// </summary>
        public bool? isStampQFZ { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? zeal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? person { get; set; }
    }
}
