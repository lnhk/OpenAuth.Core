﻿using OpenAuth.Repository;
using OpenAuth.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Common
{
    public class ExtensionHelper
    {
        private IUnitWork<OpenAuthDBContext> _unitWork;
        public ExtensionHelper(IUnitWork<OpenAuthDBContext> unitWork)
        {
            _unitWork = unitWork;
        }

        public void Save()
        {
            _unitWork.Save();
        }
    }
}
