using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;
using Z.EntityFramework.Plus;

namespace OpenAuth.App
{
    public class SampleTransferListApp : BaseLongApp<SampleTransferList,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleTransferListListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void AddNoSave(SampleTransferList sampleTransferList)
        {
            UnitWork.Add(sampleTransferList);
            sampleTransferList.CreateTime = DateTime.Now;
            sampleTransferList.CreateUserId = _auth.GetCurrentUser().User.Id;
        }

        public void Add(SampleTransferList sampleTransferList)
        {
            AddNoSave(sampleTransferList);
            UnitWork.Save();
        }

        public void UpdateNoSave(SampleTransferList sampleTransferList)
        {
            UnitWork.Update(sampleTransferList);
        }

        public void Update(SampleTransferList sampleTransferList)
        {
            UpdateNoSave(sampleTransferList);
            UnitWork.Save();
        }

        public SampleTransferListApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SampleTransferList,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}