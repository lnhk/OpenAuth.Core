﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateSampleTransferListReq: BaseSampleTransferList
    {
        /// <summary>
        ///任务单id
        /// </summary>
        public decimal id { get; set; }
    }
}