﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseSampleTransferList
    {

        /// <summary>
        ///审核时间
        /// </summary>
        public DateTime? auditTime { get; set; }

        /// <summary>
        ///合同样品关联id
        /// </summary>
        public string contract_sample { get; set; }

        /// <summary>
        ///实验室
        /// </summary>
        public string lab { get; set; }

        /// <summary>
        ///下单时间
        /// </summary>
        public DateTime? orderDate { get; set; }

        /// <summary>
        ///流水号
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        ///审核人
        /// </summary>
        public string auditUser { get; set; }



        /// <summary>
        ///下单人
        /// </summary>
        public string orderUser { get; set; }

        /// <summary>
        ///流转批次号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        ///接收时间
        /// </summary>
        public DateTime? receiveTime { get; set; }

        /// <summary>
        ///报告日期
        /// </summary>
        public DateTime? reportDate { get; set; }

        /// <summary>
        ///重新流转备注
        /// </summary>
        public string FlowAgainRemark { get; set; }

        /// <summary>
        ///是否为重新流转
        /// </summary>
        public bool? IsFlowAgin { get; set; }

        /// <summary>
        ///检测类型value(0-企业，1-政府)
        /// </summary>
        public string checkType { get; set; }

        /// <summary>
        ///样品编号(单样品任务单)
        /// </summary>
        public string SampleCode { get; set; }

        /// <summary>
        ///审批备注
        /// </summary>
        public string auditRemark { get; set; }

        /// <summary>
        ///接收人
        /// </summary>
        public string receiveUser { get; set; }

        /// <summary>
        ///样品编号小号
        /// </summary>
        public string SampleCodeSub { get; set; }

        /// <summary>
        ///录入人
        /// </summary>
        public string create_by { get; set; }

        /// <summary>
        ///是否为单样品任务单（1-是）
        /// </summary>
        public bool IsSingleSample { get; set; }

        /// <summary>
        ///是否为加急状态(0-标准,1-加急)
        /// </summary>
        public bool IsUrgent { get; set; }

        /// <summary>
        ///任务单pdf 路径
        /// </summary>
        public string pdf_url { get; set; }

        /// <summary>
        ///检测类型txt(0-企业，1-政府)
        /// </summary>
        public string checkTypeTxt { get; set; }

        /// <summary>
        ///项目状态
        /// </summary>
        public int status { get; set; }

        /// <summary>
        ///生成时间
        /// </summary>
        public DateTime create_time { get; set; }

        ///// <summary>
        /////创建时间(后端处理)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建用户id(后端处理)
        ///// </summary>
        //public string CreateUserId { get; set; }
    }
}
