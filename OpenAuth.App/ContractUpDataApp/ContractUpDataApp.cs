using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ContractUpDataApp : BaseLongApp<ContractUpData,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryContractUpDataListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateContractUpDataReq obj)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = obj.MapTo<ContractUpData>();
            //addObj.Time = DateTime.Now;
            addObj.ack_pass = 0;
            Repository.Add(addObj);
        }

        public void Update(AddOrUpdateContractUpDataReq obj)
        {
            ContractUpData contractUpData = obj.MapTo<ContractUpData>();
            Repository.Update(u => u.Id == obj.id, u => contractUpData);

        }

        public void DelByContractId(decimal ContractId)
        {

            Repository.Delete(u => u.ct_id == ContractId);
        }

        /// <summary>
        /// 根据合同id获取修改的合同信息
        /// </summary>
        /// <returns></returns>
        public Contract getUpContract(decimal ContractId)
        {
            var querySample = from s in UnitWork.Find<ContractUpData>(null)
                              where s.ct_id == ContractId
                              orderby s.create_time descending
                              select s;
            ContractUpData contractUpData = querySample.FirstOrDefault();
            if (contractUpData == null) return null;
            string jsonStr = contractUpData.up_contract_data;
            if ("".Equals(jsonStr) || jsonStr == null) return null;
            Contract contract = Json.ToObject<Contract>(jsonStr);
            return contract;
        }

        public ContractUpDataApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ContractUpData,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}