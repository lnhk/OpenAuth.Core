﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseSampleTaskListDataReq
    {
        /// <summary>
        ///复检申请书id
        /// </summary>
        public decimal? OrgRecordRecheckApplyId { get; set; }

        /// <summary>
        ///检测项目
        /// </summary>
        public string project { get; set; }

        /// <summary>
        ///样品id
        /// </summary>
        public int? sample_id { get; set; }

        /// <summary>
        ///样品名称
        /// </summary>
        public string sample_name { get; set; }

        /// <summary>
        ///检测类型txt
        /// </summary>
        public string CheckTypeTxt { get; set; }

        /// <summary>
        ///标准限量
        /// </summary>
        public string standardLimit { get; set; }

        /// <summary>
        ///下单时间
        /// </summary>
        public DateTime orderDate { get; set; }

        /// <summary>
        ///检测标准id
        /// </summary>
        public string standardId { get; set; }

        /// <summary>
        ///是否加急(1-是)
        /// </summary>
        public bool IsUrgent { get; set; }

        /// <summary>
        ///报告日期
        /// </summary>
        public DateTime? reportDate { get; set; }

        /// <summary>
        ///样品形态
        /// </summary>
        public string sampleForm { get; set; }

        /// <summary>
        ///样品编号下标号
        /// </summary>
        public int? sample_codeSub { get; set; }

        /// <summary>
        ///保存条件
        /// </summary>
        public string storageCondition { get; set; }

        /// <summary>
        ///判断依据
        /// </summary>
        public string judgmentBasis { get; set; }

        /// <summary>
        ///项目id
        /// </summary>
        public int? projectId { get; set; }

        /// <summary>
        ///流转批次号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        ///保质期至
        /// </summary>
        public string Expire { get; set; }

        /// <summary>
        ///原始记录信息id
        /// </summary>
        public decimal? OrgRecordId { get; set; }

        /// <summary>
        ///是否为复检（1-是）
        /// </summary>
        public bool IsRecheck { get; set; }

        /// <summary>
        ///检测标准
        /// </summary>
        public string standard { get; set; }

        /// <summary>
        ///协议备注
        /// </summary>
        public string protocolRemark { get; set; }

        /// <summary>
        ///实验室
        /// </summary>
        public string Lab { get; set; }

        /// <summary>
        ///原始记录是否为全部上传附加的(1-是)
        /// </summary>
        public bool IsAllAddtionalOrgRecord { get; set; }

        /// <summary>
        ///备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        ///下单人
        /// </summary>
        public string orderUser { get; set; }

        /// <summary>
        ///状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        ///样品数量
        /// </summary>
        public string sample_count { get; set; }

        /// <summary>
        ///结果录入提交信息id
        /// </summary>
        public decimal? EntryResultId { get; set; }

        /// <summary>
        ///是否已作废（1-作废）合同
        /// </summary>
        public bool IsObsolete { get; set; }

        /// <summary>
        ///检测类型（0-委托,1-抽检）
        /// </summary>
        public string CheckType { get; set; }

        /// <summary>
        ///提交审核人id
        /// </summary>
        public string CommitUserId { get; set; }

        /// <summary>
        ///任务单id
        /// </summary>
        public decimal? tranferlist_id { get; set; }

        /// <summary>
        ///复检原始记录id
        /// </summary>
        public decimal? OrgRecordRecheckId { get; set; }

        /// <summary>
        ///计量单位
        /// </summary>
        public string measurementUnit { get; set; }

        /// <summary>
        ///任务单号
        /// </summary>
        public string TasklistNumber { get; set; }

        /// <summary>
        ///样品编号
        /// </summary>
        public string sample_code { get; set; }

        /// <summary>
        ///提交时间
        /// </summary>
        public DateTime? CommitTime { get; set; }

        ///// <summary>
        /////创建时间(迁移用)
        ///// </summary>
        //public DateTime create_time { get; set; }

        ///// <summary>
        /////创建用户id(后端处理)
        ///// </summary>
        //public string CreateUserId { get; set; }

        ///// <summary>
        /////创建时间(后端处理)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }
    }
}
