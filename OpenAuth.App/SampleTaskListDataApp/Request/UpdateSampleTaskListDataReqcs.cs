﻿using OpenAuth.App.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class UpdateSampleTaskListDataReqcs: BaseSampleTaskListDataReq
    {
        /// <summary>
        ///任务单样品数据id
        /// </summary>
        public decimal id { get; set; }
    }
}