using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Enum.ContractEnum;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class SampleTaskListDataApp : BaseLongApp<SampleTaskListData,OpenAuthDBContext>
    {
        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QuerySampleTaskListDataListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void AddNoSave(SampleTaskListData sampleTaskListData)
        {
            sampleTaskListData.CreateTime = DateTime.Now;
            sampleTaskListData.CreateUserId = _auth.GetCurrentUser().User.Id;
        }

        public void Add(SampleTaskListData sampleTaskListData)
        {
            AddNoSave(sampleTaskListData);
            Repository.Add(sampleTaskListData);
        }

        public void UpdateNoSave(SampleTaskListData sampleTaskListData)
        {
            UnitWork.Update(sampleTaskListData);
        }

        public void Update(SampleTaskListData sampleTaskListData)
        {
            UpdateNoSave(sampleTaskListData);
            UnitWork.Save();
        }

        public void UpdateForAbandonNoSave(decimal sampleId)
        {
            var query = from taskList in UnitWork.Find<SampleTaskListData>(null) 
                        join transfer in UnitWork.Find<SampleTransferList>(null) on taskList.tranferlist_id equals transfer.Id 
                        where taskList.sample_id == sampleId && taskList.CheckType== Convert.ToString(TasklistCheckType.委托检测)
                        select taskList;
            foreach (SampleTaskListData sampleTaskListData in query.ToList())
            {
                sampleTaskListData.IsObsolete = true;
                UpdateNoSave(sampleTaskListData);
            }
        }

        public SampleTaskListDataApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<SampleTaskListData,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}