﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class CheckDevCstmrUserReq
    {
        /// <summary>
        /// 归属领域的名称
        /// </summary>
        public string DomainName { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }
    }
}
