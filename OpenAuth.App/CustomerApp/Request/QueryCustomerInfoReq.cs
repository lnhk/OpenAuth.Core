﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    /// <summary>
    /// 查询客户信息的对象，客户编号和客户名称可选
    /// </summary>
    public class QueryCustomerInfoReq
    {
        /// <summary>
        /// 客户编号
        /// </summary>
        public decimal? CompanyId { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string CompanyName { get; set; }
    }
}
