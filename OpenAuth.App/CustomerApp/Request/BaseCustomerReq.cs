﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseCustomerReq
    {
        /// <summary>
        ///备注信息
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        ///客户类型名称
        /// </summary>
        public string CompanyTypeName { get; set; }

        /// <summary>
        ///客户类型 企业，政府
        /// </summary>
        public string CompanyType { get; set; }

        /// <summary>
        ///委托单位
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        ///区域name
        /// </summary>
        public string CompanyAddressArea { get; set; }

        /// <summary>
        ///所属部门
        /// </summary>
        public string Office { get; set; }

        /// <summary>
        ///公司英文名称
        /// </summary>
        public string CompanyNameEn { get; set; }

        /// <summary>
        ///统一社会信用代码
        /// </summary>
        public string IdentityCode { get; set; }

        /// <summary>
        ///英文地址
        /// </summary>
        public string CompanyAddressEn { get; set; }

        /// <summary>
        ///邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///报告邮寄地址
        /// </summary>
        public string ReportAddress { get; set; }

        /// <summary>
        ///客户openid
        /// </summary>
        public string CustomerOpenId { get; set; }

        /// <summary>
        ///委托单位地址(详细)
        /// </summary>
        public string CompanyAddress { get; set; }

        /// <summary>
        ///附件
        /// </summary>
        public string Files { get; set; }

        /// <summary>
        ///行业领域
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        ///行业领域名称
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// 客户地址编码
        /// </summary>
        public string CompanyAddressCode { get; set; }

        ///// <summary>
        ///// 市(迁移用)
        ///// </summary>
        //public int? Xian { get; set; }

        ///// <summary>
        ///// 县(迁移用)
        ///// </summary>
        //public int? Shi { get; set; }

        ///// <summary>
        /////逻辑删除标记（0：显示；1：隐藏）(迁移用)
        ///// </summary>
        //public string DelFlag { get; set; }

        ///// <summary>
        /////创建者(迁移用)
        ///// </summary>
        //public string CreateBy { get; set; }

        ///// <summary>
        /////创建时间(迁移用)
        ///// </summary>
        //public DateTime? CreateDate { get; set; }

        ///// <summary>
        /////更新者(迁移用)
        ///// </summary>
        //public string UpdateBy { get; set; }

        ///// <summary>
        /////更新时间(迁移用)
        ///// </summary>
        //public DateTime? UpdateDate { get; set; }

        ///// <summary>
        /////创建时间(后端操作)
        ///// </summary>
        //public DateTime? CreateTime { get; set; }

        ///// <summary>
        /////创建时间(后端操作)
        ///// </summary>
        //public string CreateUserId { get; set; }

    }
}
