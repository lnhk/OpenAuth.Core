﻿using OpenAuth.App.Request;
using OpenAuth.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public  class AddCustomerWithRelatedReq
    {
        /// <summary>
        /// 客户
        /// </summary>
        public Customer Customer { get; set; }
        /// <summary>
        /// 客户联系人
        /// </summary>
        public List<BaseCustomerContractReq> CustomerContractReqs { get; set; }
        /// <summary>
        /// 发票
        /// </summary>
        public List<BaseInvoiceReq> InvoiceReqs { get; set; }
        /// <summary>
        /// 客户归属人名称
        /// </summary>
        public string CustomerUsername { get; set; }
        /// <summary>
        /// 客户归属人Id
        /// </summary>
        public string CustomerUserId { get; set; }
        /// <summary>
        /// 客户领域名称集合
        /// </summary>
        public string CustomerDomainNameArray { get; set; }
        /// <summary>
        /// 客户领域值集合
        /// </summary>
        public string CustomerDomainValueArray { get; set; }
    }
}
