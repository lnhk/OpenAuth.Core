namespace OpenAuth.App.Request
{
    public class QueryCustomerListReq : PageReq
    {
        /// <summary>
        /// 客户名称（可选）
        /// </summary>
        public string CompanyName { get; set; }
    }
}
