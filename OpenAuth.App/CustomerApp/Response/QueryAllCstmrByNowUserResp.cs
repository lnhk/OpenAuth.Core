﻿using OpenAuth.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Response
{
    public class QueryAllCstmrByNowUser
    {
        /// <summary>
        /// 当前用户所归属的所有客户的详细信息
        /// </summary>
        public List<QueryCustomerInfo> CustomerInfos { get; set; }
    }
}
