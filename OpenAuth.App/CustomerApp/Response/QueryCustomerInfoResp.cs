﻿using OpenAuth.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure;

namespace OpenAuth.App.Response
{
    public class QueryCustomerInfo: BaseCustomerResp
    {
        /// <summary>
        /// 客户对应的所有联系人
        /// </summary>
        public List<CustomerContract> CustomerContracts { get; set; }

        /// <summary>
        /// 客户对应的所有领域
        /// </summary>
        public List<CustomerUser> CustomerUsers { get; set; }

        /// <summary>
        /// 客户对应的所有发票
        /// </summary>
        public List<Invoice> Invoices { get; set; }
    }
}
