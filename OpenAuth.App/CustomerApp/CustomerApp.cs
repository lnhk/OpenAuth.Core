using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using MoreLinq.Extensions;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;

namespace OpenAuth.App
{
    public class CustomerApp : BaseLongApp<Customer, OpenAuthDBContext>
    {
        private CustomerUserApp _cstmrUserApp;
        private CustomerContractApp _cstmrContractApp;
        private InvoiceApp _invoiceApp;

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryCustomerListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.CompanyName))
            {
                objs=objs.Where(x => x.CompanyName.Contains(request.CompanyName));
            }

            result.data = objs.OrderBy(u => u.Id).OrderByDescending(c => c.CreateTime)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public void Add(Customer cstmr)
        {
            AddNoSave(cstmr);
            UnitWork.Save();
        }

        public void AddNoSave(Customer cstmr)
        {
            var user = _auth.GetCurrentUser().User;
            cstmr.CreateUserId = user.Id;
            cstmr.CreateDate = DateTime.Now;
            UnitWork.Add(cstmr);
        }

        public void AddWithRelated(Customer cstmr,List<CustomerUser> cstmrUsers,List<CustomerContract> cstmrContracts,List<Invoice> invoices)
        {
            AddNoSave(cstmr);
            foreach(CustomerUser cstmrUser in cstmrUsers)
            {
                cstmrUser.CompanyId = cstmr.Id;
                _cstmrUserApp.AddNoSave(cstmrUser);
            }
            foreach (CustomerContract cstmrContract in cstmrContracts)
            {
                cstmrContract.CompanyId = cstmr.Id;
                _cstmrContractApp.AddNoSave(cstmrContract);
            }
            foreach (Invoice invoice in invoices)
            {
                invoice.CompanyId = cstmr.Id;
                _invoiceApp.AddNoSave(invoice);
            }
            UnitWork.Save();
        }

        public void Update(UpdateCustomerReq req)
        {
            UnitWork.Update<Customer>(u => u.Id == req.Id, u => new Customer()
            {
                Remarks = req.Remarks,
                Domain = req.Domain,
                CompanyTypeName = req.CompanyTypeName,
                CompanyType = req.CompanyType,
                CompanyName = req.CompanyName,
                CompanyAddressArea = req.CompanyAddressArea,
                Office = req.Office,
                CompanyNameEn = req.CompanyNameEn,
                IdentityCode = req.IdentityCode,
                CompanyAddressEn = req.CompanyAddressEn,
                Email = req.Email,
                ReportAddress = req.ReportAddress,
                CustomerOpenId = req.CustomerOpenId,
                CompanyAddress = req.CompanyAddress,
                Files = req.Files,
                DomainName = req.DomainName,
                CompanyAddressCode = req.CompanyAddressCode,
            });
        }

        public Customer Get(string companyName)
        {
            return UnitWork.Find<Customer>((c) => c.CompanyName == companyName).FirstOrDefault();
        }

        public bool Check(string companyName)
        {
            return UnitWork.Find<Customer>((c) => c.CompanyName == companyName).Any();
        }

        public bool Check(decimal? companyId)
        {
            return UnitWork.Find<Customer>((c) => c.Id == companyId).Any();
        }

        public Customer Get(decimal? companyId)
        {
            return UnitWork.Find<Customer>((c) => c.Id == companyId).SingleOrDefault();
        }

        public List<Customer> GetAllCstmrByNowUser()
        {
            User user = _auth.GetCurrentUser().User;
            var query = from cu in UnitWork.Find<CustomerUser>(null)
                        join c in UnitWork.Find<Customer>(null) on cu.CompanyId equals c.Id
                        where cu.Username == user.Name
                        select c;
            return query.DistinctBy(c => c.Id).ToList<Customer>();
        }

        public int GetDevUserNum(string domain, decimal companyId)
        {
            var query = from cu in UnitWork.Find<CustomerUser>(null)
                        join c in UnitWork.Find<Customer>(null) on cu.CompanyId equals c.Id
                        where cu.UserType == "0" && c.Id == companyId && cu.Domain == domain
                        select c;
            return query.Count();
        }

        public CustomerApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Customer, OpenAuthDBContext> repository, IAuth auth,
            CustomerContractApp customerContractApp, CustomerUserApp customerUserApp, InvoiceApp invoiceApp) : base(unitWork, repository, auth)
        {
            _cstmrContractApp = customerContractApp;
            _cstmrUserApp = customerUserApp;
            _invoiceApp = invoiceApp;
        }
    }
}