using System;

namespace OpenAuth.App.Request
{
    public class VerifyInvoiceReq
    {
        /// <summary>
        /// 发票id
        /// </summary>
        public decimal invoiceId { get; set; }

        /// <summary>
        /// 审核状态：1通过；2拒绝
        /// </summary>
        public int verifyStatus { get; set; }

        /// <summary>
        /// 拒绝原因(通过可不写)
        /// </summary>
        public string verifyReason { get; set; }

        /// <summary>
        /// 发票开票时间(不传则为当前时间)
        /// </summary>
        public DateTime? invoiceDate { get; set; }

        /// <summary>
        /// 邮寄时间
        /// </summary>
        public DateTime? sendDate { get; set; }

    }
}
