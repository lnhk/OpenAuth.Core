using System;

namespace OpenAuth.App.Request
{
    public class QueryToBeInvoiceDRecordsReq : PageReq
    {

        /// <summary>
        /// 合同编号
        /// </summary>
        public string contractNumber { get; set; }

        /// <summary>
        /// 受检单位名称
        /// </summary>
        public string companyName { get; set; }

        /// <summary>
        /// 业务员
        /// </summary>
        public string businessManager { get; set; }

    }
}
