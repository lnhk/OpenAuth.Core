﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class ApplyInvoiceReq
    {

        /// <summary>
        /// 拒绝后重新申请需要填写发票id，其他情况为空
        /// </summary>
        public decimal? invoiceId { get; set; }

        /// <summary>
        /// 所选合同ids
        /// </summary>
        public string contractIds { get; set; }

        // 发票信息

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string invoice_title { get; set; }

        /// <summary>
        /// 发票类型：0专票；1普票；2电子发票；3开收据；4不开收据
        /// </summary>
        public int? invoice_type { get; set; }

        /// <summary>
        /// 开票金额
        /// </summary>
        public double? invoice_cose { get; set; }

        /// <summary>
        /// 发票邮寄地址
        /// </summary>
        public string invoice_address { get; set; }

        /// <summary>
        /// 发票电话
        /// </summary>
        public string opening_phone { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks { get; set; }

        /// <summary>
        /// 开户行账户
        /// </summary>
        public string opening_bank_num { get; set; }

        /// <summary>
        /// 电子邮件地址
        /// </summary>
        public string invoice_email_addr { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string opening_bank { get; set; }

        /// <summary>
        /// 发票地址
        /// </summary>
        public string opening_address { get; set; }

        /// <summary>
        /// 代码
        /// </summary>
        public string invoice_number { get; set; }


    }
}
