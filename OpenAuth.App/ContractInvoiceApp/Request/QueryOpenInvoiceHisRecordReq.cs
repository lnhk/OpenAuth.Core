using System;

namespace OpenAuth.App.Request
{
    public class QueryOpenInvoiceHisRecordReq : PageReq
    {

        /// <summary>
        /// 发票类型：0专票；1普票；2电子发票；3开收据；4不开收据
        /// </summary>
        public int? invoiceType { get; set; }

        /// <summary>
        /// 开票时间-开始
        /// </summary>
        public DateTime? invoiceOpenBeginTime { get; set; }

        /// <summary>
        /// 开票时间-结束
        /// </summary>
        public DateTime? invoiceOpenEndTime { get; set; }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string invoiceTitle { get; set; }

        /// <summary>
        /// 发票状态:0申请中；1开票完成；2作废；3冲红；4拒绝开票
        /// </summary>
        public int? invoiceStatus { get; set; }

        /// <summary>
        /// 业务员
        /// </summary>
        public string businessManager { get; set; }

    }
}
