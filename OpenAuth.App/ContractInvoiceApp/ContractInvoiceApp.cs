using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;
using OpenAuth.Repository.QueryObj;
using Yitter.IdGenerator;

namespace OpenAuth.App
{
    public class ContractInvoiceApp : BaseLongApp<ContractInvoice,OpenAuthDBContext>
    {

        /*/// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryContractInvoiceListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateContractInvoiceReq obj)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = obj.MapTo<ContractInvoice>();
            //addObj.Time = DateTime.Now;
            Repository.Add(addObj);
        }
        
        public void Update(AddOrUpdateContractInvoiceReq obj)
        {
            *//*UnitWork.Update<ContractInvoice>(u => u.Id == obj.Id, u => new ContractInvoice
            {
               //todo:要修改的字段赋值
            });*//*

        }*/

        // 获取销售可开发票额度信息
        public object getXsFpJe() 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            //string userAccount = 
            string userId = loginContext.User.Id;

            // 全部发票金额
            var queryAllMoney = from c in UnitWork.Find<Contract>(null)
                                where c.beforeuser == userId
                                select c;
            object queryAllMoneyRes = queryAllMoney.Sum(c => c.ct_pay_cose);

            // 历史开票金额
            var queryHisMoeny = from c in UnitWork.Find<Contract>(null)
                                where c.beforeuser == userId
                                select c;
            object queryHisMoenyRes = queryHisMoeny.Sum(c => c.opencose);



            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("totalMoney", queryAllMoneyRes);// 全部合同金额
            dic.Add("hisMoeny", queryHisMoenyRes);//历史开票金额

            //可开票金额
            double queryAllMoneyResInt = Convert.ToDouble(queryAllMoneyRes);
            double queryHisMoenyResInt = Convert.ToDouble(queryHisMoenyRes);
            double openInvoiceMoeny = queryAllMoneyResInt - queryHisMoenyResInt;
            dic.Add("openInvoiceMoeny", openInvoiceMoeny);//可开票金额
            return dic;
        }

        // 获取某销售开票记录
        public Task<TableResp<ContractInvoice>> getXsOpenInvoiceHisRecords(QueryOpenInvoiceHisRecordReq request) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            //string userAccount = 
            string userId = loginContext.User.Id;

            // 条件：发票类型 invoiceType
            // 开票时间 invoiceOpenBeginTime  invoiceOpenEndTime
            // 发票抬头 invoiceTitle
            // 发票状态 status
            // 业务员 businessManager

            string tj = "";
            if (request.invoiceType != null) 
            {
                tj = tj + " AND ci.invoice_type = "+ request.invoiceType +" ";
            }
            if (request.invoiceOpenBeginTime != null && request.invoiceOpenEndTime != null) 
            {
                tj = tj + " AND ci.invoice_date BETWEEN '"+ request.invoiceOpenBeginTime +"' AND '"+ request.invoiceOpenEndTime +"' ";
            }
            if (!string.IsNullOrEmpty(request.invoiceTitle)) 
            {
                tj = tj + " AND ci.invoice_title LIKE '%"+ request.invoiceTitle +"%' ";
            }
            if (request.invoiceStatus != null)
            {
                tj = tj + " AND ci.status = "+ request.invoiceStatus +" ";
            }
            if (!string.IsNullOrEmpty(request.businessManager))
            {
                tj = tj + " AND ci.business_manager = "+ request.businessManager +" ";
            }

            string countSql = "SELECT " +
                        "COUNT(*) " +
                        "FROM hk_contract_invoice ci " +
                        "WHERE ci.create_by = '" + userId + "' "
                        + tj
                        ;

            int count = UnitWork.ScalarQuery<int>(countSql, null);

            string sql = "SELECT ci.* " +
                        "FROM hk_contract_invoice ci " +
                        "WHERE ci.create_by = '" + userId + "' " +
                        tj + 
                        "ORDER BY ci.id DESC offset " + ((request.page - 1) * request.limit) + " ROWS FETCH NEXT " + request.limit + " ROWS ONLY "
                        ;


            List<ContractInvoice> queryXsOpenInvoiceRecordRes = UnitWork.DynamicQuery<ContractInvoice>(sql,null).ToList();

            var result = new TableResp<ContractInvoice>();
            result.data = queryXsOpenInvoiceRecordRes;
            result.count = count;
            return Task.FromResult(result);
        }

        // 获取某销售未开的发票列表
        public Task<TableResp<Contract>> getXsToBeInvoiceDRecords(QueryToBeInvoiceDRecordsReq request) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            //string userAccount = 
            string userId = loginContext.User.Id;

            // 查询条件：
            // 合同编号
            // 业务员
            // 受检单位

            string tj = "";
            if (!string.IsNullOrEmpty(request.contractNumber)) 
            {
                tj = tj + " AND c.ct_number = "+ request.contractNumber +" ";
            }
            if (!string.IsNullOrEmpty(request.companyName))
            {
                tj = tj + " AND c.ct_entrust_company LIKE '%"+ request.companyName + "%' ";
            }
            if (!string.IsNullOrEmpty(request.businessManager))
            {
                tj = tj + " AND c.beforeusername = '"+ request.businessManager +"' ";
            }

            string countSql = "SELECT COUNT(*) " +
                            "FROM hk_contract c " +
                            "WHERE c.beforeuser = '"+ userId + "' " +
                            "AND c.opencose < c.ct_pay_cose " + tj;

            int count = UnitWork.ScalarQuery<int>(countSql, null);

            string sql = "SELECT c.* " +
                        "FROM hk_contract c " +
                        "WHERE c.beforeuser = '" + userId + "' " +
                        "AND c.opencose < c.ct_pay_cose " +
                        tj + 
                        "ORDER BY c.id DESC offset " + ((request.page - 1) * request.limit) + " ROWS FETCH NEXT " + request.limit + " ROWS ONLY";
            List<Contract> queryContractListRes = UnitWork.DynamicQuery<Contract>(sql, null).ToList();


            var result = new TableResp<Contract>();
            result.data = queryContractListRes;
            result.count = count;
            return Task.FromResult(result);
        }

        // 申请开票
        public void applyInvoice(ApplyInvoiceReq req) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            string userName = loginContext.User.Name;
            string userId = loginContext.User.Id;

            // 是否是拒绝的票 重新提交的
            if (req.invoiceId != null)
            {
                // 获取发票信息
                var queryInvoceInfo = from i in UnitWork.Find<ContractInvoice>(null)
                                      where i.Id == req.invoiceId
                                      select i;
                ContractInvoice contractInvoice = queryInvoceInfo.FirstOrDefault();
                if (contractInvoice == null) { throw new CommonException("未找到发票信息", 500); }
                if(contractInvoice.verify_status != 2) { throw new CommonException("非拒绝状态", 500); }
                // 修改的值赋值
                EntityToEntity(req, contractInvoice);
                contractInvoice.verify_status = 0;
                UnitWork.Update<ContractInvoice>(contractInvoice);

                //保存操作记录
                ContractInvoiceOperatorRecord operationRecord = new ContractInvoiceOperatorRecord();
                operationRecord.invoice_id = contractInvoice.Id;
                operationRecord.operator_id = userId;
                operationRecord.operator_name = userName;
                operationRecord.create_time = DateTime.Now;
                operationRecord.operator_data = "拒绝的发票重新发起审核";
                UnitWork.Add<ContractInvoiceOperatorRecord>(operationRecord);
            }
            else 
            {
                // 生成一张发票
                ContractInvoice contractInvoice = new ContractInvoice();
                contractInvoice.create_date = DateTime.Now;
                contractInvoice.invoice_email_addr = req.invoice_email_addr;
                contractInvoice.opening_bank = req.opening_bank;
                contractInvoice.status = 0;
                contractInvoice.create_by = userId;
                contractInvoice.invoice_title = req.invoice_title;
                contractInvoice.opening_address = req.opening_address;
                contractInvoice.opening_bank_num = req.opening_bank_num;
                contractInvoice.remarks = req.remarks;
                contractInvoice.invoice_number = req.invoice_number;
                contractInvoice.opening_phone = req.opening_phone;
                contractInvoice.invoice_address = req.invoice_address;
                contractInvoice.invoice_cose = req.invoice_cose;
                contractInvoice.invoice_type = req.invoice_type;
                contractInvoice.invoice_date = null;
                contractInvoice.send_date = null;
                contractInvoice.update_by = null;
                contractInvoice.update_date = DateTime.Now;
                contractInvoice.verify_status = 0;
                contractInvoice.change_status = null;
                contractInvoice.openperson_name = null;
                contractInvoice.openperson_id = null;
                contractInvoice.create_by_name = userName;
                contractInvoice.verify_reason = null;


                // 关联合同与发票
                string[] contractidsArray = req.contractIds.Split(",");

                decimal contractAllMoeny = 0m;

                string ctNnumbers = null;
                string ctCompanyNames = null;
                string busMgr = null;

                foreach (string cid in contractidsArray)
                {
                    decimal contractId = Convert.ToDecimal(cid);
                    // 获取合同信息
                    var queryContract = from c in UnitWork.Find<Contract>(null)
                                        where c.Id == contractId
                                        select c;
                    Contract contractTmp = queryContract.FirstOrDefault();
                    if (contractTmp == null) { throw new CommonException("未找到合同信息", 500); }

                    // 赋值合同编号和委托单位
                    ctNnumbers += "[" + contractTmp.ct_number + "]";
                    ctCompanyNames += "[" + contractTmp.ct_entrust_company + "]";
                    if (string.IsNullOrEmpty(busMgr)) 
                    {
                        busMgr = contractTmp.beforeusername;
                    }


                    // 计算合同金额
                    decimal tempct_pay_cose = (decimal)contractTmp.ct_pay_cose;
                    contractAllMoeny = contractAllMoeny + tempct_pay_cose;

                    // 判断重复开票问题
                    decimal openCose = (decimal)contractTmp.opencose;
                    if (openCose >= tempct_pay_cose) { throw new CommonException("已经申请/已完成开票", 500); }


                    // 修改合同内容：赋值已开票金额 修改开票状态
                    // 开票金额基准：如果是一个合同以前台传的开票金额为准；如果是多个合同以合同金额为准
                    if (contractidsArray.Length == 1)
                    {
                        contractTmp.Invoice = true;
                        // 开票金额
                        decimal opencose = Convert.ToDecimal(req.invoice_cose);
                        if (opencose > tempct_pay_cose) { throw new CommonException("开票金额大于合同金额", 500); }
                        contractTmp.opencose = opencose;
                        // 未开票金额
                        contractTmp.no_opencose = tempct_pay_cose - opencose;
                        // 更新合同
                        UnitWork.Update<Contract>(contractTmp);
                    }
                    else
                    {
                        contractTmp.Invoice = true;
                        // 开票金额
                        contractTmp.opencose = tempct_pay_cose;
                        // 未开票金额
                        contractTmp.no_opencose = 0;
                        // 更新合同
                        UnitWork.Update<Contract>(contractTmp);
                    }

                }

                contractInvoice.ct_number = ctNnumbers;
                contractInvoice.company_name = ctCompanyNames;
                if (contractidsArray.Length == 1)
                {
                    contractInvoice.invoice_cose = Convert.ToDouble(req.invoice_cose);
                }
                else
                {
                    contractInvoice.invoice_cose = Convert.ToDouble(contractAllMoeny);
                }
                UnitWork.Add<ContractInvoice>(contractInvoice);
                // 获取发票id
                decimal InvoiceId = contractInvoice.Id;

                foreach (string cid in contractidsArray)
                {
                    decimal contractId = Convert.ToDecimal(cid);
                    // 关联关系
                    ContractInvoiceRelation relation = new ContractInvoiceRelation();
                    relation.invoiceid = InvoiceId;
                    relation.contract_id = contractId;
                    UnitWork.Add<ContractInvoiceRelation>(relation);
                }

                //保存操作记录
                ContractInvoiceOperatorRecord operationRecord = new ContractInvoiceOperatorRecord();
                operationRecord.invoice_id = InvoiceId;
                operationRecord.operator_id = userId;
                operationRecord.operator_name = userName;
                operationRecord.create_time = DateTime.Now;
                operationRecord.operator_data = "提交开票申请";
                UnitWork.Add<ContractInvoiceOperatorRecord>(operationRecord);
            }

            UnitWork.Save();
        }

        // 作废发票
        public void voidInvoice(decimal invoiceId) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            string userName = loginContext.User.Name;
            string userId = loginContext.User.Id;

            // 获取发票信息
            var queryInvoceInfo = from i in UnitWork.Find<ContractInvoice>(null)
                                  where i.Id == invoiceId
                                  select i;
            ContractInvoice contractInvoice = queryInvoceInfo.FirstOrDefault();
            if (contractInvoice == null) { throw new CommonException("未找到发票信息", 500); }
            if (contractInvoice.verify_status != 1) { throw new CommonException("请等待发票审核后再做处理", 500); }
            // 判断发票日期是否当月发票
            if (contractInvoice.status == 0) { throw new CommonException("请等待审核后操作", 500); }
            if (contractInvoice.status == 2) { throw new CommonException("已作废发票无需重复作废", 500); }
            if (contractInvoice.status == 3) { throw new CommonException("冲红发票不可作废", 500); }
            // 判断开票日期  如果是当月可直接作废，不是则报错处理
            DateTime nowTime = DateTime.Now;
            DateTime invoiceDate = (DateTime)contractInvoice.invoice_date;
            if (invoiceDate.Month != nowTime.Month) { throw new CommonException("请点击冲红发票", 500); }
            // 申请作废状态
            contractInvoice.verify_status = 0;
            contractInvoice.change_status = 2;// 申请作废
            UnitWork.Update<ContractInvoice>(contractInvoice);

            //保存操作记录
            ContractInvoiceOperatorRecord operationRecord = new ContractInvoiceOperatorRecord();
            operationRecord.invoice_id = invoiceId;
            operationRecord.operator_id = userId;
            operationRecord.operator_name = userName;
            operationRecord.create_time = DateTime.Now;
            operationRecord.operator_data = "申请发票作废";
            UnitWork.Add<ContractInvoiceOperatorRecord>(operationRecord);

            UnitWork.Save();
        }

        // 冲红
        public void redInkInvoice(decimal invoiceId) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            string userName = loginContext.User.Name;
            string userId = loginContext.User.Id;

            // 获取发票信息
            var queryInvoceInfo = from i in UnitWork.Find<ContractInvoice>(null)
                                  where i.Id == invoiceId
                                  select i;
            ContractInvoice contractInvoice = queryInvoceInfo.FirstOrDefault();
            if (contractInvoice == null) { throw new CommonException("未找到发票信息", 500); }
            if (contractInvoice.verify_status != 1) { throw new CommonException("请等待发票审核后再做处理", 500); }
            // 判断发票日期是否当月发票
            if (contractInvoice.status == 0) { throw new CommonException("请等待审核后操作", 500); }
            if (contractInvoice.status == 2) { throw new CommonException("已作废发票", 500); }
            if (contractInvoice.status == 3) { throw new CommonException("已是冲红发票", 500); }
            // 判断开票日期 后 如果是当月可直接作废，不是则报错处理
            DateTime nowTime = DateTime.Now;
            DateTime invoiceDate = (DateTime)contractInvoice.invoice_date;
            if (invoiceDate.Month == nowTime.Month) { throw new CommonException("本月发票请作废处理", 500); }

            // 发票申请冲红
            contractInvoice.verify_status = 0;
            contractInvoice.change_status = 3;// 申请冲红
            UnitWork.Update<ContractInvoice>(contractInvoice);


            //保存操作记录
            ContractInvoiceOperatorRecord operationRecord = new ContractInvoiceOperatorRecord();
            operationRecord.invoice_id = invoiceId;
            operationRecord.operator_id = userId;
            operationRecord.operator_name = userName;
            operationRecord.create_time = DateTime.Now;
            operationRecord.operator_data = "申请冲红发票";
            UnitWork.Add<ContractInvoiceOperatorRecord>(operationRecord);

            UnitWork.Save();
        }


        // 获取财务待开/已开列表
        public async Task<TableData> getFinancialInvoiceList(QueryContractInvoiceListReq request) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            // 查询条件：
            // 审核状态 ·
            // 发票类型
            // 开票时间-开始
            // 开票时间-结束
            // 发票抬头
            // 发票状态
            // 业务员

            bool isDcl = false; // 是否待处理列表
            if (request.verifyStatus == 0) 
            {
                isDcl = true;
            }


            var objs = GetDataPrivilege("u");
            /*if (!string.IsNullOrEmpty(request.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }*/
            if (isDcl)
            {
                objs = objs.Where(u => u.verify_status == 0);
            }
            else 
            {
                objs = objs.Where(u => u.verify_status != 0);
            }
            if(request.invoiceType != null) 
            {
                objs = objs.Where(u => u.invoice_type == request.invoiceType);
            }
            if (request.invoiceOpenBeginTime != null && request.invoiceOpenEndTime != null)
            {
                objs = objs.Where(u => u.invoice_date >= request.invoiceOpenBeginTime && u.invoice_date <= request.invoiceOpenEndTime);
            }
            if (!string.IsNullOrEmpty(request.invoiceTitle)) 
            {
                objs = objs.Where(u => u.invoice_title.Contains(request.invoiceTitle));
            }
            if (request.invoiceStatus != null) 
            {
                objs = objs.Where(u => u.status == request.invoiceStatus);
            }
            if (!string.IsNullOrEmpty(request.businessManager))
            {
                objs = objs.Where(u => u.business_manager.Equals(request.businessManager));
            }


            var result = new TableData();
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        // 财务审核发票
        public void financialVerifyInvoice(VerifyInvoiceReq req) 
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            // 获取发票信息
            string userName = loginContext.User.Name;
            string userId = loginContext.User.Id;

            // 获取发票信息
            var queryInvoceInfo = from i in UnitWork.Find<ContractInvoice>(null)
                                  where i.Id == req.invoiceId
                                  select i;
            ContractInvoice contractInvoice = queryInvoceInfo.FirstOrDefault();
            if (contractInvoice == null) { throw new CommonException("未找到发票信息", 500); }
            if (contractInvoice.verify_status != 0) { throw new CommonException("发票非审核状态", 500); }
            // 判断发票日期是否当月发票
            if (contractInvoice.status == 2) { throw new CommonException("已作废发票", 500); }
            if (contractInvoice.status == 3) { throw new CommonException("已是冲红发票", 500); }

            string operatorData = "申请通过";

            if (contractInvoice.status == 0 || contractInvoice.status == 4)
            {
                // 新申请发票
                if (req.verifyStatus == 1)
                {
                    contractInvoice.status = 1;
                }
                else 
                {
                    if (string.IsNullOrEmpty(req.verifyReason)) { throw new CommonException("请输入拒绝原因", 500); }
                    contractInvoice.status = 4;
                    operatorData = "审核拒绝["+ req.verifyReason +"]";
                }
                contractInvoice.verify_status = req.verifyStatus;
                contractInvoice.invoice_date = req.invoiceDate == null ? DateTime.Now : req.invoiceDate;
                contractInvoice.openperson_id = userId;
                contractInvoice.send_date = req.sendDate;
                contractInvoice.change_status = null;
                contractInvoice.openperson_name = userName;
                contractInvoice.verify_reason = operatorData;
                UnitWork.Update<ContractInvoice>(contractInvoice);
            }
            else 
            {
                // 通过的发票
                if (req.verifyStatus == 1)
                {
                    // 审核通过
                    // 查询关联合同信息
                    // 获取发票关联的合同信息
                    string sql = "SELECT c.* " +
                                "FROM hk_contract c " +
                                "LEFT JOIN hk_contract_invoice_relation cir ON c.id = cir.contract_id " +
                                "WHERE cir.invoiceid = " + req.invoiceId;
                    List<Contract> queryContractListRes = UnitWork.DynamicQuery<Contract>(sql, null).ToList();

                    // 更新所有合同开票状态：未开票，开票金额为0 未开票金额(多合同为合同金额，单合同为开票金额)
                    foreach (Contract item in queryContractListRes)
                    {
                        item.Invoice = false;
                        // 开票金额
                        item.opencose = 0;
                        // 未开票金额
                        item.no_opencose = queryContractListRes.Count == 1 ? item.no_opencose + Convert.ToDecimal(contractInvoice.invoice_cose) : item.ct_pay_cose;
                        // 更新合同
                        UnitWork.Update<Contract>(item);
                    }

                    // 判断发票变更操作 是 作废2 or 冲红3
                    if (contractInvoice.change_status == 2)
                    {
                        // 判断开票日期 后 如果是当月可直接作废，不是则报错处理
                        DateTime nowTime = DateTime.Now;
                        DateTime invoiceDate = (DateTime)contractInvoice.invoice_date;
                        if (invoiceDate.Month != nowTime.Month) { throw new CommonException("请提交【冲红】申请", 500); }

                        // 作废
                        contractInvoice.status = 2;
                        operatorData = "发票【作废】申请通过";
                    }
                    else if (contractInvoice.change_status == 3)
                    {
                        // 判断开票日期 后 如果是当月可直接作废，不是则报错处理
                        DateTime nowTime = DateTime.Now;
                        DateTime invoiceDate = (DateTime)contractInvoice.invoice_date;
                        if (invoiceDate.Month == nowTime.Month) { throw new CommonException("请提交【作废】申请", 500); }

                        // 冲红
                        contractInvoice.status = 1;
                        operatorData = "发票【冲红】申请通过";
                        // 生成一张原发票的负数发票

                        ContractInvoice tempCH = new ContractInvoice();
                        EntityToEntity(contractInvoice, tempCH);
                        tempCH.Id = YitIdHelper.NextId();
                        tempCH.status = 3;
                        tempCH.change_status = null;
                        tempCH.verify_status = 1;
                        tempCH.verify_reason = "冲红发票对应原始发票id：" + contractInvoice.Id;
                        tempCH.invoice_cose = -tempCH.invoice_cose;
                        tempCH.openperson_id = userId;
                        tempCH.openperson_name = userName;
                        UnitWork.Add<ContractInvoice>(tempCH);
                        // 关联新发票和原始的合同关系表
                        foreach (Contract item in queryContractListRes) 
                        {
                            ContractInvoiceRelation tempRelation = new ContractInvoiceRelation();
                            tempRelation.contract_id = item.Id;
                            tempRelation.invoiceid = tempCH.Id;
                            UnitWork.Add<ContractInvoiceRelation>(tempRelation);
                        }
                    }
                    else 
                    {
                        throw new CommonException("非法操作", 500);
                    }

                }
                else 
                {
                    // 审核拒绝
                    if (string.IsNullOrEmpty(req.verifyReason)) { throw new CommonException("请输入拒绝原因", 500); }
                    // 目前只有两种状态 暂时这么判断
                    operatorData = contractInvoice.change_status == 2 ? "作废" : "冲红" + "|审核拒绝[" + req.verifyReason + "]";
                }


                contractInvoice.verify_status = req.verifyStatus;
                contractInvoice.change_status = null; // 清空请求变更装填
                contractInvoice.openperson_id = userId;
                contractInvoice.openperson_name = userName;
                contractInvoice.verify_reason = operatorData;
                UnitWork.Update<ContractInvoice>(contractInvoice);
            }

            //保存操作记录
            ContractInvoiceOperatorRecord operationRecord = new ContractInvoiceOperatorRecord();
            operationRecord.invoice_id = req.invoiceId;
            operationRecord.operator_id = userId;
            operationRecord.operator_name = userName;
            operationRecord.create_time = DateTime.Now;
            operationRecord.operator_data = operatorData;
            UnitWork.Add<ContractInvoiceOperatorRecord>(operationRecord);

            UnitWork.Save();
        }

        /// <summary>
        /// 将一个实体的值赋值到另外一个实体
        /// </summary>
        /// <param name="objectsrc">源实体</param>
        /// <param name="objectdest">目标实体</param>
        private void EntityToEntity(object objectsrc, object objectdest) 
        {
            var sourceType = objectsrc.GetType();
            var destType = objectdest.GetType();
            foreach (var source in sourceType.GetProperties())
            {
                foreach (var dest in destType.GetProperties())
                {
                    if (dest.Name == source.Name)
                    {
                        dest.SetValue(objectdest, source.GetValue(objectsrc));
                    }
                }
            }
        }

        public ContractInvoiceApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ContractInvoice,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}