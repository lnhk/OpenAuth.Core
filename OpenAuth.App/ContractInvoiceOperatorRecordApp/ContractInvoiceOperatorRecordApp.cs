using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ContractInvoiceOperatorRecordApp : BaseLongApp<ContractInvoiceOperatorRecord,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryContractInvoiceOperatorRecordListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            
          
            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateContractInvoiceOperatorRecordReq obj)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = obj.MapTo<ContractInvoiceOperatorRecord>();
            //addObj.Time = DateTime.Now;
            Repository.Add(addObj);
        }
        
        public void Update(AddOrUpdateContractInvoiceOperatorRecordReq obj)
        {
            /*UnitWork.Update<ContractInvoiceOperatorRecord>(u => u.Id == obj.Id, u => new ContractInvoiceOperatorRecord
            {
               //todo:要修改的字段赋值
            });*/

        }

        public ContractInvoiceOperatorRecordApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ContractInvoiceOperatorRecord,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}