namespace OpenAuth.App.Request
{
    public class QueryStandardListReq : PageReq
    {
        /// <summary>
        /// 方法名称
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// 数据来源
        /// </summary>
        public int? DataSource { get; set; }
    }
}
