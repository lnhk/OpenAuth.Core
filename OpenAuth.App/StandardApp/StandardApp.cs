using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Cache;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class StandardApp : BaseStringApp<Standard,OpenAuthDBContext>
    {
        private CacheContext _cache;
        public StandardApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Standard, OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
            _cache = new CacheContext(new MemoryCache(new MemoryCacheOptions()));
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryStandardListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }

            if (!string.IsNullOrEmpty(request.StandardName))
            {
                //增加筛选条件,如：
                objs = objs.Where(u => u.standardname.Contains(request.StandardName));
            }
            if (request.DataSource.HasValue)
            {
                objs = objs.Where(u => u.DataSource == request.DataSource.Value);
            }

            result.data = objs.OrderByDescending(u => u.create_date)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateStandardReq obj)
        {
            //程序类型取入口应用的名称，可以根据自己需要调整
            var addObj = obj.MapTo<Standard>();
            //addObj.Time = DateTime.Now;
            Repository.Add(addObj);
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }
        
        public void Update(AddOrUpdateStandardReq obj)
        {
            //UnitWork.Update<Standard>(u => u.Id == obj.id, u => new Standard
            //{
            //   //todo:要修改的字段赋值
            //});
            var standard = obj.MapTo<Standard>();
            Repository.Update(standard);
            _cache.Set("basedata_new_version", DateTime.Now.Ticks.ToString(), DateTime.Now.AddDays(3));
        }


    }
}