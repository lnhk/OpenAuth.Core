using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class InvoiceApp : BaseLongApp<Invoice, OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryInvoiceListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }


            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
                //增加筛选条件,如：
                //objs = objs.Where(u => u.Name.Contains(request.key));
            }



            result.data = objs.OrderBy(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count = await objs.CountAsync();
            return result;
        }

        public List<Invoice> GetByComapnyId(decimal? companyId)
        {
            List<Invoice> invoices = UnitWork.Find<Invoice>(null).Where((i) => i.CompanyId == companyId).ToList();
            return invoices;
        }

        public void Add(Invoice invoice)
        {
            AddNoSave(invoice);
            UnitWork.Save();
        }

        public void AddNoSave(Invoice invoice)
        {
            User user = _auth.GetCurrentUser().User;
            invoice.CreateUserId = user.Id;
            invoice.CreateDate = DateTime.Now;
            UnitWork.Add(invoice);
        }

        public void Update(UpdateInvoiceReq req)
        {
            UnitWork.Update<Invoice>(u => u.Id == req.Id, u => new Invoice
            {
                CreateUserId = u.CreateUserId,
                InvoiceAddressArea = u.InvoiceAddressArea,
                UpdateDate = u.UpdateDate,
                InvoiceAddress = u.InvoiceAddress,
                InvoiceTypeId = u.InvoiceTypeId,
                Remarks = u.Remarks,
                Payable = u.Payable,
                CreateTime = u.CreateTime,
                IdentificationNumber = u.IdentificationNumber,
                CreateBy = u.CreateBy,
                UpdateBy = u.UpdateBy,
                OpeningBankNumber = u.OpeningBankNumber,
                OpeningBank = u.OpeningBank,
                InvoiceTelephone = u.InvoiceTelephone,
                CreateDate = u.CreateDate,
                InvoiceType = u.InvoiceType,
                DelFlag = u.DelFlag,
                CompanyId = u.CompanyId,
                OldId = u.OldId,
            });

        }

        public Invoice GetByContractId(decimal? contractId)
        {
            var query = from c in UnitWork.Find<Contract>(null) 
                        join i in UnitWork.Find<Invoice>(null) on c.invoice_id equals i.Id 
                        where c.Id==contractId select i;
            return query.FirstOrDefault();
        }

        public InvoiceApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<Invoice, OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}