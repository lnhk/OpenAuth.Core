﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAuth.App.Request
{
    public class BaseInvoiceReq
    {
        /// <summary>
        ///开票地址(省市县区代码)
        /// </summary>
        public string InvoiceAddressArea { get; set; }

        /// <summary>
        ///开票地址(详细)
        /// </summary>
        public string InvoiceAddress { get; set; }

        /// <summary>
        ///发票类型id
        /// </summary>
        public string InvoiceTypeId { get; set; }

        /// <summary>
        ///备注信息
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        ///发票抬头
        /// </summary>
        public string Payable { get; set; }

        /// <summary>
        ///纳税人识别号
        /// </summary>
        public string IdentificationNumber { get; set; }

        /// <summary>
        ///开户账号
        /// </summary>
        public string OpeningBankNumber { get; set; }

        /// <summary>
        ///开户行
        /// </summary>
        /// 
        public string OpeningBank { get; set; }

        /// <summary>
        ///开票电话
        /// </summary>
        public string InvoiceTelephone { get; set; }

        /// <summary>
        ///发票类型
        /// </summary>
        public string InvoiceType { get; set; }

        ///// <summary>
        /////逻辑删除标记（0：显示；1：隐藏）(迁移用)
        ///// </summary>
        //public string DelFlag { get; set; }

        ///// <summary>
        /////旧主键(迁移用)
        ///// </summary>
        //public string OldId { get; set; }

        ///// <summary>
        ///// 创建时间(迁移用)
        ///// </summary>
        //public DateTime CreateDate { get; set; }

        ///// <summary>
        ///// 创建者(迁移用)
        ///// </summary>
        //public DateTime CreateBy { get; set; }

        ///// <summary>
        ///// 更新者(迁移用)
        ///// </summary>
        //public string UpdateBy { get; set; }

        ///// <summary>
        /////更新时间(迁移用)
        ///// </summary>
        //public string UpdateDate { get; set; }

        ///// <summary>
        ///// 创建时间(后端操作)
        ///// </summary>
        //public DateTime CreateTime { get; set; }

        ///// <summary>
        ///// 创建者编号(后端操作)
        ///// </summary>
        //public String CreateUserId { get; set; }
    }
}
