using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using OpenAuth.App.Interface;
using OpenAuth.App.Request;
using OpenAuth.App.Response;
using OpenAuth.Repository;
using OpenAuth.Repository.Domain;
using OpenAuth.Repository.Interface;


namespace OpenAuth.App
{
    public class ContractBackApp : BaseLongApp<ContractBack,OpenAuthDBContext>
    {

        /// <summary>
        /// 加载列表
        /// </summary>
        public async Task<TableData> Load(QueryContractBackListReq request)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
            throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }
        
           
            var result = new TableData();
            var objs = GetDataPrivilege("u");
            if (!string.IsNullOrEmpty(request.key))
            {
            //增加筛选条件,如：
            //objs = objs.Where(u => u.Name.Contains(request.key));
            }


            objs = objs.Where(u => u.contractid.Equals(request.contractId));


            result.data = objs.OrderByDescending(u => u.Id)
            .Skip((request.page - 1) * request.limit)
            .Take(request.limit);
            result.count =await objs.CountAsync();
            return result;
        }

        public void Add(AddOrUpdateContractBackReq obj)
        {
            var loginContext = _auth.GetCurrentUser();
            if (loginContext == null)
            {
                throw new CommonException("登录已过期", Define.INVALID_TOKEN);
            }

            // 获取合同内容
            var queryContract = from c in UnitWork.Find<Contract>(null)
                                where c.Id == obj.contractid
                                select c;
            Contract contract = queryContract.FirstOrDefault();
            if (contract == null) 
            {
                throw new CommonException("未找到合同内容", 500);
            }
            // 查询是否存在历史回款记录
            decimal hisCBackMoeny = (decimal) contract.backcose;

            // 取得合同内所需内容
            // 合同金额
            decimal contractAmount = (decimal) contract.ct_pay_cose;
            // 判断回款金额 是否符合
            if ((hisCBackMoeny + (decimal)obj.backcose) > contractAmount) 
            {
                throw new CommonException("回款总金额大于合同金额", 500);
            }

            // 判断回款金额，如果回款完成修改合同内字段，如果回款部分  需不需要修改合同内的未回款金额赋值
            if ((hisCBackMoeny + (decimal)obj.backcose) == contractAmount)
            {
                // 回款完成 修改 状态和回款金额
                contract.money_back = true;
                contract.backcose = contractAmount;
            }
            else 
            {
                // 部分回款 修改 回款金额
                contract.money_back = false;
                contract.backcose = hisCBackMoeny + (decimal)obj.backcose;
            }
            // 更新合同内容
            UnitWork.Update<Contract>(contract);

            // 合同折扣
            string contractDis = contract.ct_cose_discount;
            // 合同报告完成时间
            DateTime? doneTime = contract.donetime == null ? null : contract.donetime;
            // 业务员id
            string beforeUserId = contract.beforeuser;
            // 业务员名称
            string beforeUserName = contract.beforeusername;
            // 计算回款周期
            double days = 0;
            if (doneTime != null && obj.backdate != null) 
            {
                DateTime t = (DateTime) doneTime;
                DateTime h = (DateTime) obj.backdate;
                TimeSpan timeSpan = t.Subtract(h).Duration();
                days = timeSpan.TotalDays;//天数
            }
            
            ContractBack contractBack = new ContractBack();
            contractBack.contractid = obj.contractid;
            contractBack.backcose = obj.backcose;
            contractBack.backdate = obj.backdate;
            contractBack.create_by = loginContext.User.Id;
            contractBack.create_date = DateTime.Now;
            contractBack.back_type_k = obj.back_type_k;
            contractBack.back_type_v = obj.back_type_v;
            contractBack.back_days = Convert.ToInt32(days);// 四舍五入
            contractBack.back_discount = Convert.ToInt32(string.IsNullOrEmpty(contractDis) ? "0" : contractDis);
            contractBack.contract_report_done_time = doneTime;
            contractBack.business_manager_id = beforeUserId;
            contractBack.business_manager_name = beforeUserName;
            contractBack.contract_money = contractAmount;
            contractBack.create_by_name = loginContext.User.Name;
            UnitWork.Add<ContractBack>(contractBack);
            UnitWork.Save();
        }

        public void del(decimal[] ids) 
        {
            foreach (decimal cbId in ids) 
            {
                // 删除。，同时合同金额减掉，回款完成的变为未完成
                var queryContractBack = from cb in UnitWork.Find<ContractBack>(null)
                                        where cb.Id == cbId
                                        select cb;
                ContractBack contractBack = queryContractBack.FirstOrDefault();
                if (contractBack == null)
                {
                    throw new CommonException("未找到回款记录", 500);
                }
                // 获取合同
                var queryContract = from c in UnitWork.Find<Contract>(null)
                                    where c.Id == contractBack.contractid
                                    select c;
                Contract contract = queryContract.FirstOrDefault();
                if (contract == null)
                {
                    throw new CommonException("未找到合同记录", 500);
                }
                // 更新合同内的回款金额
                contract.money_back = false;
                contract.backcose = contract.backcose - contractBack.backcose;
                UnitWork.Update<Contract>(contract);
                UnitWork.Delete<ContractBack>(contractBack);
            }
            UnitWork.Save();
        }

        public ContractBackApp(IUnitWork<OpenAuthDBContext> unitWork, IRepository<ContractBack,OpenAuthDBContext> repository, IAuth auth) : base(unitWork, repository, auth)
        {
        }
    }
}